trigger APTPS_AccountLocationTrigger on Apttus_Config2__AccountLocation__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerHandlerDispatcher.execute(Apttus_Config2__AccountLocation__c.getSObjectType());
}