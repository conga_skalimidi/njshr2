trigger APTS_QuoteProposalTrigger on Apttus_Proposal__Proposal__c (before insert, before update, after update) {
	system.debug('>>>>>>>>>>Before dispatching to trigger handler ');
	TriggerHandlerDispatcher.execute(Apttus_Proposal__Proposal__c.getSObjectType());
}