import { LightningElement, api, track, wire  } from 'lwc';
import { getRecord, getFieldValue  } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import Agreement__c_FIELD from '@salesforce/schema/Payment_Entry__c.Agreement__c';
import Comments__c_FIELD from '@salesforce/schema/Payment_Entry__c.Comments__c';
import Payment_Method__c_FIELD from '@salesforce/schema/Payment_Entry__c.Payment_Method__c';
import CurrencyIsoCode_FIELD from '@salesforce/schema/Payment_Entry__c.CurrencyIsoCode';
import Payment_Amount__c_FIELD from '@salesforce/schema/Payment_Entry__c.Payment_Amount__c';
import Payment_Date__c_FIELD from '@salesforce/schema/Payment_Entry__c.Payment_Date__c';
import CurrencyIsoCode_Agreement_FIELD from '@salesforce/schema/Apttus__APTS_Agreement__c.CurrencyIsoCode';

export default class CreatePaymentEntry extends LightningElement {

    @api recordId;
    @api Payment_Entry__c;
    @track disabledSubmit= false;
    @track defaultCurrencyIsoCode = 'USD';
    fields = [Comments__c_FIELD, Payment_Method__c_FIELD, CurrencyIsoCode_FIELD, Payment_Amount__c_FIELD, Payment_Date__c_FIELD];
    
    handleSubmit(event){
        event.preventDefault();       // stop the form from submitting
        this.disabledSubmit = true;
        const fields = event.detail.fields;
        fields.Agreement__c = this.recordId ;
        console.log('My fiels '+ JSON.stringify(fields));
        this.template.querySelector('lightning-record-edit-form').submit(fields);
    }
    

    @wire(getRecord, { recordId: '$recordId', fields: [CurrencyIsoCode_Agreement_FIELD] })
    wiredRecord({ error, data }) {
        if (error) {
            console.log('default error CurrencyISO code -', this.defaultCurrencyIsoCode);
            this.defaultCurrencyIsoCode = 'USD';
        } else if (data) {
            this.defaultCurrencyIsoCode = getFieldValue(data, CurrencyIsoCode_Agreement_FIELD);
            console.log('default CurrencyISO code -', this.defaultCurrencyIsoCode);
        }
    }

    handleSucess(event){
        const evt = new ShowToastEvent({
            title: "Payment Entry created!",
            variant: "success"
        });
        this.dispatchEvent(evt);
        const updatedRecord = event.detail.id;
        console.log('onsuccess: ', updatedRecord);
        this.disabledSubmit = false;
        this.handleReset(event);
    }
    handleReset(event) {
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                console.log(field);
                field.reset();
                if(field.fieldName == 'CurrencyIsoCode') {
                    field.value = this.defaultCurrencyIsoCode;
                }
            });
        }
    }  
}