import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'; 
import Requires_Re_Approval__c_FIELD from '@salesforce/schema/Apttus_Proposal__Proposal__c.Requires_Re_Approval__c';
import APTPS_Requires_Re_Approval_Comments__c_FIELD from '@salesforce/schema/Apttus_Proposal__Proposal__c.APTPS_Requires_Re_Approval_Comments__c';
export default class APTPSOverrideApprovalRequest extends LightningElement {
    @api recordId;
    @api Apttus_Proposal__Proposal__c;
    fields = [Requires_Re_Approval__c_FIELD,APTPS_Requires_Re_Approval_Comments__c_FIELD];
    handleSucess(event){
        const evt = new ShowToastEvent({
            title: "Fields updated!",
            variant: "success"
        });
        this.dispatchEvent(evt);
        const updatedRecord = event.detail.id;
        console.log('onsuccess: ', updatedRecord);
    }
    handleReset(event) {
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
        }
     }
}