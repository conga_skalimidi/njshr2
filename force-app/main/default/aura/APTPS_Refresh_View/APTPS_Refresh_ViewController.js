({
    refreshClick : function(component, event, helper) {
        
        var recId = component.get("v.recordId");

        console.log("IN FUNCTION:"+recId);
        var action = component.get("c.updateRecord");
        
        action.setParams({
            "recordId": component.get("v.recordId"),
            "recordType": component.get("v.agreementRecord.Apttus_QPComply__RelatedProposalId__r.Master_Agreement_Record_Type_ID__c")
        });
        // Configure response handler
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                console.log('Success, response state: ' + state);
                //$A.get('e.force:refreshView').fire();
                location.reload();
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        console.log("OUT FUNCTION");
        $A.enqueueAction(action);
    
    },
})