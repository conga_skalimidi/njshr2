/***********************************************************************************************************************
@Name: APTS_AssetLineItemCallBackTest 
@Author: Siva Kumar
@CreateDate: 23 October 2020
@Description: Test Class for APTS_AssetLineItemCallBack.
************************************************************************************************************************
@ModifiedBy: Conga Dev Team
@ModifiedDate: 25 March 2021
@ChangeDescription: Remvoed usage of createLineItem. testsetup cleanup
************************************************************************************************************************/
@isTest
public with sharing class APTS_AssetLineItemCallBackTest {
    @testsetup
    static void setupData(){
        
        Account testAccount = APTS_CPQTestUtility.createAccount('test apttus account', 'Enterprise');
        //populate custom fields value if required
        insert testAccount;
        system.assert(testAccount.Name == 'test apttus account');
        
        Contact testContact = APTS_CPQTestUtility.createContact('ContactFN', 'Apttus', testAccount.Id);
        //populate custom fields value if required
        insert testContact;
        //system.assert(testContact.Id != null);
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', testAccount.Id, 'Propose');
        //populate custom fields value if required
        insert testOpportunity;
        //system.assert(testOpportunity.Name == 'test Apttus Opportunity');
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        //populate custom fields value if required
        insert testPriceList;
        //system.assert(testPriceList.Name == 'test Apttus Price List');
        
        Product2 testProduct = APTS_CPQTestUtility.createProduct('test Apttus Product', 'APTS', 'Apttus', 'Bundle', true, true, true, true);
        //populate custom fields value if required
        insert testProduct;
        //system.assert(testProduct.Name == 'test Apttus Product');
        
        PricebookEntry testPBE = APTS_CPQTestUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct.Id, 100, true);
        //populate custom fields value if required
        insert testPBE;
        //system.assert(testPBE.Id != null);
        
        Apttus_Config2__PriceListItem__c testPriceListItem = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testProduct.Id, 100, 'Standard Price', 'One Time', 'Per Unit', 'Each', true);
        //populate custom fields value if required
        insert testPriceListItem;
       // system.assert(testPriceListItem.Id != null);
        
        Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', testAccount.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        insert testProposal;
        //system.assert(testProposal.Id != null);
        
        String testConfigurationId = APTS_CPQTestUtility.createConfiguration(testProposal.Id);
        
        system.assert(testConfigurationId != null);
        
        Apttus_Config2__ClassificationName__c testClassification = APTS_CPQTestUtility.createCategory('test Apttus Classification', 'test Hiearchy Label', 'Apttus', null, true);
        //populate custom fields value if required
        insert testClassification;
        //system.assert(testClassification.Name == 'test Apttus Classification');
        
        Apttus_Config2__ClassificationHierarchy__c testClassificationHierarchy = APTS_CPQTestUtility.createClassificationHierarchy(testClassification.Id, 'Classification Hierarchy Label');
        //populate custom fields value if required
        insert testClassificationHierarchy;
        //system.assert(testClassificationHierarchy.Id != null);
        
        Apttus_Config2__ProductOptionComponent__c testProductOptionComponent = APTS_CPQTestUtility.createProductOptionComponent(1);
        //populate custom fields value if required
        insert testProductOptionComponent;
        //system.assert(testProductOptionComponent.Id != null);
        
        Apttus_Config2__SummaryGroup__c testSummaryGroup = APTS_CPQTestUtility.createSummaryGroup(testConfigurationId, 1, 1);
        //populate custom fields value if required
        insert testSummaryGroup;
        //system.assert(testSummaryGroup.Id != null);
        
        //APTS_CPQTestUtility.createLineItem(testConfigurationId, testProduct.Id, 1);
        Apttus_Config2__LineItem__c liToInsert = APTS_CPQTestUtility.getLineItem(testConfigurationId, testProposal, testPriceListItem, testProduct, testProduct.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
        insert liToInsert;
        list<Apttus_Config2__LineItem__c> listLineItem = [SELECT Id FROM Apttus_Config2__LineItem__c WHERE Apttus_Config2__ConfigurationId__c = :testConfigurationId];
        //system.assert(listLineItem != null);

    }

    @isTest
    static void assetLineItemTest(){
        Test.startTest();

        Account objAccount = [SELECT ID FROM Account where Name = 'test apttus account'];
        Apttus_Proposal__Proposal__c testProposal = [SELECT Id,CurrencyIsoCode FROM Apttus_Proposal__Proposal__c WHERE Apttus_Proposal__Proposal_Name__c = 'test Apttus Proposal'];
        Apttus_Config2__ProductConfiguration__c config1 = [SELECT ID,Apttus_CQApprov__Approval_Status__c,Apttus_QPConfig__Proposald__c,Apttus_QPConfig__Proposald__r.CurrencyIsoCode FROM Apttus_Config2__ProductConfiguration__c WHERE Apttus_QPConfig__Proposald__c = :testProposal.Id];

        Apttus_Config2.CustomClass.ActionParams actionP = new Apttus_Config2.CustomClass.ActionParams();
        actionP.AccountId = objAccount.ID;
        Apttus_Config2.CallbackTester.testAssetLineItemCallback(config1.ID , null, null, new APTS_AssetLineItemCallBack(), actionP);

        Test.stopTest();
    }
}