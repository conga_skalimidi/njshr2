/************************************************************************************************************************
@Name: APTPS_CPQFields_Demo
@Author: Conga PS Dev Team
@CreateDate: 09 July 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CPQFields_Demo{   
    
    public void populateDemoFields(Apttus_Proposal__Proposal__c oProposal,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        Apttus_Proposal__Proposal_Line_Item__c oPLI = null;
        for (Apttus_Proposal__Proposal_Line_Item__c oLi : quoteLines) {   
            if(oLi.Apttus_QPConfig__LineType__c == 'Product/Service') {
                if(oLi.Apttus_Proposal__Product__r.ProductCode == 'NJE_DEMO') {         
                   oProposal.Aircraft_Types__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_NJE_Demo_Aircraft__c;
                    oProposal.APTPS_PaymentMethod__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_NJE_Payment_Method__c; 
                    oProposal.APTPS_NJE_Rate_Type__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_NJE_Rate_Type__c; 
                } else if(oLi.Apttus_Proposal__Product__r.ProductCode == 'NJUS_DEMO') {
                    oProposal.Aircraft_Types__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Demo_Aircraft__c;
                    oProposal.APTPS_PaymentMethod__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Payment_Method__c; 
                    oProposal.APTPS_Rate_Type__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Rate_Type__c; 
                }           
                oProposal.APTPS_Type_of_Demo__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Type_of_Demo__c;
                oProposal.APTPS_Purpose_of_Flight__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Purpose_of_Flight__c;
                oProposal.APTPS_Flying_with_Competition__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Flying_with_Competition__c;
                oProposal.APTPS_How_do_they_currently_fly__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_How_do_they_currently_fly__c;                            
                //oProposal.APTPS_Comments__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Comments__c;                
                if(oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Waive_Credit_Card_Authorization__c) {
                    oProposal.APTPS_Waive_Credit_Card_Authorization__c = 'Yes'; 
                } else {
                    oProposal.APTPS_Waive_Credit_Card_Authorization__c = 'No';
                }                            
                oProposal.APTPS_Waive_Positioning_Fees__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Waive_Positioning_Fees__c;                  
            } else if(oLi.Apttus_QPConfig__LineType__c == 'Option') {
                if(oLi.Apttus_QPConfig__OptionId__r.ProductCode == 'NJE_FLIGHT_INFO' || oLi.Apttus_QPConfig__OptionId__r.ProductCode == 'NJUS_FLIGHT_INFO') {
                    oProposal.APTPS_Date__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Date__c;
                    oProposal.APTPS_Time__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Time__c;
                    oProposal.APTPS_Departure_Airport_Code__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Departure_Airport_Code__c;
                    oProposal.APTPS_Arrival_Airport_Code__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Arrival_Airport_Code__c;
                    oProposal.APTPS_Number_of_Passengers__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Number_of_Passengers__c;
                    oProposal.APTPS_Number_of_Pets__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Number_of_Pets__c;
                }
                if(oLi.Apttus_QPConfig__OptionId__r.ProductCode == 'NJUS_RET_FLIGHT_INFO' || oLi.Apttus_QPConfig__OptionId__r.ProductCode == 'NJE_RET_FLIGHT_INFO') {
                    oProposal.APTPS_Return_Arrival_Airport_Code__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Arrival_Airport_Code__c;
                    oProposal.APTPS_Return_Date__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Date__c;
                    oProposal.APTPS_Return_Departure_Airport_Code__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Departure_Airport_Code__c;
                    oProposal.APTPS_Return_Number_of_Pets__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Number_of_Pets__c;
                    oProposal.APTPS_Return_Number_of_Passengers__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Number_of_Passengers__c;
                    oProposal.APTPS_Return_Time__c = oLi.Apttus_QPConfig__AttributeValueId__r.APTPS_Time__c;
                    oProposal.APTPS_Return_Flight_Information__c = true;
                } else {
                    oProposal.APTPS_Return_Arrival_Airport_Code__c = null;
                    oProposal.APTPS_Return_Date__c = null;
                    oProposal.APTPS_Return_Departure_Airport_Code__c = null;
                    oProposal.APTPS_Return_Number_of_Pets__c = null;
                    oProposal.APTPS_Return_Number_of_Passengers__c = null;
                    oProposal.APTPS_Return_Time__c = null;
                    oProposal.APTPS_Return_Flight_Information__c = false;
                }
            }
        }      
            oProposal.Product_Line__c = APTS_ConstantUtil.DEMO;
        
        
         system.debug('oProposal-->'+oProposal);
    }   
   
    /** 
    @description: Demo NJE implementation
    @param:
    @return: 
    */
    public class NJE_Fields implements APTPS_CPQFieldsInterface {
        public void populateCPQFields(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Demo NJE Fields');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_CPQFields_Demo obj = new APTPS_CPQFields_Demo();
            obj.populateDemoFields(proposalObj,quoteLines);            
            
        }
    }
    
     /** 
    @description: Demo NJA implementation
    @param:
    @return: 
    */
    public class NJA_Fields implements APTPS_CPQFieldsInterface {
        public void populateCPQFields(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Demo NJA Fields');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_CPQFields_Demo obj = new APTPS_CPQFields_Demo();
            obj.populateDemoFields(proposalObj,quoteLines);            
            
        }
    }
}