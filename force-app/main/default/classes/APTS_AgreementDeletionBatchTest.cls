/*******************************************************************************************************************************************
@Name: APTS_AgreementDeletionBatchTest
@Author: Avinash Bamane
@CreateDate: 08/03/2021
@Description: Test Coverage for APTS_AgreementDeletionScheduler & APTS_AgreementDeletionBatch
*******************************************************************************************************************************************/

@isTest
public class APTS_AgreementDeletionBatchTest {
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    @testsetup
    static void setupData(){
        // Create Settigs
		APTPS_Agreement_Deletion_Batch__c batchDetails = new APTPS_Agreement_Deletion_Batch__c();
		batchDetails.Number_of_days__c = '=TODAY';
		batchDetails.Agreement_Status__c = '\'Contract Generated\', \'Pending Funding\', \'Contract Pending\', \'Draft\'';
        batchDetails.Proposal_Status__c = 'Cancelled';
        batchDetails.Proposal_Comment__c = 'Test Comment';
		insert batchDetails;
        
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus_Proposal__Proposal__c.getRecordTypeInfosByName().get('Proposal NJUS').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus_Proposal__Proposal__c.getRecordTypeInfosByName().get('Proposal NJE').getRecordTypeId();
        
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
        accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
        insert accToInsert;
        
        Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', accToInsert.Id);
        insert testAccLocation;
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
        
        Product2 testProduct = APTS_CPQTestUtility.createProduct('Card', 'APTS', 'Apttus', 'Bundle', true, true, true, true);
        insert testProduct;
        
        List<Apttus_Proposal__Proposal__c> proposalList = new List<Apttus_Proposal__Proposal__c>();
        
        //New Sale
        Apttus_Proposal__Proposal__c NSProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        NSProposal.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        NSProposal.RecordTypeId  = recordTypeIdNJUS;
        proposalList.add(NSProposal);
        
        Apttus_Proposal__Proposal__c NCSCProposal = APTS_CPQTestUtility.createProposal('test Apttus ProposalNCSC', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        NCSCProposal.APTS_New_Sale_or_Modification__c = APTS_Constants.MODIFICATION;
        NCSCProposal.APTS_Modification_Type__c = APTS_Constants.MODIFICATION_TYPE_NEWCARDSALE_CONVERSION;
        NCSCProposal.RecordTypeId  = recordTypeIdNJUS;
        proposalList.add(NCSCProposal);
        
        Apttus_Proposal__Proposal__c EHCProposal = APTS_CPQTestUtility.createProposal('EHC Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        EHCProposal.APTS_New_Sale_or_Modification__c = APTS_Constants.MODIFICATION;
        EHCProposal.APTS_Modification_Type__c = APTS_Constants.EHC;
        EHCProposal.RecordTypeId  = recordTypeIdNJE;
        proposalList.add(EHCProposal);
        insert proposalList;
        
        List<Apttus__APTS_Agreement__c> agList = new List<Apttus__APTS_Agreement__c>();
        Apttus__APTS_Agreement__c ag = APTS_CPQTestUtility.createAgreement('Test In Modification agreement', 'In Modification', accToInsert.Id, NCSCProposal.Id);
        ag.APTS_New_Sale_or_Modification__c = APTS_Constants.MODIFICATION;
        ag.APTS_Modification_Type__c = APTS_Constants.MODIFICATION_TYPE_NEWCARDSALE_CONVERSION;
        ag.Agreement_Status__c = 'Draft';
        system.assert(ag!=null);
        agList.add(ag);
        
        Apttus__APTS_Agreement__c ag1 = APTS_CPQTestUtility.createAgreement('Test In Modification agreement 1', 'In Modification', accToInsert.Id, EHCProposal.Id);
        ag1.APTS_New_Sale_or_Modification__c = APTS_Constants.MODIFICATION;
        ag1.APTS_Modification_Type__c = APTS_Constants.EHC;
        ag1.Agreement_Status__c = 'Draft';
        system.assert(ag1!=null);
        agList.add(ag1);
        insert agList;
        
        List<Apttus_Config2__AssetLineItem__c> assetList = new List<Apttus_Config2__AssetLineItem__c>();
        Apttus_Config2__AssetLineItem__c assetLi = APTS_CPQTestUtility.createAssetLI(testProduct.Id, 'Option', 'Activated', ag.Id);
        assetLi.APTS_Original_Agreement__c = ag.Id;
        assetList.add(assetLi);
        
        Apttus_Config2__AssetLineItem__c assetLi1 = APTS_CPQTestUtility.createAssetLI(testProduct.Id, 'Option', 'Activated', ag1.Id);
        assetLi1.Apttus_CMConfig__AgreementId__c = ag1.Id;
        assetList.add(assetLi1);
        
        insert assetList;
        
        List<Apttus_Config2__AssetAttributeValue__c> assetAttrList = new List<Apttus_Config2__AssetAttributeValue__c>();
        Apttus_Config2__AssetAttributeValue__c termAssetAttri = new Apttus_Config2__AssetAttributeValue__c();
        termAssetAttri.Apttus_Config2__AssetLineItemId__c = assetLi.Id;
        termAssetAttri.Delayed_Start_Amount_Months__c = String.valueOf(0);
        termAssetAttri.Initial_Term_Amount_Months__c = String.valueOf(12);
        termAssetAttri.Grace_Period_Amount_Months__c = String.valueOf(12);
        termAssetAttri.Override_Term__c = true;
        termAssetAttri.Overridden_Delayed_Start_Months__c = String.valueOf(6);
        termAssetAttri.Overridden_Grace_Period_Months__c = String.valueOf(6);
        termAssetAttri.Overridden_Initial_Term_Months__c = String.valueOf(6);
        assetAttrList.add(termAssetAttri);
        
        Apttus_Config2__AssetAttributeValue__c termAssetAttri1 = new Apttus_Config2__AssetAttributeValue__c();
        termAssetAttri1.Apttus_Config2__AssetLineItemId__c = assetLi.Id;
        termAssetAttri1.Delayed_Start_Amount_Months__c = String.valueOf(0);
        termAssetAttri1.Initial_Term_Amount_Months__c = String.valueOf(12);
        termAssetAttri1.Grace_Period_Amount_Months__c = String.valueOf(12);
        termAssetAttri1.Override_Term__c = true;
        termAssetAttri1.Overridden_Delayed_Start_Months__c = String.valueOf(6);
        termAssetAttri1.Overridden_Grace_Period_Months__c = String.valueOf(6);
        termAssetAttri1.Overridden_Initial_Term_Months__c = String.valueOf(6);
        assetAttrList.add(termAssetAttri1);
        insert assetAttrList;
        
        assetLi.Apttus_Config2__AttributeValueId__c = termAssetAttri.Id;
        update assetLi;
        
        assetLi1.Apttus_Config2__AttributeValueId__c = termAssetAttri1.Id;
        update assetLi1;
        
        List<Apttus_Proposal__Proposal_Line_Item__c> pliList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        Apttus_Proposal__Proposal_Line_Item__c NCSCquoteli = new Apttus_Proposal__Proposal_Line_Item__c();
        NCSCquoteli.Apttus_Proposal__Product__c = testProduct.Id;
        NCSCquoteli.Apttus_QPConfig__LineStatus__c = 'Existing';
        NCSCquoteli.Apttus_Proposal__Proposal__c = NCSCProposal.Id;
        NCSCquoteli.Option_Product_Name__c = APTS_Constants.UPGRADE;
        //NCSCquoteli.Apttus_QPConfig__DerivedFromId__c = NCSClistLineItem[0].id;
        NCSCquoteli.Apttus_QPConfig__IsPrimaryLine__c = true;
        NCSCquoteli.Apttus_QPConfig__LineType__c = 'Product/Service';
        NCSCquoteli.Apttus_QPConfig__AssetLineItemId__c = assetLi.Id;
        NCSCquoteli.Apttus_QPConfig__ChargeType__c = 'Purchase Price';
        pliList.add(NCSCquoteli);
        
        Apttus_Proposal__Proposal_Line_Item__c EHCquoteli = new Apttus_Proposal__Proposal_Line_Item__c();
        EHCquoteli.Apttus_Proposal__Product__c = testProduct.Id;
        EHCquoteli.Apttus_QPConfig__LineStatus__c = 'Existing';
        EHCquoteli.Apttus_Proposal__Proposal__c = EHCProposal.Id;
        EHCquoteli.Option_Product_Name__c = APTS_Constants.UPGRADE;
        //EHCquoteli.Apttus_QPConfig__DerivedFromId__c = assignlistLineItem[0].id;
        EHCquoteli.Apttus_QPConfig__IsPrimaryLine__c = true;
        EHCquoteli.Apttus_QPConfig__LineType__c = 'Product/Service';
        EHCquoteli.Apttus_QPConfig__AssetLineItemId__c = assetLi1.Id;
        EHCquoteli.Apttus_QPConfig__ChargeType__c = 'Purchase Price';
        pliList.add(EHCquoteli);
        insert pliList;     
        
        Apttus_QPConfig__ProposalProductAttributeValue__c NCSCpav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        NCSCpav.Apttus_QPConfig__LineItemId__c = NCSCquoteli.id;
        NCSCpav.Proposed_Extended_End_Date__c  = System.today();
        NCSCpav.APTS_Bonus_Hours__c = 1;
        NCSCpav.APTS_Credit_Amount__c = 100;
        NCSCpav.Aircraft_Hours__c = 25;
        insert NCSCpav;
        NCSCquoteli.Apttus_QPConfig__AttributeValueId__c = NCSCpav.id;
        update NCSCquoteli;    
        
        Apttus_QPConfig__ProposalProductAttributeValue__c EHCpav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        EHCpav.Apttus_QPConfig__LineItemId__c = EHCquoteli.id;
        EHCpav.Proposed_Extended_End_Date__c  = System.today();
        EHCpav.APTS_Bonus_Hours__c = 1;
        EHCpav.APTS_Credit_Amount__c = 100;
        EHCpav.Aircraft_Hours__c = 25;
        insert EHCpav;
        EHCquoteli.Apttus_QPConfig__AttributeValueId__c = EHCpav.id;
        update EHCquoteli;
        //END: GCM-10120
    }
    
    @isTest
    public static void testAgreementDeletionScheduler() {
        Test.startTest();
        String jobId = System.schedule('APTS Test APTS_AgreementDeletionScheduler', CRON_EXP, new APTS_AgreementDeletionScheduler());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        
        Set<Id> qSet = new Set<Id>();
        for(Apttus_Proposal__Proposal__c q : [SELECT Id FROM Apttus_Proposal__Proposal__c]) {
            qSet.add(q.Id);
        }
        System.enqueueJob(new APTS_AgreementDeleteQueueable(qSet, 'Cancelled', 'Test Comments'));
        Test.stopTest();
    }
}