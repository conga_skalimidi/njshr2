public with sharing class AcctTeamAgreementPETriggerHandler extends TriggerHandler {
    public override void afterInsert() {
        AcctTeamAgreementPETriggerHelper.CreateAccTTeamMembersPlatformEvent((Map<Id, AccountTeamMember>)Trigger.newMap);
    }
    public override void afterUpdate() {
        AcctTeamAgreementPETriggerHelper.CreateAccTTeamMembersPlatformEvent((Map<Id, AccountTeamMember>)Trigger.newMap);
    }
   public override void afterDelete() {
        AcctTeamAgreementPETriggerHelper.CreateAccTTeamMembersPlatformEvent((Map<Id, AccountTeamMember>)Trigger.newMap);
    }

}