public with sharing class GCMAcctTeamMemberPETriggerHandler extends TriggerHandler {
    public override void afterInsert() {
         //System.enqueueJob(new UpdateAgrAcctTeamMbrQueueableHelper((Map<Id, AccountTeamMemberEvent__e>)Trigger.newMap));
         UpdateAgrAcctTeamMbrHelper.UpdateAgreementsWithAcctTeamMembers((List<AccountTeamMemberEvent__e>)Trigger.new);
    }

}