/***********************************************************************************************************************
@Name: APTPS_UpdateContractRequestTest
@Author: Siva Kumar
@CreateDate: 07 December 2020
@Description: APEX class to test classes APTPS_UpdateContractRequestCtrl
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
@isTest
public with sharing class APTPS_UpdateContractRequestTest {
     @isTest
    static void UpdateContractRequestTest(){
        List<Account> accList = new List<Account>();
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();

        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
        accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
        insert accToInsert;

        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;

        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;

        Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal.APTS_Modification_Type__c = 'Add Enhancements';
        insert testProposal;
        
        Apttus_Proposal__Proposal__c testProposal1 = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal1.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal1.APTS_Modification_Type__c = 'New Card Sale Conversion';
        insert testProposal1;
        
        //Start Test
        Test.startTest();
        APTPS_UpdateContractRequestCtrl.updateContractRequest(testProposal1.id);
        //Stop Test
        Test.stopTest();
                
    }
    
    
    
    
}