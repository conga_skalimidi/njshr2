/************************************************************************************************************************
@Name: APTPS_CLMLifeCycle_Corporate
@Author: Conga PS Dev Team
@CreateDate: 4 June 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CLMLifeCycle_Corporate {
    /** 
    @description: Coprporate Trail implementation
    @param:
    @return: 
    */
    public class NJE_CLMFlow implements APTPS_CLMLifeCycleInterface {
        public void callCLMAction(Map<String, Object> args) {
            system.debug('update Agreement Status Fields');
            List<Apttus__APTS_Agreement__c> newAg = (List<Apttus__APTS_Agreement__c>)args.get('newAgrList');
            List<Apttus__APTS_Agreement__c> ctAgList = new List<Apttus__APTS_Agreement__c>();
            Map<Id,SObject> oldAgMap = (Map<Id,SObject>)args.get('oldAgrList');
            for(Apttus__APTS_Agreement__c ag : newAg) {
            
                Apttus__APTS_Agreement__c oldAgObj = (Apttus__APTS_Agreement__c) oldAgMap.get(ag.Id);
                if (oldAgObj != null) {
                    String oldStatus = oldAgObj.Apttus__Status__c;
                    String oldStatusCat = oldAgObj.Apttus__Status_Category__c;
                    String newStatus = ag.Apttus__Status__c;
                    String newStatusCat = ag.Apttus__Status_Category__c;
                    system.debug('SNL Agreement Status Lifecycle. oldStatus --> '+oldStatus+' oldStatusCat --> '+oldStatusCat+
                                ' newStatus --> '+newStatus+' newStatusCat --> '+newStatusCat);
                    //Document Generated
                    if(APTS_ConstantUtil.REQUEST.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.REQUEST.equalsIgnoreCase(oldStatusCat) 
                       && APTS_ConstantUtil.READY_FOR_SIGN.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(newStatusCat)) {
                           APTPS_CLMLifeCycle_Corporate.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_CONTRACT_GENERATED, APTS_ConstantUtil.AGREEMENT_PENDING_FUNDING, APTS_ConstantUtil.DOC_GENERATED);
                           //Stamp contract generated date
                           ag.APTPS_Contract_Generated_Date__c = Date.today();
                       }
                    
                    //Send for Signatures
                    if(APTS_ConstantUtil.READY_FOR_SIGN.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldStatusCat) 
                       && APTS_ConstantUtil.OTHER_PARTY_SIGN.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(newStatusCat)) {
                           APTPS_CLMLifeCycle_Corporate.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_CONTRACT_PENDING, APTS_ConstantUtil.AGREEMENT_PENDING_FUNDING, APTS_ConstantUtil.DOC_PENDING);
                           //START
                           if(ag.Number_of_Payments_Made__c > 0)
                               ag.Funding_Status__c = APTS_ConstantUtil.FUNDED_CONTRACT;
                           //END
                       }
                    
                    //Fully Signed
                    if(APTS_ConstantUtil.OTHER_PARTY_SIGN.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldStatusCat) 
                       && APTS_ConstantUtil.FULLY_SIGNED.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(newStatusCat)) {
                           String agreementStatus = APTS_ConstantUtil.AGREEMENT_PENDING_FUNDING;
                           String fundingStatus = APTS_ConstantUtil.AGREEMENT_PENDING_FUNDING;
                           //GCM-11585: We might need to change this condition. Payment can be done before document is sent for Signatures
                           //if(oldAgObj.Number_of_Payments_Made__c == 0 && ag.Number_of_Payments_Made__c > 0) {
                           if(ag.Number_of_Payments_Made__c > 0) {
                               agreementStatus = APTS_ConstantUtil.AGREEMENT_PENDING_ACTIVATION;
                               fundingStatus = APTS_ConstantUtil.FUNDED_CONTRACT;
                           }
                           APTPS_CLMLifeCycle_Corporate.setAgreementStatus(ag, agreementStatus, fundingStatus, APTS_ConstantUtil.DOC_SIGNED);
                       }
                    
                    //Payment entry
                    if(oldAgObj.Number_of_Payments_Made__c == 0 && ag.Number_of_Payments_Made__c > 0) {
                        ag = APTPS_CLMLifeCycle_Corporate.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_PENDING_ACTIVATION, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_SIGNED);
                        if(APTS_ConstantUtil.FULLY_SIGNED.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(newStatusCat))
                            ag.Document_Status__c = APTS_ConstantUtil.DOC_SIGNED;
                        if(APTS_ConstantUtil.OTHER_PARTY_SIGN.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(newStatusCat))
                            ag.Document_Status__c = APTS_ConstantUtil.DOC_PENDING;
                        
                        //Date calculation - Specific for Coprporate Trial
                        if(APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(ag.APTPS_Program_Type__c))
                            ctAgList.add(ag);
                        
                        //GCM-11585 START: Payment can be done before document is sent for Signatures
                        if(APTS_ConstantUtil.READY_FOR_SIGN.equalsIgnoreCase(oldStatus) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldStatusCat) 
                          && APTS_ConstantUtil.DOC_GENERATED.equalsIgnoreCase(oldAgObj.Document_Status__c)) {
                            APTPS_CLMLifeCycle_Corporate.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_CONTRACT_GENERATED, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_GENERATED);
                        }
                        //END: Payment can be done before document is sent for Signatures
                    }
                    
                    //Activate
                    if((APTS_ConstantUtil.FULLY_SIGNED.equalsIgnoreCase(oldStatus) || APTS_ConstantUtil.OTHER_PARTY_SIGN.equalsIgnoreCase(oldStatus)) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldStatusCat) 
                       && APTS_ConstantUtil.ACTIVATED.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_EFFECT.equalsIgnoreCase(newStatusCat)) {
                           APTPS_CLMLifeCycle_Corporate.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_ACTIVE, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_SIGNED);
                       }
                }
            
            }
            if(!ctAgList.isEmpty()) {
                APTPS_CLMLifeCycle_Corporate.stampCTDates(oldAgMap, ctAgList);
                //END: SNL Agreement lifecycle
            }
            
            //system.debug('agrList-->'+agrList);
                
            
        }
        
        
        
    }
    
    /**
    @description: Reusable method to set Agreement Status
    @param: ag - Agreement record
    @param: agStatus - Agreement Status and Chevron Status
    @param: fundingStatus - Funding Status
    @param: docStatus - Document Status
    @return: void
    */
    public static Apttus__APTS_Agreement__c setAgreementStatus(Apttus__APTS_Agreement__c ag, String agStatus, String fundingStatus, String docStatus) {
        ag.Agreement_Status__c = agStatus;
        ag.Chevron_Status__c = agStatus;
        ag.APTS_Path_Chevron_Status__c = agStatus;
        ag.Funding_Status__c = fundingStatus;
        ag.Document_Status__c = docStatus;
        return ag;
    }
    /**
     @description: DEMO CLM Automation implementation
     @param: oldAgMap - Map of Old Agreement records
     @param: agList - List of Corporate Trial agreements to process
     @return: void
    */
    public static void stampCTDates(Map<Id, SObject> oldAgMap, List<Apttus__APTS_Agreement__c> agList) {
        Map<Id, Date> payMap = new Map<Id, Date>();
        try{
            for(Payment_Entry__c paymentRec : [SELECT Agreement__c, Payment_Date__c FROM Payment_Entry__c 
                                               WHERE Agreement__c IN :oldAgMap.keySet()]) {
                                                   payMap.put(paymentRec.Agreement__c, paymentRec.Payment_Date__c);
                                               }
            for(Apttus__APTS_Agreement__c ag : agList){
                if(!payMap.isEmpty() && payMap.containsKey(ag.Id)){
                    ag.Funding_Date__c = payMap.get(ag.Id);
                    ag.Apttus__Contract_Start_Date__c = payMap.get(ag.Id);
                    if(ag.APTPS_Term_Months__c != null){
                        ag.Active_Until_Date__c = payMap.get(ag.Id) != null ? payMap.get(ag.Id).addMonths(Integer.valueOf(ag.APTPS_Term_Months__c)) : null;
                    }
                }
                if(ag.APTPS_Term_End_Date__c != null)
                    ag.Active_Until_Date__c = ag.APTPS_Term_End_Date__c;
            }
        } catch(Exception e) {
            system.debug('Error while calculating dates for Corporate trial. Error details --> '+e.getMessage());
        }
    }
}