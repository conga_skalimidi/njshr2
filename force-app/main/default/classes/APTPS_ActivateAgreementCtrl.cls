/*************************************************************
@Name: APTPS_ActivateAgreementCtrl
@Author: Siva Kumar
@CreateDate: 02 July 2021
@Description : This  class is called to Activate SNL Agreement
******************************************************************/
public class APTPS_ActivateAgreementCtrl {
@AuraEnabled
    public static string activateAgreement(string recordId)
    {
		String msg = '';
		List<Attachment> files = new List<Attachment>();
		List<String> activateDocIds = new List<String>();
		 try {
				files = [SELECT Id,Name, Body, ContentType FROM Attachment WHERE parentId = :recordId LIMIT 1];
				if(!files.isEmpty()) {							
				   activateDocIds.add(files[0].Id);				   
				}
				else {				
					Attachment dummyAttachment = new Attachment();
					dummyAttachment.Name = 'Test';
					dummyAttachment.Body = Blob.valueOf('Test');
					dummyAttachment.ContentType = 'text/plain';
					dummyAttachment.ParentId = recordId;
					insert dummyAttachment;
					activateDocIds.add(dummyAttachment.Id);				
				}

					 
				String[] remDocIds = new String[]{};
				Boolean response = Apttus.AgreementWebService.activateAgreement(recordId, activateDocIds, remDocIds);
				if(response) {
					msg = 'Success';
				} else {
					msg = 'Fail';
				}
					
			} catch(Exception e) {
				msg = 'Fail';
			}
		return msg;
    }
}