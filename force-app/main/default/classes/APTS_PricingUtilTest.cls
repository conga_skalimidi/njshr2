/*******************************************************************************************************************************************
@Name: APTS_PricingUtilTest
@Author: Avinash Bamane
@CreateDate: 30/10/2020
@Description: Test class for APTS_PricingUtil 
********************************************************************************************************************************************
@ModifiedBy: 25/03/2021
@ModifiedDate: Conga Dev Team
@ChangeDescription: Do cleanup for testsetup
********************************************************************************************************************************************/
@IsTest
public class APTS_PricingUtilTest {
    @testsetup
    static void setup(){
        List<Account> accList = new List<Account>();
        List<Opportunity> oppList = new List<Opportunity>();
        Account accToInsert = APTS_CPQTestUtility.createAccount('test apttus account', 'Enterprise');
        accList.add(accToInsert);
        
        Account acc2 = new Account();
        acc2.Name = 'Cross Account Src Account';
        acc2.APTS_Has_Active_NJUS_Agreement__c = true;
        acc2.APTS_Has_Active_NJE_Agreement__c = true;
        accList.add(acc2);
        
        insert accList;
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        oppList.add(testOpportunity);
        
        Opportunity testOpportunity1 = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity 1', 'New Customer', acc2.Id, 'Propose');
        oppList.add(testOpportunity1);
        insert oppList;
    }
    
    @IsTest
    static void testPricingUtil() {
        try{
            Test.startTest();
            Account testAccount = [SELECT Id FROM Account WHERE Name = 'test apttus account' LIMIT 1];
            
            Opportunity testOpportunity = [SELECT Id FROM Opportunity WHERE Name = 'test Apttus Opportunity' LIMIT 1];
            
            Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
            insert testPriceList;
            
            Product2 ascendProd = APTS_CPQTestUtility.createProduct('Ascend', 'ASCEND_NJUS', 'Product Type', 'Standalone', true, true, true, true);
            insert ascendProd;
            
            Product2 testProd = APTS_CPQTestUtility.createProduct('Test NJE', 'TEST_NJE', 'Product Type', 'Standalone', true, true, true, true);
            testProd.Program__c = APTS_ConstantUtil.NJ_EUROPE_PROGRAM;
            insert testProd;
            
            PricebookEntry ascendPBE = APTS_CPQTestUtility.createPricebookEntry(Test.getStandardPricebookId(), ascendProd.Id, 100, true);
            insert ascendPBE;
            
            Apttus_Config2__PriceListItem__c ascendPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, ascendProd.Id, 100, 'Standard Price', 'One Time', 'Per Unit', 'Each', true);
            insert ascendPLI;
            
            Apttus_Config2__PriceListItem__c testProdPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testProd.Id, 100, 'Standard Price', 'One Time', 'Per Unit', 'Each', true);
            insert testProdPLI;
            
            Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', testAccount.Id);
            insert testAccLocation;
            
            Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', testAccount.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
            insert testProposal;
            
            String testConfigurationId = APTS_CPQTestUtility.createConfiguration(testProposal.Id);
            
            Apttus_Config2__LineItem__c item = APTS_CPQTestUtility.getLineItem(testConfigurationId, testProposal, ascendPLI, ascendProd, ascendProd.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
            item.Apttus_Config2__ChargeType__c = 'Half Card Premium';
            item.APTPS_Referral_Referee_Type__c = APTS_ConstantUtil.REFEREE_LINE;
            insert item;
            
            Apttus_Config2__ProductAttributeValue__c pav = APTS_CPQTestUtility.createAttributeValue(item.Id);
            pav.Override_Requested_Hours__c = true;
            pav.APTS_Override_Requested_Hours__c = 12;
            pav.Requested_Hours__c = 2;
            pav.APTS_Override_Hours_To_Unexpire__c = true;
            pav.APTS_Hours_To_Unexpire__c = 3;
            pav.APTS_Overridden_Hours_To_Unexpire__c = 4;
            pav.APTS_Aircraft_Expired_Hours__c = 1;
            pav.APTS_Selected_Product_Type__c = APTS_ConstantUtil.REDEMPTION_PRODUCT_TYPE;
            pav.Aircraft_Hours__c = 25;
            pav.Aircraft_Remaining_Hours__c = 10;
            insert pav;
            
            system.assert(APTS_PricingUtil.isParentLine(item)==TRUE);
            system.assert(APTS_PricingUtil.isHalfCardPremiumCT(item)==TRUE);
            system.assert(APTS_PricingUtil.isAdjustmentApplied(item)==FALSE);
            system.assert(APTS_PricingUtil.isReferralRefereeLine(item)==TRUE);
            system.assert(APTS_PricingUtil.isAddEnhModification(APTS_ConstantUtil.ADD_ENHANCEMENTS)==TRUE);
            
            item.Apttus_Config2__AttributeValueId__c = pav.Id;
            item.Apttus_Config2__ChargeType__c = 'Combo Card Premium';
            update item;
            
            Apttus_Config2__LineItem__c testItem = [SELECT Id, Apttus_Config2__LineNumber__c, 
                                                    Apttus_Config2__AttributeValueId__r.Override_Requested_Hours__c, 
                                                    Apttus_Config2__AttributeValueId__r.APTS_Override_Requested_Hours__c, 
                                                    Apttus_Config2__AttributeValueId__r.Requested_Hours__c, 
                                                    Apttus_Config2__AttributeValueId__r.APTS_Override_Hours_To_Unexpire__c, 
                                                    Apttus_Config2__AttributeValueId__r.APTS_Hours_To_Unexpire__c, 
                                                    Apttus_Config2__AttributeValueId__r.APTS_Overridden_Hours_To_Unexpire__c, 
                                                    Apttus_Config2__AttributeValueId__r.APTS_Aircraft_Expired_Hours__c, 
                                                    Apttus_Config2__AttributeValueId__r.APTS_Selected_Product_Type__c, 
                                                    Apttus_Config2__AttributeValueId__r.Aircraft_Hours__c, 
                                                    Apttus_Config2__AttributeValueId__r.Aircraft_Remaining_Hours__c 
                                                    FROM Apttus_Config2__LineItem__c WHERE Id =: item.Id];
            
            system.assert(APTS_PricingUtil.getRequestedHours(testItem) > 0);
            system.assert(APTS_PricingUtil.getUnexpiredHours(testItem) > 0);
            system.assert(APTS_PricingUtil.getExpiredHours(testItem) > 0);
            system.assert(APTS_PricingUtil.isComboCardPremiumCT(item)==TRUE);
            
            system.assertEquals('Purchase Price', APTS_PricingUtil.getRelatedChargeType('Federal Excise Tax'));
            system.assertEquals('Half Card Premium', APTS_PricingUtil.getRelatedChargeType('Half Card Premium FET'));
            system.assertEquals('Combo Card Premium', APTS_PricingUtil.getRelatedChargeType('Combo Card Premium FET'));
            system.assertEquals('', APTS_PricingUtil.getRelatedChargeType('Test'));
            system.assertEquals(APTS_ConstantUtil.REDEMPTION_PRODUCT_TYPE, APTS_PricingUtil.getProductType(testItem));
            system.assert(APTS_PricingUtil.isRedemptionPT(testItem)==TRUE);
            
            Map<decimal, decimal> currentMap = new Map<decimal, decimal>();
            system.assert(APTS_PricingUtil.updateAircraftHoursMap(currentMap, testItem).get(testItem.Apttus_Config2__LineNumber__c)==25);
            currentMap = new Map<decimal, decimal>();
            system.assert(APTS_PricingUtil.updateRMNGAircraftHoursMap(currentMap, testItem).get(testItem.Apttus_Config2__LineNumber__c)==10);
            currentMap = new Map<decimal, decimal>();
            system.assert(APTS_PricingUtil.updateEXPAircraftHoursMap(currentMap, testItem).get(testItem.Apttus_Config2__LineNumber__c)==1);
            
            currentMap.put(testItem.Apttus_Config2__LineNumber__c, 25);
            system.assert(APTS_PricingUtil.updateAircraftHoursMap(currentMap, testItem).get(testItem.Apttus_Config2__LineNumber__c)==50);
            currentMap.put(testItem.Apttus_Config2__LineNumber__c, 25);
            system.assert(APTS_PricingUtil.updateRMNGAircraftHoursMap(currentMap, testItem).get(testItem.Apttus_Config2__LineNumber__c)==35);
            currentMap.put(testItem.Apttus_Config2__LineNumber__c, 25);
            system.assert(APTS_PricingUtil.updateEXPAircraftHoursMap(currentMap, testItem).get(testItem.Apttus_Config2__LineNumber__c)==26);
            
            pav.APTS_Selected_Product_Type__c = APTS_ConstantUtil.COMBO_PRODUCT_TYPE;
            update pav;
            
            Apttus_Config2__LineItem__c comboLI = [SELECT Id, Apttus_Config2__LineType__c, Apttus_Config2__AttributeValueId__r.APTS_Selected_Product_Type__c 
                                                   FROM Apttus_Config2__LineItem__c WHERE Id =: item.Id LIMIT 1];
            system.assert(APTS_PricingUtil.isComboPT(comboLI)==TRUE);
            
            APTS_PricingUtil.removeOrphanTLineItem(testConfigurationId, testAccount.Id);
            Apttus_Config2__LineItem__c njeLI = APTS_CPQTestUtility.getLineItem(testConfigurationId, testProposal, testProdPLI, testProd, testProd.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
            insert njeLI;
            Apttus_Config2__LineItem__c resultNJE = new Apttus_Config2__LineItem__c();
            resultNJE = [SELECT Id, Apttus_Config2__LineType__c, Apttus_Config2__ProductId__c, 
                         Apttus_Config2__ProductId__r.Program__c FROM Apttus_Config2__LineItem__c 
                         WHERE Id =: njeLI.Id LIMIT 1];
            system.assert(APTS_PricingUtil.isEuropeProgram(resultNJE)==TRUE);
            
            njeLI.Apttus_Config2__LineType__c = 'Option';
            update njeLI;
            resultNJE = [SELECT Id, Apttus_Config2__LineType__c, Apttus_Config2__ProductId__c, 
                         Apttus_Config2__ProductId__r.Program__c FROM Apttus_Config2__LineItem__c 
                         WHERE Id =: njeLI.Id LIMIT 1];
            system.assert(APTS_PricingUtil.isEuropeProgram(resultNJE)==TRUE);
            Test.stopTest();
        } catch (Exception e){
            System.debug(e.getStackTraceString());
        }
    }
    
    @isTest
    static void processPAVandLineITemTest(){
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();
        
        List<Apttus__APTS_Agreement__c> aggList = new List<Apttus__APTS_Agreement__c>();
        
        Account accToInsert = [SELECT Id, APTS_Has_Active_NJUS_Agreement__c, APTS_Has_Active_NJE_Agreement__c 
                               FROM Account WHERE Name = 'Cross Account Src Account' LIMIT 1];
        
        Opportunity testOpportunity = [SELECT Id FROM Opportunity WHERE Name = 'test Apttus Opportunity 1' LIMIT 1];
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
        
        Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        testProposal.APTS_New_Sale_or_Modification__c = 'Assignment';
        testProposal.APTS_Modification_Type__c = 'Cross-account assignment';
        insert testProposal;           
        
        Id testConfigurationId = APTS_CPQTestUtility.createConfiguration(testProposal.Id);
        Product2 testProduct = APTS_CPQTestUtility.createProduct('test Apttus Product', 'APTS', 'Apttus', 'Bundle', true, true, true, true);
        insert testProduct;
        
        Apttus_Config2__PriceListItem__c cardPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testProduct.Id, 0.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);
		insert cardPLI;
        
        Apttus__APTS_Agreement__c aggToInsert1 = new Apttus__APTS_Agreement__c();
        aggToInsert1.Name = 'APTS Test Agreement 1111';
        aggToInsert1.Program__c = 'NetJets U.S.';
        aggToInsert1.Apttus__Account__c = accToInsert.Id;
        aggToInsert1.Card_Number__c = '1324567890';
        aggToInsert1.APTS_Modification_Type__c = 'New Sale';
        aggToInsert1.RecordTypeId = recordTypeIdNJUS;
        aggToInsert1.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
        aggList.add(aggToInsert1);
        insert aggList;
        
        Apttus_Config2__AssetLineItem__c  assetLi = new Apttus_Config2__AssetLineItem__c ();
        assetLi.Apttus_CMConfig__AgreementId__c = aggList[0].Id;
        assetLi.Apttus_Config2__ChargeType__c = 'Purchase Price';
        assetLi.Apttus_Config2__LineType__c = 'Product/Service';
        assetLi.Apttus_Config2__IsPrimaryLine__c = TRUE;
        assetLi.Apttus_Config2__BillToAccountId__c = accToInsert.Id;
        assetLi.Apttus_Config2__ShipToAccountId__c = accToInsert.Id;
        assetLi.Apttus_Config2__AccountId__c = accToInsert.Id;
        insert assetLi;
        
        //Create New Asset Line Item
        Apttus_Config2__AssetLineItem__c  assetLi1 = new Apttus_Config2__AssetLineItem__c ();
        assetLi1.Apttus_CMConfig__AgreementId__c = aggList[0].Id;
        assetLi1.Apttus_Config2__ChargeType__c = 'Purchase Price';
        assetLi1.Apttus_Config2__LineType__c = 'Federal Excise Tax';
        assetLi1.Apttus_Config2__IsPrimaryLine__c = TRUE;
        assetLi1.Apttus_Config2__BundleAssetId__c = assetLi.Id;
        assetLi1.Apttus_Config2__BillToAccountId__c = accToInsert.Id;
        assetLi1.Apttus_Config2__ShipToAccountId__c = accToInsert.Id;
        assetLi1.Apttus_Config2__AccountId__c = accToInsert.Id;
        insert assetLi1;
        
        //Create Asset PAVs
        Apttus_Config2__AssetAttributeValue__c NCSCAseetAV = new Apttus_Config2__AssetAttributeValue__c();
        NCSCAseetAV.Apttus_Config2__AssetLineItemId__c = assetLi.Id;
        NCSCAseetAV.Aircraft_Remaining_Hours__c = 20;
        NCSCAseetAV.Requested_Hours__c = 10;
        insert NCSCAseetAV;
        assetLi.Apttus_Config2__AttributeValueId__c = NCSCAseetAV.id;
        update assetLi;
        
        Apttus_Config2__AssetAttributeValue__c NCSCAseetAV1 = new Apttus_Config2__AssetAttributeValue__c();
        NCSCAseetAV1.Apttus_Config2__AssetLineItemId__c = assetLi1.Id;
        NCSCAseetAV1.Aircraft_Remaining_Hours__c = 20;
        NCSCAseetAV1.Requested_Hours__c = 25;
        insert NCSCAseetAV1;
        assetLi1.Apttus_Config2__AttributeValueId__c = NCSCAseetAV1.id;
        update assetLi1;
        
        List<Apttus_Config2__LineItem__c> listLineItem = new List<Apttus_Config2__LineItem__c>();
        Apttus_Config2__LineItem__c li1 = APTS_CPQTestUtility.getLineItem(testConfigurationId, testProposal, cardPLI, testProduct, testProduct.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
        li1.Apttus_Config2__LineType__c = 'Product/Service';
        li1.Apttus_Config2__ChargeType__c = 'Purchase Price';
        li1.Apttus_Config2__AssetLineItemId__c = assetLi.Id;
        li1.Apttus_Config2__BillToAccountId__c = accToInsert.Id;
        li1.Apttus_Config2__ShipToAccountId__c = accToInsert.Id;
        listLineItem.add(li1);
        
        Apttus_Config2__LineItem__c li2 = APTS_CPQTestUtility.getLineItem(testConfigurationId, testProposal, cardPLI, testProduct, testProduct.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
        li2.Apttus_Config2__LineType__c = 'Product/Service';
        li2.Apttus_Config2__ChargeType__c = 'Federal Excise Tax';
        li2.Apttus_Config2__AssetLineItemId__c = assetLi1.Id;
        li2.Apttus_Config2__BillToAccountId__c = accToInsert.Id;
        li2.Apttus_Config2__ShipToAccountId__c = accToInsert.Id;
        listLineItem.add(li2);
        insert listLineItem;
        
        Apttus_Config2__ProductAttributeValue__c testProductAttributeValue = APTS_CPQTestUtility.createAttributeValue(listLineItem[0].Id);
        insert testProductAttributeValue;
        
        Apttus_Config2__ProductAttributeValue__c testProductAttributeValue1 = APTS_CPQTestUtility.createAttributeValue(listLineItem[1].Id);
        insert testProductAttributeValue1;

        //Start Test
        Test.startTest();
        APTS_PricingUtil.ProcessPAVandLineITem(testConfigurationId, accToInsert.Id);
        //Stop Test
        Test.stopTest();       
    }
}