/***********************************************************************************************************************
@Name: APTS_UpdateRefAgrQueuebleTest
@Author: Siva Kumar
@CreateDate: 27 October 2020
@Description: APEX class to test classes APTS_UpdateRefAgrQueueble
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
@isTest
public with sharing class APTS_UpdateRefAgrQueuebleTest {
    @isTest
    static void updateRefAgrQueuebleTest(){
       List<Account> accList = new List<Account>();
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();

        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
        accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
        insert accToInsert;

        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;

        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;

        Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal.APTS_Modification_Type__c = 'Expired Hours Card';
        insert testProposal;
        
         ID accID = accToInsert.Id;
        List<Apttus__APTS_Agreement__c> aggList = new List<Apttus__APTS_Agreement__c>();
        Apttus__APTS_Agreement__c aggToInsert = new Apttus__APTS_Agreement__c();
        aggToInsert.Name = 'APTS Test Agreement 1111';
        aggToInsert.Program__c = 'NetJets U.S.';
        aggToInsert.Apttus__Account__c = accID;

        aggToInsert.RecordTypeId = recordTypeIdNJUS;
        aggToInsert.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
        aggList.add(aggToInsert);

        insert aggList;
        
        //Create New Agreement Line Item
        Apttus__AgreementLineItem__c li = new Apttus__AgreementLineItem__c();
        li.Apttus__AgreementId__c = aggList[0].Id;
        li.Apttus__NetPrice__c = 1000;
        li.Option_Product_Family_System__c = 'Aircraft';
        li.Option_Product_Name__c = 'Citation Excel';
        insert li;
       
        

        //Start Test
        Test.startTest();
          System.enqueueJob(new APTS_UpdateRefAgrQueueble(aggList[0].Id, aggList[0].Name, 'Elite'));
        //Stop Test
        Test.stopTest();
    }

    
    
}