/***********************************************************************************************************************
@Name: LockCardNumberActionTest
@Author: Ramesh Kumar
@CreateDate: 02 Nov 2020
@Description: APEX class to test classes LockCardNumberActionTest
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
@isTest
public with sharing class LockCardNumberActionTest {
    @isTest
    static void setCardNumberTest(){
        List<Account> accList = new List<Account>();
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();

        List<Apttus__APTS_Agreement__c> aggList = new List<Apttus__APTS_Agreement__c>();

        Account accToInsert = new Account();
        accToInsert.Name = 'Cross Account Src Account';
        accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
        accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
        insert accToInsert;


        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;

        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;

        Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal.APTS_Modification_Type__c = 'Termination';
        insert testProposal;            
        
        
        Apttus__APTS_Agreement__c aggToInsert1 = new Apttus__APTS_Agreement__c();
        aggToInsert1.Name = 'APTS Test Agreement 1111';
        aggToInsert1.Program__c = 'NetJets U.S.';
        aggToInsert1.Apttus__Account__c = accToInsert.Id;
        aggToInsert1.APTS_Modification_Type__c = 'New Sale';
        aggToInsert1.RecordTypeId = recordTypeIdNJUS;
        aggToInsert1.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
        insert aggToInsert1;
                
  
        //Start Test
        Test.startTest();

        LockCardNumberAction.LockCardNumberRequest req = new LockCardNumberAction.LockCardNumberRequest();
        req.agreementId = aggToInsert1.Id;

        LockCardNumberAction.LockCardNumberRequest[] lockCardNumberRequest = new LockCardNumberAction.LockCardNumberRequest[]{req};
        LockCardNumberAction.setCardNumber(lockCardNumberRequest);
        //Stop Test
        Test.stopTest();
                
    }
    
    
    
    
}