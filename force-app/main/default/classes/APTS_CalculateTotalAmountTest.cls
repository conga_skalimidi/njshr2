/**
 * @description       : Test class for APTS_CalculateTotalAmount
 * @author            : Sagar Solanki
 * @group             : Apttus(Conga) Dev
 * @last modified on  : 10-26-2020
 * @last modified by  : Sagar Solanki
 *
 * Modifications Log
 * Ver   Date         Author          Modification
 * 1.0   10-22-2020   Sagar Solanki   Added test class : GCM-9780
 * 2.0	 03-25-2021	  Conga DEV Team  Do cleanup for testsetup
 **/
@isTest
public with sharing class APTS_CalculateTotalAmountTest{
    @testsetup
    static void setup(){
        Account accToInsert = new Account();
		accToInsert.Name = 'TestGenerateAcc-1';
		insert accToInsert;
    }
    
	@isTest
	static void calculateTestHalfCard(){
		//Create new account
		Account testaccount = [SELECT Id FROM Account WHERE Name = 'TestGenerateAcc-1' LIMIT 1];
		System.debug('TestAccount-:' + testaccount.ID);

		//Create New Agreement
		Apttus__APTS_Agreement__c testAgreement = new Apttus__APTS_Agreement__c();
		testAgreement.Name = 'TestGeneratedAGR-1';
		testAgreement.Apttus__Account__c = testaccount.Id;
		insert testAgreement;
		ID agreementid = testAgreement.ID;

		//Create New Agreement Line Item
		Apttus__AgreementLineItem__c li = new Apttus__AgreementLineItem__c();
		li.Apttus__AgreementId__c = agreementid;
		li.APTS_Modification_Type__c = 'Expired Hours Card';
		li.Apttus_CMConfig__ChargeType__c = 'Half Card Premium';
		li.Apttus__NetPrice__c = 1000;
		insert li;
		ID aliID = li.ID;
		List<ID> aliList = new List<ID>();
		aliList.add(aliID);

		//Start Test
		Test.startTest();
		try{
			APTS_CalculateTotalAmount.calculate(aliList);

			Apttus__APTS_Agreement__c agreementDetails = [SELECT Id, APTS_Total_Purchase_Price__c, Apttus__Account__c
			                                              FROM Apttus__APTS_Agreement__c
			                                              WHERE Id = :agreementid
			                                              LIMIT 1];

			System.assertEquals(1000, agreementDetails.APTS_Total_Purchase_Price__c);
		} catch (Exception e){
			System.debug(e.getStackTraceString());
		}
		//Stop Test
		Test.stopTest();
	}

	@isTest
	static void calculateTestComboCard(){
		//Create new account
		Account testaccount = [SELECT Id FROM Account WHERE Name = 'TestGenerateAcc-1' LIMIT 1];
		System.debug('TestAccount-:' + testaccount.ID);

		//Create New Agreement
		Apttus__APTS_Agreement__c testAgreement = new Apttus__APTS_Agreement__c();
		testAgreement.Name = 'TestGeneratedAGR-1';
		testAgreement.Apttus__Account__c = testaccount.Id;
		insert testAgreement;
		ID agreementid = testAgreement.ID;

		//Create New Agreement Line Item
		Apttus__AgreementLineItem__c li = new Apttus__AgreementLineItem__c();
		li.Apttus__AgreementId__c = agreementid;
		li.APTS_Modification_Type__c = 'Expired Hours Card';
		li.Apttus_CMConfig__ChargeType__c = 'Combo Card Premium';
		li.Apttus__NetPrice__c = 1000;
		insert li;
		ID aliID = li.ID;
		List<ID> aliList = new List<ID>();
		aliList.add(aliID);

		//Start Test
		Test.startTest();
		try{
			APTS_CalculateTotalAmount.calculate(aliList);
			Apttus__APTS_Agreement__c agreementDetails = [SELECT Id, APTS_Total_Purchase_Price__c, Apttus__Account__c
			                                              FROM Apttus__APTS_Agreement__c
			                                              WHERE Id = :agreementid
			                                              LIMIT 1];

			System.assertEquals(1000, agreementDetails.APTS_Total_Purchase_Price__c);
		} catch (Exception e){
			System.debug(e.getStackTraceString());
		}
		//Stop Test
		Test.stopTest();
	}

	@isTest
	static void calculateTestNewCardSale(){
		//Create new account
		Account testaccount = [SELECT Id FROM Account WHERE Name = 'TestGenerateAcc-1' LIMIT 1];
		System.debug('TestAccount-:' + testaccount.ID);

		//Create New Agreement
		Apttus__APTS_Agreement__c testAgreement = new Apttus__APTS_Agreement__c();
		testAgreement.Name = 'TestGeneratedAGR-1';
		testAgreement.Apttus__Account__c = testaccount.Id;
		testAgreement.APTS_Modification_Type__c = 'New Card Sale Conversion';
		insert testAgreement;
		ID agreementid = testAgreement.ID;

		List<ID> agreementLineIDs = new List<ID>();
		//Create New Agreement Line Item
		Apttus__AgreementLineItem__c li = new Apttus__AgreementLineItem__c();
		li.Apttus__AgreementId__c = agreementid;
		li.APTS_Modification_Type__c = 'New Card Sale Conversion';
		li.Apttus_CMConfig__ChargeType__c = 'Purchase Price';
		li.Apttus__NetPrice__c = 1000;
		li.Apttus_CMConfig__LineType__c = 'Product/Service';
		li.Apttus_CMConfig__LineStatus__c = 'New';
		insert li;
		ID aliID = li.ID;
		agreementLineIDs.add(aliID);

		Apttus__AgreementLineItem__c li1 = new Apttus__AgreementLineItem__c();
		li1.Apttus__AgreementId__c = agreementid;
		li1.APTS_Modification_Type__c = 'New Card Sale Conversion';
		li1.Apttus_CMConfig__ChargeType__c = 'Federal Excise Tax';
		li1.Apttus__NetPrice__c = 500;
		li1.Apttus_CMConfig__LineType__c = 'Product/Service';
		li1.Apttus_CMConfig__LineStatus__c = 'New';
		insert li1;
		ID aliID1 = li1.ID;
		agreementLineIDs.add(aliID1);

		//Start Test
		Test.startTest();
		try{
			APTS_CalculateTotalAmount.calculate(agreementLineIDs);
			Apttus__APTS_Agreement__c agreementDetails = [SELECT Id, APTS_Total_Purchase_Price__c, Apttus__Account__c
			                                              FROM Apttus__APTS_Agreement__c
			                                              WHERE Id = :agreementid
			                                              LIMIT 1];

			System.assertEquals(1500, agreementDetails.APTS_Total_Purchase_Price__c);
		} catch (Exception e){
			System.debug(e.getStackTraceString());
		}
		//Stop Test
		Test.stopTest();
	}
}