/**
 * @description       : Test class for APTS_ReconfigureCartPageController
 * @author            : Sagar Solanki
 * @group             : Apttus(Conga) Dev
 * @last modified on  : 10-22-2020
 * @last modified by  : Sagar Solanki
 * 
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   10-22-2020   Sagar Solanki   Added test class : GCM-9780 
**/
@isTest
public with sharing class APTS_ReconfigureCartPageControllerTest {
    @isTest
    static void testUrlRedirectionForEHC(){
        test.startTest();
        PageReference pageRef = Page.Apttus_QPConfig__ProposalConfiguration;

        Apttus_Proposal__Proposal__c  testProposal = new Apttus_Proposal__Proposal__c();
        testProposal.Apttus_Proposal__Approval_Stage__c='Approval Required';
        testProposal.APTPS_Program_Type__c = 'Card';
        testProposal.APTS_Modification_Type__c='Expired Hours Card';
        insert testProposal;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',testProposal.id);
        ApexPages.StandardController sc = new ApexPages.standardController(testProposal);
        APTS_ReconfigureCartPageController controller = new APTS_ReconfigureCartPageController(sc);
        System.assertNotEquals(null,controller.urlRedirection(),'URL Redirection test for EHC');

        test.stopTest();
    }
    @isTest
    static void testUrlRedirectionForTermExtension(){
        test.startTest();
        PageReference pageRef = Page.Apttus_QPConfig__ProposalConfiguration;

        Apttus_Proposal__Proposal__c  testProposal = new Apttus_Proposal__Proposal__c();
        testProposal.Apttus_Proposal__Approval_Stage__c='Approval Required';
        testProposal.APTPS_Program_Type__c = 'Card';
        testProposal.APTS_Modification_Type__c='Term Extension';
        insert testProposal;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',testProposal.id);
        ApexPages.StandardController sc = new ApexPages.standardController(testProposal);
        APTS_ReconfigureCartPageController controller = new APTS_ReconfigureCartPageController(sc);
        System.assertNotEquals(null,controller.urlRedirection(), 'URL Redirection test for Term Extension');

        test.stopTest();
    }
    @isTest
    static void testUrlRedirectionOther(){
        test.startTest();
        PageReference pageRef = Page.Apttus_QPConfig__ProposalConfiguration;

        Apttus_Proposal__Proposal__c  testProposal = new Apttus_Proposal__Proposal__c();
        testProposal.Apttus_Proposal__Approval_Stage__c='Approval Required';
        testProposal.APTPS_Program_Type__c = 'Card';
        testProposal.APTS_Modification_Type__c='Term';
        insert testProposal;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',testProposal.id);
        ApexPages.StandardController sc = new ApexPages.standardController(testProposal);
        APTS_ReconfigureCartPageController controller = new APTS_ReconfigureCartPageController(sc);
        System.assertNotEquals(null,controller.urlRedirection(),'Tested URL redirection for other modification types');

        test.stopTest();
    }
    
    @isTest
    static void testShareRedirection() {
        Test.startTest();
        PageReference pageRef = Page.Apttus_QPConfig__ProposalConfiguration;
        Apttus_Proposal__Proposal__c  testProposal = new Apttus_Proposal__Proposal__c();
        testProposal.Apttus_Proposal__Approval_Stage__c='Approval Required';
        testProposal.APTPS_Program_Type__c = 'Share';
        testProposal.Contract_Request__c = true;
        //testProposal.APTS_Modification_Type__c='Term';
        insert testProposal;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',testProposal.id);
        ApexPages.StandardController sc = new ApexPages.standardController(testProposal);
        APTS_ReconfigureCartPageController controller = new APTS_ReconfigureCartPageController(sc);
        System.assertNotEquals(null,controller.urlRedirection(),'Tested URL redirection for other modification types');
        Test.stopTest();
    }
}