public with sharing class APTS_ProposalApprovalEmailController {
    public string ApprovalRequestObjId {get; set;}
    public Apttus_Approval__Approval_Request__c ApprovalRequestObj {get; private set;}
    public string instanceurl {get; private set;}
    public String ProposalId{get; set;}
    public Boolean notifyOnly{get; set;}
    public Apttus_Proposal__Proposal__c proposalSO{get;private set;}
    public String strError{get;set;}
    private final String NL = '\n';
    public Map<String, String> mpStrFieldsLabels_Proposal {get; private set;}
    public List<RowEntry> rePLI{get; private set;}
    public Boolean isTesting{get;private set;}

    public APTS_ProposalApprovalEmailController() {
        String str = NL;
        try {
            instanceurl = Apttus_Approval__ApprovalsSystemProperties__c.getvalues('System Properties').Apttus_Approval__InstanceUrl__c;
            isTesting = false;
        } catch(Exception e){
            str += 'ERR: ' + e;
            formException(e);
            System.debug(LoggingLevel.ERROR, strError);
        }
    }

    public class RowEntry{
        public String label{get;private set;}
        public String value{get;private set;}
        public Boolean isHeader{get;private set;}
        public Boolean isHTML{get;private set;}

        public RowEntry(final String lbl, final String val){
            this(lbl, val, false, false); // Default no header
        }
        public RowEntry(final String lbl, final String val,final Boolean isHTML){
            this(lbl, val, false, isHTML); // Default no header
        }
        public RowEntry(final String lbl, final String val, final Boolean isHeader,final Boolean isHTML){
            label = lbl;
            value = val;
            this.isHeader = isHeader;
            this.isHTML = isHTML;
        }
    }

    private void formException(Exception e){
        strError = 'Msg: ' + e.getMessage()
            +NL + 'Type: ' + e.getTypeName()
            +NL + 'Stack: ' + e.getStackTraceString();
    }

    public Apttus_Approval__Approval_Request__c getApprovalRequest() {
        String str = NL;

        try{
            str += NL + 'inside: getApprovalRequest';
            str += NL + 'ProposalId: ' + ProposalId;
            str += NL + 'Proposal: ' + proposalSO;

            List<Apttus_Approval__Approval_Request__c> lstApprovalRequest;
            ApprovalRequestObj = new Apttus_Approval__Approval_Request__c();

            if(ApprovalRequestObjId != null){
                lstApprovalRequest = [ Select Id, Apttus_Approval__Initial_Submitter__r.name, Apttus_Approval__Assigned_To_Id__c,Apttus_Approval__SubmissionComment1__c,
                                    Apttus_Approval__Approval_Status__c, Apttus_Approval__DateAssigned__c, Apttus_Approval__Assigned_To_Name__c,Apttus_Approval__Notify_Only__c,
                                    Apttus_Approval__Request_Comments__c, Apttus_Approval__Step_Name__c, Apttus_QPApprov__ProposalId__r.Apttus_Proposal__Account__c,Apttus_QPApprov__ProposalId__c, Apttus_QPApprov__ProposalId__r.Name,Apttus_Approval__Approver_Comments__c,
                                    Apttus_QPApprov__ProposalId__r.Apttus_Proposal__Proposal_Name__c,Apttus_QPApprov__ProposalId__r.Apttus_Proposal__Account__r.name
                                    from Apttus_Approval__Approval_Request__c where Id = :ApprovalRequestObjId limit 1];
            } else{
                lstApprovalRequest = [ Select Id, Apttus_Approval__Initial_Submitter__r.name, Apttus_Approval__Assigned_To_Id__c,Apttus_Approval__SubmissionComment1__c,
                                    Apttus_Approval__Approval_Status__c, Apttus_Approval__DateAssigned__c, Apttus_Approval__Assigned_To_Name__c,Apttus_Approval__Notify_Only__c,
                                    Apttus_Approval__Request_Comments__c, Apttus_Approval__Step_Name__c, Apttus_QPApprov__ProposalId__r.Apttus_Proposal__Account__c,Apttus_QPApprov__ProposalId__c, Apttus_QPApprov__ProposalId__r.Name,Apttus_Approval__Approver_Comments__c,
                                    Apttus_QPApprov__ProposalId__r.Apttus_Proposal__Proposal_Name__c,Apttus_QPApprov__ProposalId__r.Apttus_Proposal__Account__r.name
                                    from Apttus_Approval__Approval_Request__c
                                    where Apttus_QPApprov__ProposalId__c = :ProposalId
                                    and Apttus_Approval__Sequence__c <> null
                                    order by LastModifiedDate desc
                                    limit 1];
            }

            if(lstApprovalRequest != null && !lstApprovalRequest.isEmpty()){
                ApprovalRequestObj = lstApprovalRequest[0];
            }

            if( Apttus_Approval__ApprovalsSystemProperties__c.getvalues('System Properties') != null && Apttus_Approval__ApprovalsSystemProperties__c.getvalues('System Properties').Apttus_Approval__InstanceUrl__c != null ) {
                instanceurl = Apttus_Approval__ApprovalsSystemProperties__c.getvalues('System Properties').Apttus_Approval__InstanceUrl__c;
            }
        }catch(Exception e){
            //str += 'ERR: ' + e;
            formException(e);
            System.debug(LoggingLevel.ERROR, strError);
        }

        strError += str;

        return ApprovalRequestObj;
    }

    public Apttus_Proposal__Proposal__c getProposal(){
        String str = NL;
        try{
            mpStrFieldsLabels_Proposal = getFieldsLabels('Apttus_Proposal__Proposal__c');
            str += NL + 'inside: getProposal';
            str += NL + 'ProposalId: ' + ProposalId;
            str += NL + 'Proposal: ' + proposalSO;

            if(proposalSO == null){
                List<Apttus_Proposal__Proposal__c> proposals = [SELECT ID,
                                                                Name,
                                                                Apttus_Proposal__Proposal_Name__c,
                                                                Apttus_Proposal__Account__r.Name,
                                                                Apttus_QPConfig__LegalEntityId__r.Name,
                                                                Apttus_QPConfig__LocationId__r.Name,
                                                                Apttus_Proposal__Primary_Contact__r.Name,
                                                                Approval_Primary_SE_User__r.Name,
                                                                Approval_Primary_AE_User__r.Name,
                                                                Approval_RVP_User__r.Name,
                                                                Deal_Description__c,
                                                                Deal_Background__c,
                                                                Approval_Reason__c,
                                                                APTS_Transaction_Type__c
                                                                FROM Apttus_Proposal__Proposal__c WHERE Id = :ProposalId];
                str += NL + 'proposals.size(): ' + proposals.size();
                if(!proposals.isEmpty()) {
                    proposalSO = proposals[0];
                } else {
                    system.debug('error in finding the proposal by id ' + ProposalId + 'detail: ' + str);
                    return null;
                }
            }

            rePLI = new List<RowEntry>();
            rePLI.add(new RowEntry(mpStrFieldsLabels_Proposal.get('Apttus_Proposal__Account__c'), proposalSO.Apttus_Proposal__Account__r.Name));
            rePLI.add(new RowEntry('Legal Entity (ReadOnly)', proposalSO.Apttus_QPConfig__LocationId__r.Name));
            //rePLI.add(new RowEntry('Legal Entity Name', proposalSO.Apttus_QPConfig__LegalEntityId__r.Name));
            rePLI.add(new RowEntry(mpStrFieldsLabels_Proposal.get('Apttus_Proposal__Primary_Contact__c'), proposalSO.Apttus_Proposal__Primary_Contact__r.Name));
            rePLI.add(new RowEntry('Primary Sales Executive', proposalSO.Approval_Primary_SE_User__r.Name));
            rePLI.add(new RowEntry('Primary Account Executive', proposalSO.Approval_Primary_AE_User__r.Name));
            rePLI.add(new RowEntry('Regional Vice President', proposalSO.Approval_RVP_User__r.Name));
            rePLI.add(new RowEntry('Approval Submission Comments', ApprovalRequestObj.Apttus_Approval__SubmissionComment1__c));
            rePLI.add(new RowEntry('Opportunity Type', proposalSO.APTS_Transaction_Type__c));
            //rePLI.add(new RowEntry(mpStrFieldsLabels_Proposal.get('Opportunity Type'), proposalSO.APTS_Transaction_Type__c, true));
            rePLI.add(new RowEntry(mpStrFieldsLabels_Proposal.get('Deal_Description__c'), proposalSO.Deal_Description__c, true));

        }catch(Exception e){
            str += 'ERR: ' + e;
            formException(e);
            System.debug(LoggingLevel.ERROR, strError);
        }

        strError += str;

        return proposalSO;
    }

    public static Map<String, String> getFieldsLabels(final String type) {
        Map<String, String> mpFieldLabel = new Map<String, String>();
        final SObjectType soType = Schema.getGlobalDescribe().get(type);
        final List<Schema.SObjectField> mfields = soType.getDescribe().fields.getMap().values();
        for (Schema.SObjectField s : mfields)
          {
            final Schema.DescribeFieldResult dfr = s.getDescribe();
            mpFieldLabel.put(dfr.getName(), dfr.getLabel());
          }
        return mpFieldLabel;
      }
}