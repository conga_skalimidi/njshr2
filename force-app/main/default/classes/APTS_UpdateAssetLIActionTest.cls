/***********************************************************************************************************************
@Name: APTS_UpdateAssetLIActionTest
@Author: Siva Kumar
@CreateDate: 26 October 2020
@Description: APEX class to test classes APTS_UpdateAssetLineItemAction
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
@isTest
public with sharing class APTS_UpdateAssetLIActionTest {
    @isTest
    static void updateAssetLIActionTest(){
       List<Account> accList = new List<Account>();
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();

        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
        accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
        insert accToInsert;

        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;

        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;

        Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal.APTS_Modification_Type__c = 'Expired Hours Card';
        insert testProposal;

        

        //Start Test
        Test.startTest();
        APTS_UpdateAssetLineItemAction  updateALIAction  = new APTS_UpdateAssetLineItemAction ();
        APTS_UpdateAssetLineItemAction.UpdateAssetLineItemRequest updateALIreq = new APTS_UpdateAssetLineItemAction.UpdateAssetLineItemRequest();
        updateALIreq.vProposalId = testProposal.Id;
        updateALIreq.vNJAssetStatus = 'In Modification';
        updateALIreq.vALIAgreementStatus = 'Inactive';
        updateALIreq.vAgreementStatus = 'In Modification';
        APTS_UpdateAssetLineItemAction.updateParentAssetAgreements (new List<APTS_UpdateAssetLineItemAction.UpdateAssetLineItemRequest>{updateALIreq});
        //Stop Test
        Test.stopTest();
    }

    
    
}