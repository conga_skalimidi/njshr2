/*************************************************************
@Name: APTPS_AccountLocationTriggerHandler
@Author: Siva Kumar
@CreateDate: 21 October 2020
@Description : This class is used to prevent Duplicate values for Account Location
******************************************************************/
public class APTPS_AccountLocationTriggerHandler extends TriggerHandler{
  
    public override void beforeInsert(){
        APTPS_AccountLocation_Helper.AvoidDuplicate(Trigger.new,null);
        
    } 
    
    public override void beforeUpdate(){
        Map<id,Apttus_Config2__AccountLocation__c> oldMap = new  Map<id,Apttus_Config2__AccountLocation__c>();
        for(id leId :Trigger.oldMap.keyset()){
            oldMap.put(leId,(Apttus_Config2__AccountLocation__c)Trigger.oldMap.get(leId ));
        }
        APTPS_AccountLocation_Helper.AvoidDuplicate(Trigger.new,oldMap);
    } 
    
}