/***********************************************************************************************************************
@Name: NJ_AssignmentModificationActionTest
@Author: Ramesh Kumar
@CreateDate: 01 Nov 2020
@Description: APEX class to test classes NJ_AssignmentModificationAction
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
@isTest
public with sharing class NJ_AssignmentModificationActionTest {
    @isTest
    static void createCrossAccountAssetLineItemsTest(){
        List<Account> accList = new List<Account>();
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();

        List<Apttus__APTS_Agreement__c> aggList = new List<Apttus__APTS_Agreement__c>();

        Account srcAccToInsert = new Account();
        srcAccToInsert.Name = 'Cross Account Src Account';
        srcAccToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
        srcAccToInsert.APTS_Has_Active_NJE_Agreement__c = true;
        insert srcAccToInsert;

        Account trgAccToInsert = new Account();
        trgAccToInsert.Name = 'Cross Account Trg Account';
        trgAccToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
        trgAccToInsert.APTS_Has_Active_NJE_Agreement__c = true;
        insert trgAccToInsert;

        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', srcAccToInsert.Id, 'Propose');
        insert testOpportunity;

        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;

        Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', srcAccToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal.APTS_Modification_Type__c = 'Termination';
        insert testProposal;            
        
        
        Apttus__APTS_Agreement__c aggToInsert1 = new Apttus__APTS_Agreement__c();
        aggToInsert1.Name = 'APTS Test Agreement 1111';
        aggToInsert1.Program__c = 'NetJets U.S.';
        aggToInsert1.Apttus__Account__c = srcAccToInsert.Id;
        aggToInsert1.Card_Number__c = '1324567890';
        aggToInsert1.APTS_Modification_Type__c = 'New Sale';
        aggToInsert1.RecordTypeId = recordTypeIdNJUS;
        aggToInsert1.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
        aggList.add(aggToInsert1);
        insert aggList;
                
        Apttus_Config2__AssetLineItem__c  assetLi = new Apttus_Config2__AssetLineItem__c ();
        assetLi.Apttus_CMConfig__AgreementId__c = aggList[0].Id;
        assetLi.Apttus_Config2__ChargeType__c = 'Purchase Price';
        assetLi.Apttus_Config2__LineType__c = 'Product/Service';
        assetLi.Apttus_Config2__IsPrimaryLine__c = TRUE;
        insert assetLi;
        
        //Create New Asset Line Item
        Apttus_Config2__AssetLineItem__c  assetLi1 = new Apttus_Config2__AssetLineItem__c ();
        assetLi1.Apttus_CMConfig__AgreementId__c = aggList[0].Id;
        assetLi1.Apttus_Config2__ChargeType__c = 'Purchase Price';
        assetLi1.Apttus_Config2__LineType__c = 'Product/Service';
        assetLi1.Apttus_Config2__IsPrimaryLine__c = TRUE;
        assetLi1.Apttus_Config2__BundleAssetId__c = assetLi.Id;
        insert assetLi1;
        
        //Create Asset PAVs
        Apttus_Config2__AssetAttributeValue__c NCSCAseetAV = new Apttus_Config2__AssetAttributeValue__c();
        NCSCAseetAV.Apttus_Config2__AssetLineItemId__c = assetLi.Id;
        NCSCAseetAV.Aircraft_Remaining_Hours__c = 20;
        NCSCAseetAV.Requested_Hours__c = 10;
        insert NCSCAseetAV;
        assetLi.Apttus_Config2__AttributeValueId__c = NCSCAseetAV.id;
        update assetLi;
   
    
         Apttus_Config2__AssetAttributeValue__c NCSCAseetAV1 = new Apttus_Config2__AssetAttributeValue__c();
         NCSCAseetAV1.Apttus_Config2__AssetLineItemId__c = assetLi1.Id;
         NCSCAseetAV1.Aircraft_Remaining_Hours__c = 20;
         NCSCAseetAV1.Requested_Hours__c = 25;
         insert NCSCAseetAV1;
         assetLi1.Apttus_Config2__AttributeValueId__c = NCSCAseetAV1.id;
         update assetLi1;

  
        //Start Test
        Test.startTest();

        NJ_AssignmentModificationAction.NJ_AssignmentModificationRequest req = new NJ_AssignmentModificationAction.NJ_AssignmentModificationRequest();
        req.targetAccountId = trgAccToInsert.Id;
        req.bundleAssetId = assetLi.Id;
        NJ_AssignmentModificationAction.NJ_AssignmentModificationRequest[] assignmentRequest = new NJ_AssignmentModificationAction.NJ_AssignmentModificationRequest[]{req};

        NJ_AssignmentModificationAction.ExecuteCAAssignmentProcess(assignmentRequest);
        //Stop Test
        Test.stopTest();
                
    }
    
    
    
    
}