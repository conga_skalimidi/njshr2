/************************************************************************************************************************
@Name: APTPS_CLMChildAgreementsCallable
@Author: Conga PS Dev Team
@CreateDate: 16 September 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public interface APTPS_CLMChildAgreementsCallable {
    Object call(Map<String, set<string>> args);
}