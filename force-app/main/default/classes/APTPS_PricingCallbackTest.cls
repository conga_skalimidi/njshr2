/**
 * @description       : Test class for APTPS_PricingCallback
 * @author            : Shivam Aditya
 * @group             :
 * @last modified on  : 10-29-2020
 * @last modified by  : Sagar Solanki
 * Modifications Log
 * Ver   Date         Author          Modification
 * 1.0   10-29-2020   Shivam Aditya   Initial Version
 **/
@isTest
private class APTPS_PricingCallbackTest{
	@isTest
	static void TestPCB(){
		// Create Settigs
		Apttus_Config2__ConfigCustomClasses__c configCustomClasses = new Apttus_Config2__ConfigCustomClasses__c();
		configCustomClasses.Name = 'Custom Classes';
		configCustomClasses.Apttus_Config2__PricingCallbackClass__c = 'APTPS_PricingCallback';
		insert configCustomClasses;

		insert APTS_CPQTestUtility.getConfigLineItemFields();

		Apttus_Config2__ConfigSystemProperties__c configSystemProperties = new Apttus_Config2__ConfigSystemProperties__c();
		configSystemProperties.Name = 'System Properties';
		configSystemProperties.Apttus_Config2__PricingProfile__c = 'Advanced';
		configSystemProperties.Apttus_Config2__PricingBatchSize__c = 2;
		configSystemProperties.Apttus_Config2__OptionPricingChunkSize__c = 100;
		insert configSystemProperties;
        
        APTPS_Pricing_Callback__c fetDetails = new APTPS_Pricing_Callback__c();
        fetDetails.FET__c = 0.075;
        insert fetDetails;

		Account testAccount = APTS_CPQTestUtility.createAccount('Test PCB', 'Other');
		insert testAccount;
        
        Contact testContact = APTS_CPQTestUtility.createContact('ContactFN', 'PCB Contact', testAccount.Id);
        insert testContact;

		Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('New Sale Opportunity', 'New', testAccount.Id, 'Closed Won');
		insert testOpportunity;

		Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', testAccount.Id);
		insert testAccLocation;

		Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('USA', true);
		insert testPriceList;

		list<Product2> lstProducts = new list<Product2>();

		// Insert Card Bundle (US)
		Product2 testCardProduct = APTS_CPQTestUtility.createProduct('Card', 'NJUS_CARD', 'Netjets Product', 'Bundle', true, true, true, true);
		testCardProduct.Program__c = 'NetJets U.S.';

		// Insert Product Type Option (US)
		Product2 testECProduct = APTS_CPQTestUtility.createProduct('Elite Combo', 'ELITE_COMBO_NJUS', 'Product Type', 'Option', true, true, true, true);
		testECProduct.Program__c = 'NetJets U.S.';
		testECProduct.Cabin_Class__c = 'Mid-Size';

		// Insert Aircraft 1 Option (US)
		Product2 testAircraft1Product = APTS_CPQTestUtility.createProduct('Gulfstream 450', 'GULFSTREAM_450_NJUS', 'Aircraft', 'Option', true, true, true, true);
		testAircraft1Product.Program__c = 'NetJets U.S.';
		testAircraft1Product.Cabin_Class__c = 'Large';

		// Insert Aircraft 2 Option (US)
		Product2 testAircraft2Product = APTS_CPQTestUtility.createProduct('Citation Sovereign', 'CITATION_SOVEREIGN_NJUS', 'Aircraft', 'Option', true, true, true, true);
		testAircraft2Product.Program__c = 'NetJets U.S.';
		testAircraft2Product.Cabin_Class__c = 'Mid-Size';
        
        Product2 enhancementProduct = APTS_CPQTestUtility.createProduct('Enhancement', 'Enhancements', 'Enhancement', 'Option', true, true, true, true);
		enhancementProduct.Program__c = 'NetJets U.S.';

		lstProducts.add(testCardProduct);
		lstProducts.add(testECProduct);
		lstProducts.add(testAircraft1Product);
		lstProducts.add(testAircraft2Product);
        lstProducts.add(enhancementProduct);
		insert lstProducts;


		Apttus_Config2__ClassificationName__c categorySo = APTS_CPQTestUtility.createCategory('Netjets Products', 'Netjets Products', 'Offering', null, true);
		insert categorySo;


		list<Apttus_Config2__PriceListItem__c> lstPLI = new list<Apttus_Config2__PriceListItem__c>();

		//Card PLI
		Apttus_Config2__PriceListItem__c cardPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testCardProduct.Id, 0.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);

		//Elite Combo PLI's
		Apttus_Config2__PriceListItem__c ecPLI1 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testECProduct.Id, 0.0, 'Combo Card Premium FET', 'One Time', 'Flat Price', 'Each', true);
		Apttus_Config2__PriceListItem__c ecPLI2 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testECProduct.Id, 0.0, 'Product Type', 'One Time', 'Flat Price', 'Each', true);
		Apttus_Config2__PriceListItem__c ecPLI3 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testECProduct.Id, 0.0, 'Combo Card Premium', 'One Time', 'Per Unit', 'Each', true);

		//Gulfstream 450 PLI's
		Apttus_Config2__PriceListItem__c GSPLI1 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft1Product.Id, 0.0, 'Existing Asset', 'One Time', 'Flat Price', 'Each', true);
		Apttus_Config2__PriceListItem__c GSPLI2 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft1Product.Id, 0.0, 'Half Card Premium FET', 'One Time', 'Flat Price', 'Each', true);
		Apttus_Config2__PriceListItem__c GSPLI3 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft1Product.Id, 0.0, 'Half Card Premium', 'One Time', 'Per Unit', 'Each', true);
		Apttus_Config2__PriceListItem__c GSPLI4 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft1Product.Id, 0.0, 'Request Hours Value', 'One Time', 'Per Unit', 'Each', true);
		Apttus_Config2__PriceListItem__c GSPLI5 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft1Product.Id, 16796.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);
		Apttus_Config2__PriceListItem__c GSPLI6 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft1Product.Id, 0.0, 'Federal Excise Tax', 'One Time', 'Per Unit', 'Each', true);
		Apttus_Config2__PriceListItem__c GSPLI7 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft1Product.Id, 0.0, 'Expired Hours Value', 'One Time', 'Per Unit', 'Each', true);

		//Citation Sovereign PLI's
		Apttus_Config2__PriceListItem__c CSPLI1 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft2Product.Id, 0.0, 'Existing Asset', 'One Time', 'Flat Price', 'Each', true);
		Apttus_Config2__PriceListItem__c CSPLI2 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft2Product.Id, 0.0, 'Half Card Premium FET', 'One Time', 'Flat Price', 'Each', true);
		Apttus_Config2__PriceListItem__c CSPLI3 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft2Product.Id, 0.0, 'Half Card Premium', 'One Time', 'Per Unit', 'Each', true);
		Apttus_Config2__PriceListItem__c CSPLI4 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft2Product.Id, 0.0, 'Request Hours Value', 'One Time', 'Per Unit', 'Each', true);
		Apttus_Config2__PriceListItem__c CSPLI5 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft2Product.Id, 9796.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);
		Apttus_Config2__PriceListItem__c CSPLI6 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft2Product.Id, 0.0, 'Federal Excise Tax', 'One Time', 'Per Unit', 'Each', true);
		Apttus_Config2__PriceListItem__c CSPLI7 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft2Product.Id, 0.0, 'Expired Hours Value', 'One Time', 'Per Unit', 'Each', true);
        
        Apttus_Config2__PriceListItem__c enhanPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, enhancementProduct.Id, 0.0, 'Enhancements', 'One Time', 'Per Unit', 'Each', true);

		lstPLI.add(cardPLI);
		lstPLI.add(ecPLI1);
		lstPLI.add(ecPLI2);
		lstPLI.add(ecPLI3);
		lstPLI.add(GSPLI1);
		lstPLI.add(GSPLI2);
		lstPLI.add(GSPLI3);
		lstPLI.add(GSPLI4);
		lstPLI.add(GSPLI5);
		lstPLI.add(GSPLI6);
		lstPLI.add(GSPLI7);
		lstPLI.add(CSPLI1);
		lstPLI.add(CSPLI2);
		lstPLI.add(CSPLI3);
		lstPLI.add(CSPLI4);
		lstPLI.add(CSPLI5);
		lstPLI.add(CSPLI6);
		lstPLI.add(CSPLI7);
        lstPLI.add(enhanPLI);

		insert lstPLI;

		//Create proposal
		/*Apttus_Proposal__Proposal__c nsProposal = APTS_CPQTestUtility.createProposal('New Sale Test Quote', testAccount.Id, testOpportunity.Id, 'Proposal NJUS', testPriceList.Id);
		nsProposal.APTS_New_Sale_or_Modification__c = 'Modification';
		nsProposal.APTS_Modification_Type__c = 'Direct Conversion';
		nsProposal.APTS_Additional_Modification_Type__c = 'Term Extension';
		insert nsProposal;*/
        
        Apttus_Proposal__Proposal__c nsProposal = APTS_CPQTestUtility.createProposal('New Sale Test Quote', testAccount.Id, testOpportunity.Id, 'Proposal NJUS', testPriceList.Id);
		nsProposal.APTS_New_Sale_or_Modification__c = 'New Sale';
		insert nsProposal;

		// Add Configuration
		Id configuration = APTS_CPQTestUtility.createConfiguration(nsProposal.Id);

		List<Apttus_Config2__LineItem__c> lineitemList = new List<Apttus_Config2__LineItem__c>();

		Apttus_Config2__LineItem__c cardLineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
		cardLineItem.Apttus_Config2__PricingStatus__c = 'Pending';
		lineitemList.add(cardLineItem);
		Apttus_Config2__LineItem__c ECLineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, ecPLI2, testCardProduct, testCardProduct.Id, testECProduct.Id, 'Option', 1, 2, 2, 2, 1, 1);
		ECLineItem.Apttus_Config2__PricingStatus__c = 'Pending';
		lineitemList.add(ECLineItem);
		Apttus_Config2__LineItem__c AC1LineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, GSPLI5, testCardProduct, testCardProduct.Id, testAircraft1Product.Id, 'Option', 1, 2, 3, 2, 1, 1);
		AC1LineItem.Apttus_Config2__PricingStatus__c = 'Pending';
		lineitemList.add(AC1LineItem);
		Apttus_Config2__LineItem__c AC2LineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, CSPLI5, testCardProduct, testCardProduct.Id, testAircraft2Product.Id, 'Option', 1, 2, 4, 2, 1, 1);
		AC2LineItem.Apttus_Config2__PricingStatus__c = 'Pending';
		lineitemList.add(AC2LineItem);
        Apttus_Config2__LineItem__c enhLineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, enhanPLI, testCardProduct, testCardProduct.Id, enhancementProduct.Id, 'Option', 1, 2, 5, 2, 1, 1);
		enhLineItem.Apttus_Config2__PricingStatus__c = 'Pending';
		lineitemList.add(enhLineItem);
        Apttus_Config2__LineItem__c cardAssetLineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, null, 'Product/Service', 1, 1, 6, 0, null, 1);
		cardAssetLineItem.Apttus_Config2__PricingStatus__c = 'Pending';
        cardAssetLineItem.Apttus_Config2__LineStatus__c = 'Existing';
        cardAssetLineItem.Apttus_Config2__LocationId__c = testAccLocation.Id;
		lineitemList.add(cardAssetLineItem);

		insert lineitemList;
        
        List<Apttus_Config2__LineItem__c> lineList = [select id,Apttus_Config2__ItemSequence__c,Apttus_Config2__LineType__c, Apttus_Config2__LineNumber__c, 
                           Apttus_Config2__LineSequence__c, Apttus_Config2__PrimaryLineNumber__c, Apttus_Config2__OptionSequence__c, 
                           Apttus_Config2__ParentBundleNumber__c, Apttus_Config2__Quantity__c, Apttus_Config2__LineStatus__c, 
                           Apttus_Config2__IsPrimaryLine__c, Apttus_Config2__PriceListItemId__c, Apttus_Config2__PriceListId__c, 
                           Apttus_Config2__PricingStatus__c 
                           from Apttus_Config2__LineItem__c 
                           where Apttus_Config2__ConfigurationId__c =:configuration];
        system.debug('lineitemList:'+Json.serialize(lineList));

		Apttus_Config2__ProductAttributeValue__c cardLinePAV = APTS_CPQTestUtility.createAttributeValue(cardLineItem.Id);
		insert cardLinePAV;
		Apttus_Config2__ProductAttributeValue__c ecLinePAV = APTS_CPQTestUtility.createAttributeValue(ECLineItem.Id);
		ecLinePAV.APTS_Selected_Product_Type__c = 'Elite Combo';
		insert ecLinePAV;
		Apttus_Config2__ProductAttributeValue__c AC1LinePAV = APTS_CPQTestUtility.createAttributeValue(AC1LineItem.Id);
		AC1LinePAV.APTS_Selected_Product_Type__c = 'Elite Combo';
		AC1LinePAV.Aircraft_Remaining_Hours__c = 12.5;
		AC1LinePAV.APTS_Bonus_Hours__c = 2;
        AC1LinePAV.APTS_Current_Value__c = 10000.00;
        AC1LinePAV.APTPS_List_Price__c = 8000;
		insert AC1LinePAV;
		Apttus_Config2__ProductAttributeValue__c AC2LinePAV = APTS_CPQTestUtility.createAttributeValue(AC2LineItem.Id);
		AC2LinePAV.APTS_Selected_Product_Type__c = 'Elite Combo';
		AC2LinePAV.Aircraft_Remaining_Hours__c = 12.5;
		AC2LinePAV.APTS_Bonus_Hours__c = 0;
        AC2LinePAV.APTS_Current_Value__c = 10000.00;
        AC2LinePAV.APTPS_List_Price__c = 8000;
		insert AC2LinePAV;
        Apttus_Config2__ProductAttributeValue__c enhLinePAV = APTS_CPQTestUtility.createAttributeValue(enhLineItem.Id);
		enhLinePAV.APTS_Selected_Product_Type__c = 'Elite Combo';
        enhLinePAV.Non_Standard__c = 'test';
        enhLinePAV.Death_or_Disability_Clause__c = TRUE;
        enhLinePAV.Death_or_Disability_Recipient__c = testContact.Id;
        enhLinePAV.APTS_Death_or_Disability_Recipient_2__c = testContact.Id;
		insert enhLinePAV;

		Test.startTest();
		boolean hasPendingItems = false;
        Apttus_CpqApi.CPQ.UpdatePriceRequestDO objUpdatePriceRequestDO = new Apttus_CpqApi.CPQ.UpdatePriceRequestDO();
        objUpdatePriceRequestDO.CartId = configuration;
        System.debug('==objUpdatePriceRequestDO==' + objUpdatePriceRequestDO);
        Apttus_CpqApi.CPQ.UpdatePriceResponseDO result = Apttus_CpqApi.CPQWebService.updatePriceForCart(objUpdatePriceRequestDO);
        System.debug('==result==' + result);
        
        Apttus_Config2.PricingWebService.computeNetPriceForItemColl(configuration, 1);
        
        
        nsProposal.APTS_New_Sale_or_Modification__c = 'Modification';
		nsProposal.APTS_Modification_Type__c = 'Direct Conversion';
		nsProposal.APTS_Additional_Modification_Type__c = 'Term Extension';
        update nsProposal; 
        
        result = Apttus_CpqApi.CPQWebService.updatePriceForCart(objUpdatePriceRequestDO);
        System.debug('==result==' + result);
        
        Apttus_Config2.PricingWebService.computeNetPriceForItemColl(configuration, 1);
        
        nsProposal.APTS_New_Sale_or_Modification__c = 'Modification';
		nsProposal.APTS_Modification_Type__c = APTS_ConstantUtil.EXPIRED_HOURS_CARD;
		//nsProposal.APTS_Additional_Modification_Type__c = 'Term Extension';
        update nsProposal;
        
        result = Apttus_CpqApi.CPQWebService.updatePriceForCart(objUpdatePriceRequestDO);
        System.debug('==result==' + result);
        
        Apttus_Config2.PricingWebService.computeNetPriceForItemColl(configuration, 1);
        
        //NOT NEEDED ANY MORE AS WE MOVE MIGRATION CLASSES OUT OF THE PACKAGE
        //For Data Migration PCB
        /*Apttus_Config2__ConfigCustomClasses__c configCustomClasses = Apttus_Config2__ConfigCustomClasses__c.getOrgDefaults();
		configCustomClasses.Apttus_Config2__PricingCallbackClass__c = 'APTPS_PricingCallback_DM';
		update configCustomClasses;
        
        Apttus_CpqApi.CPQ.UpdatePriceRequestDO objUpdatePriceRequestDODM = new Apttus_CpqApi.CPQ.UpdatePriceRequestDO();
        objUpdatePriceRequestDODM.CartId = configuration;
        System.debug('==objUpdatePriceRequestDO==' + objUpdatePriceRequestDODM);
        Apttus_CpqApi.CPQ.UpdatePriceResponseDO resultDM = Apttus_CpqApi.CPQWebService.updatePriceForCart(objUpdatePriceRequestDODM);
        System.debug('==result==' + resultDM);*/
        
		Test.stopTest();
	}
}