/************************************************************************************************************************
@Name: APTPS_GenerateReferralRefereeAgreement
@Author: Avinash Bamane
@CreateDate: 26/08/2020
@Description: Generate configuration and agreement line items for Referral/Referee agreements.
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public with sharing class APTPS_GenerateReferralRefereeAgreement {
	
    public static void generateAgreement (String modificationType, Decimal aircraftHours, String legalEntityName, 
                                         Id locationId, String agProgram, String agSubType, Id rrParentId, Id refParentId, 
                                         Id agId, Id accId, Id plId) {
        try{
            Id cartId, bundleProductId = null;
            String productProgram, termProductCode, bundleProductCode;
            String referralName = 'Referee Letter'.equalsIgnoreCase(agSubType) ? 'Referee Card' : 'Referral Card';
            Id agreementId = 'Referee Letter'.equalsIgnoreCase(agSubType) ? rrParentId : refParentId;
            
            //Assumption - Only NJUS and NJE are available.
            if('NetJets U.S.'.equalsIgnoreCase(agProgram)) {
                productProgram = 'NJUS Product Type';
                termProductCode = 'NJUS_TERM';
                bundleProductCode = 'NJUS_CARD';
            } else {
                productProgram = 'NJE Product Type';
                termProductCode = 'NJE_TERM';
                bundleProductCode = 'NJE_CARD';
            }
            
            String productTypeCode = getProductTypeCode(agreementId, modificationType);
            String aircraftProductCode = getAircraftProductCode(agreementId, productTypeCode, modificationType);
            cartId = generateCart(agId, accId, plId);
            bundleProductId = getBundleProduct(bundleProductCode);
            if(cartId != null)
                generateReferralCardAgreement(cartId, bundleProductId, productProgram, productTypeCode, aircraftProductCode, 
                                              aircraftHours, termProductCode, legalEntityName, agId, locationId, referralName, rrParentId);
        } catch(Exception e) {
            system.debug('Error while generating referral/referee agreements --> '+e.getMessage());
        }
    } 
    
    //TODO: Need to add more logic in case of Term Extension, Assignment and also need logic for Active status
    private static String getProductTypeCode (Id aggId, String modificationType) {
        String productCode = '';
        if (modificationType != null && modificationType == 'Add Enhancements') {
            Apttus__AgreementLineItem__c ali = [SELECT Apttus_CMConfig__OptionId__r.ProductCode 
                                                FROM Apttus__AgreementLineItem__c 
                                                WHERE Apttus__AgreementId__r.Id =: aggId 
                                                AND Option_Product_Family_System__c = 'Product Type' 
                                                AND Apttus_CMConfig__LineStatus__c != 'New'
                                                AND Apttus_CMConfig__IsPrimaryLine__c = TRUE];
            productCode = ali.Apttus_CMConfig__OptionId__r.ProductCode; 
        } else {
            Apttus__AgreementLineItem__c ali = [SELECT Apttus_CMConfig__OptionId__r.ProductCode 
                                                FROM Apttus__AgreementLineItem__c 
                                                WHERE Apttus__AgreementId__r.Id =: aggId 
                                                AND Option_Product_Family_System__c = 'Product Type' 
                                                AND Apttus_CMConfig__LineStatus__c = 'New'
                                                AND Apttus_CMConfig__IsPrimaryLine__c = TRUE];
            productCode = ali.Apttus_CMConfig__OptionId__r.ProductCode; 
        }
        return productCode;
    }
    
    private static String getAircraftProductCode (Id aggId, String productTypeCode, String modificationType) {
        String acProductCode = '';
        if (modificationType != null && modificationType == 'Add Enhancements') {
            if (productTypeCode != null && (productTypeCode == 'COMBO_NJUS' || productTypeCode == 'COMBO_NJE')) {
                List<Apttus__AgreementLineItem__c> aircraftALIs = [SELECT Apttus_CMConfig__OptionId__r.ProductCode, Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c 
                                                                   FROM Apttus__AgreementLineItem__c 
                                                                   WHERE Apttus__AgreementId__r.Id =: aggId 
                                                                   AND Option_Product_Family_System__c = 'Aircraft'
                                                                   AND Apttus_CMConfig__LineStatus__c != 'New'
                                                                   AND Apttus_CMConfig__IsPrimaryLine__c = TRUE
                                                                   order by Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c desc];
                if (!aircraftALIs.isEmpty()) {
                    acProductCode = aircraftALIs[0].Apttus_CMConfig__OptionId__r.ProductCode;
                }
            } else {
                Apttus__AgreementLineItem__c ali = [SELECT Apttus_CMConfig__OptionId__r.ProductCode 
                                                    FROM Apttus__AgreementLineItem__c 
                                                    WHERE Apttus__AgreementId__r.Id =: aggId 
                                                    AND Option_Product_Family_System__c = 'Aircraft'
                                                    AND Apttus_CMConfig__LineStatus__c != 'New'
                                                    AND Apttus_CMConfig__IsPrimaryLine__c = TRUE]; 
                acProductCode = ali.Apttus_CMConfig__OptionId__r.ProductCode;
            }
        } else {
            if (productTypeCode != null && (productTypeCode == 'COMBO_NJUS' || productTypeCode == 'COMBO_NJE')) {
                List<Apttus__AgreementLineItem__c> aircraftALIs = [SELECT Apttus_CMConfig__OptionId__r.ProductCode, Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c 
                                                                   FROM Apttus__AgreementLineItem__c 
                                                                   WHERE Apttus__AgreementId__r.Id =: aggId 
                                                                   AND Option_Product_Family_System__c = 'Aircraft' 
                                                                   AND Apttus_CMConfig__LineStatus__c = 'New'
                                                                   AND Apttus_CMConfig__IsPrimaryLine__c = TRUE
                                                                   order by Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c desc];
                if (!aircraftALIs.isEmpty()) {
                    acProductCode = aircraftALIs[0].Apttus_CMConfig__OptionId__r.ProductCode;
                }
            } else {
                Apttus__AgreementLineItem__c ali = [SELECT Apttus_CMConfig__OptionId__r.ProductCode 
                                                    FROM Apttus__AgreementLineItem__c 
                                                    WHERE Apttus__AgreementId__r.Id =: aggId 
                                                    AND Option_Product_Family_System__c = 'Aircraft' 
                                                    AND Apttus_CMConfig__LineStatus__c = 'New'
                                                    AND Apttus_CMConfig__IsPrimaryLine__c = TRUE]; 
                acProductCode = ali.Apttus_CMConfig__OptionId__r.ProductCode;
            }
        }
        return acProductCode;
    }

    private static Id generateCart(Id agId, Id accId, Id plId) {
        Apttus_Config2__ProductConfiguration__c config = new Apttus_Config2__ProductConfiguration__c();
        boolean isSuccess = false;
        config.Apttus_CMConfig__AgreementId__c = agId;
        config.Apttus_Config2__AccountId__c = accId;
        config.Apttus_Config2__BusinessObjectRefId__c = agId;
        config.Apttus_Config2__BusinessObjectType__c = 'Agreement';
        config.Apttus_Config2__PriceListId__c = plId;
        try {
            insert config;
            isSuccess = true;
        } catch (Exception e) {
            System.debug(e);
        }
        
        return isSuccess ? config.Id : null;
    }

    private static Id getBundleProduct(String bundleProductCode) {
        return [SELECT Id FROM Product2 WHERE ProductCode =: bundleProductCode LIMIT 1].Id;
    }
    
    public static void generateReferralCardAgreement(Id cartId, Id bundleProductId, String productProgram, String productTypeCode, 
                                                     String aircraftProductCode, Decimal aircraftHours, String termProductCode, 
                                                     String legalEntityName, Id referralCardId, Id locationId, String referralName, 
                                                     Id parentAgId) {
                                                         String aircraftName = '';
                                                         String productTypeName = '';
                                                         // Add Bundle Product, Options to the Cart
                                                         Apttus_CPQApi.CPQ.AddBundleRequestDO requestBundle = new Apttus_CPQApi.CPQ.AddBundleRequestDO();
                                                         requestBundle.CartId = cartId;
                                                         requestBundle.SelectedBundle = new Apttus_CPQApi.CPQ.SelectedBundleDO();
                                                         requestBundle.SelectedBundle.SelectedProduct = new Apttus_CPQApi.CPQ.SelectedProductDO();
                                                         requestBundle.SelectedBundle.SelectedProduct.ProductId = bundleProductId;
                                                         requestBundle.SelectedBundle.SelectedProduct.Quantity = 1;
                                                         
                                                         // Pass Product Type Option Group ID
                                                         Apttus_Config2__ProductOptionGroup__c prodOpt = [SELECT Id, Name 
                                                                                                          FROM Apttus_Config2__ProductOptionGroup__c 
                                                                                                          WHERE Apttus_Config2__ProductId__r.Id =: bundleProductId 
                                                                                                          AND Apttus_Config2__OptionGroupId__r.Name =: productProgram];
                                                         Apttus_CPQApi.CPQ.ProductOptionGroupSearchResultDO productOptionGroupResult = 
                                                             Apttus_CPQApi.CPQWebService.getOptionGroupsForPriceListProduct(prodOpt.Id, bundleProductId);
                                                         
                                                         if(productOptionGroupResult.HasOptionGroups) {
                                                             requestBundle.SelectedBundle.SelectedOptions = new List<Apttus_CPQApi.CPQ.SelectedOptionDO>();
                                                             for(Apttus_CPQApi.CPQ.ProductOptionGroupDO productOptionGroup : productOptionGroupResult.OptionGroups) {
                                                                 
                                                                 if(productOptionGroup.HasOptionComponents) {
                                                                     for(Apttus_CPQApi.CPQ.ProductOptionComponentDO
                                                                         productOptionComponent : productOptionGroup.OptionComponents) {
                                                                             
                                                                             if(productOptionComponent.ProductCode == productTypeCode) {
                                                                                 Apttus_CPQApi.CPQ.SelectedOptionDO selectedOptionDO = new Apttus_CPQApi.CPQ.SelectedOptionDO();
                                                                                 selectedOptionDO.ComponentId = productOptionComponent.ComponentId;
                                                                                 selectedOptionDO.ComponentProductId = productOptionComponent.ComponentProductId;
                                                                                 //START: GCM-9732
                                                                                 productTypeName = productOptionComponent.Name;
                                                                                 //END: GCM-9732
                                                                                 selectedOptionDO.CustomFields = getCustomFieldList();
                                                                                 selectedOptionDO.CustomData = getLineItem();
                                                                                 requestBundle.SelectedBundle.SelectedOptions.add(selectedOptionDO);
                                                                             }
                                                                             
                                                                             if(productOptionComponent.ProductCode == aircraftProductCode) {
                                                                                 Apttus_CPQApi.CPQ.SelectedOptionDO selectedOptionDO = new Apttus_CPQApi.CPQ.SelectedOptionDO();
                                                                                 selectedOptionDO.ComponentId = productOptionComponent.ComponentId;
                                                                                 selectedOptionDO.ComponentProductId = productOptionComponent.ComponentProductId;
                                                                                 selectedOptionDO.Quantity = aircraftHours;
                                                                                 aircraftName = productOptionComponent.Name;
                                                                                 selectedOptionDO.CustomFields = getCustomFieldList();
                                                                                 selectedOptionDO.CustomData = getLineItem();
                                                                                 requestBundle.SelectedBundle.SelectedOptions.add(selectedOptionDO);
                                                                             }
                                                                             
                                                                             if(productOptionComponent.ProductCode == termProductCode) {
                                                                                 Apttus_CPQApi.CPQ.SelectedOptionDO selectedOptionDO = new Apttus_CPQApi.CPQ.SelectedOptionDO();
                                                                                 selectedOptionDO.ComponentId = productOptionComponent.ComponentId;
                                                                                 selectedOptionDO.ComponentProductId = productOptionComponent.ComponentProductId;
                                                                                 selectedOptionDO.CustomFields = getCustomFieldList();
                                                                                 selectedOptionDO.CustomData = getLineItem();
                                                                                 requestBundle.SelectedBundle.SelectedOptions.add(selectedOptionDO);
                                                                             }
                                                                             
                                                                         }
                                                                 }
                                                                 
                                                             }
                                                         }
                                                         
                                                         Apttus_CPQApi.CPQ.AddBundleResponseDO responseAddBundle = Apttus_CPQApi.CPQWebService.addBundle(requestBundle);
                                                         // End - Add Bundle Product, Options to the Cart
                                                         
                                                         Apttus_Config2__ProductAttributeValue__c aircraftLineItem = [SELECT Aircraft_Hours__c
                                                                                                                      FROM Apttus_Config2__ProductAttributeValue__c 
                                                                                                                      WHERE Apttus_Config2__LineItemId__r.Apttus_Config2__ConfigurationId__r.Id =: cartId 
                                                                                                                      AND Apttus_Config2__LineItemId__r.Apttus_Config2__IsPrimaryLine__c = TRUE 
                                                                                                                      AND Apttus_Config2__LineItemId__r.Apttus_Config2__OptionId__r.ProductCode =: aircraftProductCode];
                                                         if(aircraftLineItem != null) {
                                                             aircraftLineItem.Aircraft_Hours__c = aircraftHours;
                                                             update aircraftLineItem;
                                                         }
                                                         
                                                         Apttus_Config2__ProductAttributeValue__c termLineItem = [SELECT Initial_Term_Amount_Months__c, Delayed_Start_Amount_Months__c, Grace_Period_Amount_Months__c 
                                                                                                                  FROM Apttus_Config2__ProductAttributeValue__c 
                                                                                                                  WHERE Apttus_Config2__LineItemId__r.Apttus_Config2__ConfigurationId__r.Id =: cartId 
                                                                                                                  AND Apttus_Config2__LineItemId__r.Apttus_Config2__IsPrimaryLine__c = TRUE 
                                                                                                                  AND Apttus_Config2__LineItemId__r.Apttus_Config2__OptionId__r.ProductCode =: termProductCode];
                                                         
                                                         if(termLineItem != null) {
                                                             APTS_Refereer_Referee_Terms__c termValues = APTS_Refereer_Referee_Terms__c.getOrgDefaults();
                                                             termLineItem.Delayed_Start_Amount_Months__c = String.valueOf(termValues.APTS_Delayed_Start_Amount_Months__c);
                                                             termLineItem.Initial_Term_Amount_Months__c = String.valueOf(termValues.APTS_Initial_Term_Amount_Months__c);
                                                             termLineItem.Grace_Period_Amount_Months__c = String.valueOf(termValues.APTS_Grace_Period_Amount_Months__c);
                                                             update termLineItem;
                                                         }
                                                         
                                                         // Stamp Legal Entity on all line items 
                                                         List<Apttus_Config2__LineItem__c> lineItems = [SELECT Apttus_Config2__LocationId__c, APTS_New_Sale_or_Modification__c 
                                                                                                        FROM Apttus_Config2__LineItem__c
                                                                                                        WHERE Apttus_Config2__ConfigurationId__r.Id =: cartId];
                                                         for(Apttus_Config2__LineItem__c li : lineItems) {
                                                             li.Apttus_Config2__LocationId__c = locationId;
                                                             li.APTS_New_Sale_or_Modification__c = 'New Sale';
                                                         }
                                                         update lineItems;
                                                         // End - Stamp Legal Entity on all line items

                                                         // Do pricing for newly generated cart
                                                         boolean hasPendingItems = false;
                                                         //TODO: Pricing is disabled. Looks like environment issue.
                                                         /*do {
                                                            Apttus_CpqApi.CPQ.UpdatePriceRequestDO objUpdatePriceRequestDO = new Apttus_CpqApi.CPQ.UpdatePriceRequestDO();
                                                            objUpdatePriceRequestDO.CartId = cartId;
                                                            Apttus_CpqApi.CPQ.UpdatePriceResponseDO result = Apttus_CpqApi.CPQWebService.updatePriceForCart(objUpdatePriceRequestDO);
                                                            hasPendingItems = result.IsPricePending;
                                                         } while(hasPendingItems);*/
                                                         //End - Do pricing for newly generated cart
                                                         
                                                         // Finalize the newly generated cart
                                                         //TODO: read this 10 value from custom settings and here asset migration class is referred. Create new one if required.
                                                         String nextExecutionTime = Datetime.now().addSeconds(10).format('s m H d M ? yyyy');  
                                                         System.schedule('ScheduledJob ' + String.valueOf(Math.random()), nextExecutionTime, new APTS_LegacyAgreementSchedulable(cartId));
                                                         
                                                         // Update - Referral Card Agreement Name
                                                         String locationName = legalEntityName != null ? legalEntityName : '';
                                                         String refAgreementName = '';
                                                         refAgreementName = locationName+', '+referralName+'-'+String.valueOf(aircraftHours)+','+aircraftName;
                                                         List<Apttus__APTS_Agreement__c> agListToUpdate = new List<Apttus__APTS_Agreement__c>();
                                                         if(!String.isBlank(refAgreementName)) {
                                                             Apttus__APTS_Agreement__c refAgreement = new Apttus__APTS_Agreement__c(Id = referralCardId);
                                                             refAgreement.Name = refAgreementName;
                                                             refAgreement.Document_Name__c = refAgreementName;
                                                             refAgreement.APTS_New_Sale_or_Modification__c = 'New Sale';
                                                             refAgreement.APTS_Modification_Type__c = '';
                                                             refAgreement.Apttus__Total_Contract_Value__c = 0;
                                                             //GCM-9732 : Stamp Product Type on Agreement.
                                                             refAgreement.Product_Type__c = productTypeName;
                                                             refAgreement.Aircraft_Types__c = aircraftName;
                                                             agListToUpdate.add(refAgreement);
                                                         }
                                                         // End - Referral Card Agreement Name

                                                         if(!agListToUpdate.isEmpty())
                                                             update agListToUpdate;
                                                         
                                                     }
    
    public static List<String> getCustomFieldList() {
        List<String> customFieldList = new List<String>();
        customFieldList.add('APTS_New_Sale_or_Modification__c');
        return customFieldList;
    }
    
    public static Apttus_Config2__LineItem__c getLineItem() {
        Apttus_Config2__LineItem__c li = new Apttus_Config2__LineItem__c();
        li.APTS_New_Sale_or_Modification__c = 'New Sale';       
        return li;
    }

}