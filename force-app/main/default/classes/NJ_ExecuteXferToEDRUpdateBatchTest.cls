/***********************************************************************************************************************
@Name: NJ_ExecuteXferToEDRUpdateBatchTest
@Author: Ramesh Kumar
@CreateDate: 01 Nov 2020
@Description: APEX class to test classes NJ_ExecuteXferToEDRUpdateBatch
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
@isTest
public with sharing class NJ_ExecuteXferToEDRUpdateBatchTest {
    @isTest
    static void executeAgrExferflagBatchTest(){
        List<Account> accList = new List<Account>();
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();

        List<Apttus__APTS_Agreement__c> aggList = new List<Apttus__APTS_Agreement__c>();

        Account srcAccToInsert = new Account();
        srcAccToInsert.Name = 'Cross Account Src Account';
        srcAccToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
        srcAccToInsert.APTS_Has_Active_NJE_Agreement__c = true;
        insert srcAccToInsert;

        Account trgAccToInsert = new Account();
        trgAccToInsert.Name = 'Cross Account Trg Account';
        trgAccToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
        trgAccToInsert.APTS_Has_Active_NJE_Agreement__c = true;
        insert trgAccToInsert;

        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', srcAccToInsert.Id, 'Propose');
        insert testOpportunity;

        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;

        Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', srcAccToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal.APTS_Modification_Type__c = 'Termination';
        insert testProposal;            
        
        
        Apttus__APTS_Agreement__c aggToInsert1 = new Apttus__APTS_Agreement__c();
        aggToInsert1.Name = 'APTS Test Agreement 1111';
        aggToInsert1.Program__c = 'NetJets U.S.';
        aggToInsert1.Apttus__Account__c = srcAccToInsert.Id;
        aggToInsert1.Card_Number__c = '1324567890';
        aggToInsert1.APTS_Modification_Type__c = 'New Sale';
        aggToInsert1.RecordTypeId = recordTypeIdNJUS;
        aggToInsert1.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
        aggToInsert1.Xfer_to_EDR__c = false;
        aggList.add(aggToInsert1);
        insert aggList;
                  
        //Start Test
        Test.startTest();
        Database.executebatch(new NJ_ExecuteXferToEDRUpdateBatch(aggList[0].Id));
        //Stop Test
        Test.stopTest();
                
    }
    
}