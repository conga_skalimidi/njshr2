/***********************************************************************************************************************
@Name: UpdateAgrAcctTeamMbrHelperTest
@Author: Ramesh Kumar
@CreateDate: 11 Marc 2021
@Description: APEX class to test classes UpdateAgrAcctTeamMbrHelperTest
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
@isTest
public with sharing class UpdateAgrAcctTeamMbrHelperTest {
   
    @TestSetup
    static void makeData(){
      
        Account accToInsert = new Account(Name = 'APTS Test Account Name');
        insert accToInsert;
        
        List<Apttus__APTS_Agreement__c> aggList = new List<Apttus__APTS_Agreement__c>();

        aggList.add(new Apttus__APTS_Agreement__c(
            Name = 'APTS Test Agreement 1111',
            Program__c = 'NetJets U.S.',
            Apttus__Account__c = accToInsert.Id,
            CurrencyIsoCode = 'USD'
        ));
        insert aggList;


        Id profileId = [SELECT Id FROM Profile WHERE Name =: 'System Administrator' LIMIT 1].Id;
        List<User> testUsers = new List<User>();
        for(Integer i = 0; i < 1; i++){
            testUsers.add(new User(
                FirstName = 'Test' + i + '', LastName = 'test' + i + '@test', Username = 'test' + i + '@test.testuser', 
                Email = 'Testusername' + i + '@example.test', Alias = 'test', TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',  ProfileId = profileId, 
                NetJets_Company__c = 'NJA'
            ));
        }
        insert testUsers;    
    }

    
    @isTest
    static void UpdateAgrAcctExecutiveTest_giveAccountExecPrimary_UpdateAgreement(){

        Account accToInsert = [SELECT Id FROM Account LIMIT 1];
        User userRec = [SELECT Id FROM User WHERE Username LIKE '%test%' AND NetJets_Company__c = 'NJA' LIMIT 1];

        AccountTeamMember teamMember = new AccountTeamMember(
            AccountId = accToInsert.Id,
            TeamMemberRole = 'Account Executive - Primary',
            UserId = userRec.Id,
            NetJets_Company__c = 'NJA'
        );
        insert teamMember;

        List<AccountTeamMemberEvent__e> accTeamMemberEventList = new List<AccountTeamMemberEvent__e>();
        AccountTeamMemberEvent__e acctTeamEvent = new AccountTeamMemberEvent__e(
            AccountId__c = teamMember.AccountId,
            TeamMemberRole__c = teamMember.TeamMemberRole,
            Team_Member_User_ID__c = teamMember.UserId
        );
        accTeamMemberEventList.add(acctTeamEvent);
        
        Test.startTest();
        UpdateAgrAcctTeamMbrHelper.UpdateAgreementsWithAcctTeamMembers((List<AccountTeamMemberEvent__e>)accTeamMemberEventList);
        Test.stopTest();

        Apttus__APTS_Agreement__c agreement = [
            SELECT Id, Account_Executive__c, APTS_Associate_Sales_Executive__c, Primary_Sales_Executive__c,
                RVP__c, Sales_Consultant__c, Secondary_Sales_Executive__C
            FROM Apttus__APTS_Agreement__c
            LIMIT 1
        ];
        
        System.assertEquals(userRec.Id, agreement.Account_Executive__c, 'The Account Executive should be set on the agreement');
    }

    
    
    @isTest
    static void UpdateAgrAcctExecutiveTest_giveSalesExecPrimary_UpdateAgreement(){

        Account accToInsert = [SELECT Id FROM Account LIMIT 1];
        User userRec = [SELECT Id FROM User WHERE Username LIKE '%test%' AND NetJets_Company__c = 'NJA' LIMIT 1];

        AccountTeamMember teamMember = new AccountTeamMember(
            AccountId = accToInsert.Id,
            TeamMemberRole = 'Sales Executive - Primary',
            UserId = userRec.Id,
            NetJets_Company__c = 'NJA'
        );
        insert teamMember;

        List<AccountTeamMemberEvent__e> accTeamMemberEventList = new List<AccountTeamMemberEvent__e>();
        AccountTeamMemberEvent__e acctTeamEvent = new AccountTeamMemberEvent__e(
            AccountId__c = teamMember.AccountId,
            TeamMemberRole__c = teamMember.TeamMemberRole,
            Team_Member_User_ID__c = teamMember.UserId
        );
        accTeamMemberEventList.add(acctTeamEvent);
        
        Test.startTest();
        UpdateAgrAcctTeamMbrHelper.UpdateAgreementsWithAcctTeamMembers((List<AccountTeamMemberEvent__e>)accTeamMemberEventList);
        Test.stopTest();

        Apttus__APTS_Agreement__c agreement = [
            SELECT Id, Account_Executive__c, APTS_Associate_Sales_Executive__c, Primary_Sales_Executive__c,
                RVP__c, Sales_Consultant__c, Secondary_Sales_Executive__C
            FROM Apttus__APTS_Agreement__c
            LIMIT 1
        ];
        
        System.assertEquals(userRec.Id, agreement.Primary_Sales_Executive__c, 'The Primary Sales Executive should be set on the agreement');
    }


    
    
    @isTest
    static void UpdateAgrAcctExecutiveTest_giveAssociateSalesExec_UpdateAgreement(){

        Account accToInsert = [SELECT Id FROM Account LIMIT 1];
        User userRec = [SELECT Id FROM User WHERE Username LIKE '%test%' AND NetJets_Company__c = 'NJA' LIMIT 1];

        AccountTeamMember teamMember = new AccountTeamMember(
            AccountId = accToInsert.Id,
            TeamMemberRole = 'Associate Sales Executive',
            UserId = userRec.Id,
            NetJets_Company__c = 'NJA'
        );
        insert teamMember;

        List<AccountTeamMemberEvent__e> accTeamMemberEventList = new List<AccountTeamMemberEvent__e>();
        AccountTeamMemberEvent__e acctTeamEvent = new AccountTeamMemberEvent__e(
            AccountId__c = teamMember.AccountId,
            TeamMemberRole__c = teamMember.TeamMemberRole,
            Team_Member_User_ID__c = teamMember.UserId
        );
        accTeamMemberEventList.add(acctTeamEvent);
        
        Test.startTest();
        UpdateAgrAcctTeamMbrHelper.UpdateAgreementsWithAcctTeamMembers((List<AccountTeamMemberEvent__e>)accTeamMemberEventList);
        Test.stopTest();

        Apttus__APTS_Agreement__c agreement = [
            SELECT Id, Account_Executive__c, APTS_Associate_Sales_Executive__c, Primary_Sales_Executive__c,
                RVP__c, Sales_Consultant__c, Secondary_Sales_Executive__C
            FROM Apttus__APTS_Agreement__c
            LIMIT 1
        ];
        
        System.assertEquals(userRec.Id, agreement.APTS_Associate_Sales_Executive__c, 'The Associate Sales Executive should be set on the agreement');
    }

    
    
    @isTest
    static void UpdateAgrAcctExecutiveTest_giveSalesDirector_UpdateAgreement(){

        Account accToInsert = [SELECT Id FROM Account LIMIT 1];
        User userRec = [SELECT Id FROM User WHERE Username LIKE '%test%' AND NetJets_Company__c = 'NJA' LIMIT 1];

        AccountTeamMember teamMember = new AccountTeamMember(
            AccountId = accToInsert.Id,
            TeamMemberRole = 'Sales Director/RVP',
            UserId = userRec.Id,
            NetJets_Company__c = 'NJA'
        );
        insert teamMember;

        List<AccountTeamMemberEvent__e> accTeamMemberEventList = new List<AccountTeamMemberEvent__e>();
        AccountTeamMemberEvent__e acctTeamEvent = new AccountTeamMemberEvent__e(
            AccountId__c = teamMember.AccountId,
            TeamMemberRole__c = teamMember.TeamMemberRole,
            Team_Member_User_ID__c = teamMember.UserId
        );
        accTeamMemberEventList.add(acctTeamEvent);
        
        Test.startTest();
        UpdateAgrAcctTeamMbrHelper.UpdateAgreementsWithAcctTeamMembers((List<AccountTeamMemberEvent__e>)accTeamMemberEventList);
        Test.stopTest();

        Apttus__APTS_Agreement__c agreement = [
            SELECT Id, Account_Executive__c, APTS_Associate_Sales_Executive__c, Primary_Sales_Executive__c,
                RVP__c, Sales_Consultant__c, Secondary_Sales_Executive__C
            FROM Apttus__APTS_Agreement__c
            LIMIT 1
        ];
        
        System.assertEquals(userRec.Id, agreement.RVP__c, 'The RVP should be set on the agreement');
    }
    
    
    @isTest
    static void UpdateAgrAcctExecutiveTest_giveSalesConsultant_UpdateAgreement(){

        Account accToInsert = [SELECT Id FROM Account LIMIT 1];
        User userRec = [SELECT Id FROM User WHERE Username LIKE '%test%' AND NetJets_Company__c = 'NJA' LIMIT 1];

        AccountTeamMember teamMember = new AccountTeamMember(
            AccountId = accToInsert.Id,
            TeamMemberRole = 'Sales Consultant',
            UserId = userRec.Id,
            NetJets_Company__c = 'NJA'
        );
        insert teamMember;

        List<AccountTeamMemberEvent__e> accTeamMemberEventList = new List<AccountTeamMemberEvent__e>();
        AccountTeamMemberEvent__e acctTeamEvent = new AccountTeamMemberEvent__e(
            AccountId__c = teamMember.AccountId,
            TeamMemberRole__c = teamMember.TeamMemberRole,
            Team_Member_User_ID__c = teamMember.UserId
        );
        accTeamMemberEventList.add(acctTeamEvent);
        
        Test.startTest();
        UpdateAgrAcctTeamMbrHelper.UpdateAgreementsWithAcctTeamMembers((List<AccountTeamMemberEvent__e>)accTeamMemberEventList);
        Test.stopTest();

        Apttus__APTS_Agreement__c agreement = [
            SELECT Id, Account_Executive__c, APTS_Associate_Sales_Executive__c, Primary_Sales_Executive__c,
                RVP__c, Sales_Consultant__c, Secondary_Sales_Executive__C
            FROM Apttus__APTS_Agreement__c
            LIMIT 1
        ];
        
        System.assertEquals(userRec.Id, agreement.Sales_Consultant__c, 'The Sales Consultant should be set on the agreement');
    }
    
    
    @isTest
    static void UpdateAgrAcctExecutiveTest_giveSalesExecSecondary_UpdateAgreement(){

        Account accToInsert = [SELECT Id FROM Account LIMIT 1];
        User userRec = [SELECT Id FROM User WHERE Username LIKE '%test%' AND NetJets_Company__c = 'NJA' LIMIT 1];

        AccountTeamMember teamMember = new AccountTeamMember(
            AccountId = accToInsert.Id,
            TeamMemberRole = 'Sales Executive - Secondary',
            UserId = userRec.Id,
            NetJets_Company__c = 'NJA'
        );
        insert teamMember;

        List<AccountTeamMemberEvent__e> accTeamMemberEventList = new List<AccountTeamMemberEvent__e>();
        AccountTeamMemberEvent__e acctTeamEvent = new AccountTeamMemberEvent__e(
            AccountId__c = teamMember.AccountId,
            TeamMemberRole__c = teamMember.TeamMemberRole,
            Team_Member_User_ID__c = teamMember.UserId
        );
        accTeamMemberEventList.add(acctTeamEvent);
        
        Test.startTest();
        UpdateAgrAcctTeamMbrHelper.UpdateAgreementsWithAcctTeamMembers((List<AccountTeamMemberEvent__e>)accTeamMemberEventList);
        Test.stopTest();

        Apttus__APTS_Agreement__c agreement = [
            SELECT Id, Account_Executive__c, APTS_Associate_Sales_Executive__c, Primary_Sales_Executive__c,
                RVP__c, Sales_Consultant__c, Secondary_Sales_Executive__C
            FROM Apttus__APTS_Agreement__c
            LIMIT 1
        ];
        
        System.assertEquals(userRec.Id, agreement.Secondary_Sales_Executive__c, 'The Secondary Sales Executive should be set on the agreement');
    }
}