@IsTest
public with sharing class AcctTeamAgreementPETriggerHelperTest {
    
    @TestSetup
    static void makeData(){
        
        Account accToInsert = new Account(Name = 'APTS Test Account Name');
        insert accToInsert;

        Profile testProfile =  [SELECT Id FROM Profile WHERE Name =: 'Standard User' LIMIT 1];

        User testUser = new User(
            FirstName = 'Test',
            LastName = 'test@test',
            Username = 'test@test.testuser001',
            Email = 'Testusername@example001.test',
            Alias = 'test',
            TimeZoneSidKey = 'America/Los_Angeles',
     		EmailEncodingKey = 'UTF-8',
     		LanguageLocaleKey = 'en_US',
     		LocaleSidKey = 'en_US',
            ProfileId = testProfile.Id,
            IsActive = true
        );        
        insert testUser;

        AccountTeamMember  teamMember = new AccountTeamMember(
            AccountId = accToInsert.Id,
            TeamMemberRole = 'Sales Consultant',
            UserId= testUser.Id
        );
        insert teamMember;
    }

    @IsTest 
    static void testAcctTeamAgreementPETriggerHelper_testSendPlatformEvent(){
        AccountTeamMember acctTeamMbr = [SELECT Id, AccountId, TeamMemberRole, UserId FROM AccountTeamMember LIMIT 1];
        
        Map<id,AccountTeamMember> newMap = new  Map<id,AccountTeamMember>{
            acctTeamMbr.Id => acctTeamMbr
        };

        Test.startTest();
        AcctTeamAgreementPETriggerHelper.CreateAccTTeamMembersPlatformEvent(newMap);
        Test.stopTest();
    }

    @IsTest 
    static void testAcctTeamAgreementPETriggerHelper_testUpdateTrigger(){
        AccountTeamMember acctTeamMbr = [SELECT Id, AccountId, TeamMemberRole, UserId FROM AccountTeamMember LIMIT 1];

        Test.startTest();
        update acctTeamMbr;
        Test.stopTest();
    }

    @IsTest 
    static void testAcctTeamAgreementPETriggerHelper_testDeleteTrigger(){
        AccountTeamMember acctTeamMbr = [SELECT Id, AccountId, TeamMemberRole, UserId FROM AccountTeamMember LIMIT 1];

        Test.startTest();
        delete acctTeamMbr;
        Test.stopTest();
    }

}