/*************************************************************
@Name: NJ_ProcessAssetLIQueueble
@Author: Ramesh Kumar
@CreateDate: 16 Mar 2020
@Description : This Queueable class is used to process Asset line Items after activation.
******************************************************************/
public class NJ_ProcessAssetLIQueueble implements Queueable {
    public NJ_ProcessAssetLIQueueble(){
        
    }

    public Id  agreementId; 
    public NJ_ProcessAssetLIQueueble(Id agreementIdParam){
        this.agreementId =  agreementIdParam;  
    }
    public void execute(QueueableContext context) {
        Integer jobs = [SELECT count() FROM AsyncApexJob where (ApexClass.name='OrderWorkflowQJob' OR ApexClass.name='CreateOrderQJob') AND  (Status = 'Queued' or Status = 'Processing' or Status = 'Preparing')];
        if(jobs == 0 ) {
            system.debug('into UpdateAssetQue..');
            Apttus__APTS_Related_Agreement__c srcAgreeObj = [SELECT Id,Apttus__APTS_Contract_From__c,Apttus__APTS_Contract_To__c,APTS_Business_Object_Type__c FROM Apttus__APTS_Related_Agreement__c WHERE Apttus__APTS_Contract_From__c =:agreementId AND APTS_Business_Object_Type__c='Agreement Life Cycle Flow' Limit 1];
            
            List<Apttus_Config2__AssetLineItem__c> assetLi = new List<Apttus_Config2__AssetLineItem__c>();
            
            for(Apttus_Config2__AssetLineItem__c a : [SELECT Id, APTS_Original_Agreement__c  FROM Apttus_Config2__AssetLineItem__c WHERE Apttus_CMConfig__AgreementId__c =:agreementId ]){
                assetLi.add(a);
            }

            if(assetLi.size() > 0){

                List<Apttus_Config2__AssetLineItem__c> UpdateALIList = new List<Apttus_Config2__AssetLineItem__c>();
                for(Apttus_Config2__AssetLineItem__c updateALI :[SELECT Id, APTS_NJ_Asset_Status__c FROM      Apttus_Config2__AssetLineItem__c WHERE Apttus_CMConfig__AgreementId__c = :agreementId]) {                     
                    UpdateALIList.add(new Apttus_Config2__AssetLineItem__c(Id=updateALI.Id, APTS_NJ_Asset_Status__c='Activated',APTS_Agreement_Status__c='Active', NJ_Is_CAAssignment_Pending__c = false));
                }
                
                if(!UpdateALIList.isEmpty()) {
                    update UpdateALIList;
                }

                System.debug('agreementId--->' + agreementId);
                System.debug('srcAgreeObj.Apttus__APTS_Contract_To__c--->' + srcAgreeObj.Apttus__APTS_Contract_To__c);
                terminateAgreement(srcAgreeObj.Apttus__APTS_Contract_To__c);
     
            }

           } else{
            
            system.debug('into Job not completed. Retry..');
            System.enqueueJob(new NJ_ProcessAssetLIQueueble(agreementId));
        }
    }

    public void terminateAgreement(Id terminateAgreementId) {

        system.debug('into terminate..');
        Boolean response = Apttus.AgreementWebService.terminateAgreement(terminateAgreementId);
        if(response) {
             List<Apttus_Config2__AssetLineItem__c> updateALIList = new  List<Apttus_Config2__AssetLineItem__c>();
            Apttus__APTS_Agreement__c updateAgreement = new Apttus__APTS_Agreement__c(id=terminateAgreementId,Apttus__Termination_Date__c=system.today(),Agreement_Status__c= 'Terminated', Chevron_Status__c='Terminated', APTS_Path_Chevron_Status__c='Terminated');
            update updateAgreement;  
            for(Apttus_Config2__AssetLineItem__c updateALI :[SELECT Id,Apttus_CMConfig__AgreementId__c,Apttus_Config2__AssetStatus__c FROM Apttus_Config2__AssetLineItem__c WHERE Apttus_CMConfig__AgreementId__c =:terminateAgreementId  FOR UPDATE] ) {
                updateALI.Apttus_Config2__AssetStatus__c = 'Cancelled';
                updateALI.APTS_Agreement_Status__c = 'Inactive';
                updateALI.APTS_NJ_Asset_Status__c='Terminated';
                updateALIList.add(updateALI);
            }
            system.debug('updateALIList-->'+updateALIList);
            if(!updateALIList.isEmpty()) {
                update updateALIList;                            
            }
            
        }
    }
            
    
}