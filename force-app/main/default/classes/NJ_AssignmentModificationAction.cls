/*******************************************************************************************************************************************
@Name: NJ_AssignmentModificationAction
@Author: Ramesh Kumar
@CreateDate: 01/31/2020
@Description: This apex  copies the Asset Line Item and Attribute Value from Source to Target account.
This APEX will be called from flow - 'Asset_Cross_Account_Assignment'. 
********************************************************************************************************************************************/

global class NJ_AssignmentModificationAction{

    private static List<Apttus_Config2__AssetLineItem__c> clonedALIList = new List<Apttus_Config2__AssetLineItem__c>();
    public class NJ_AssignmentModificationRequest{
                @InvocableVariable(required=true)
                public Id targetAccountId;  
                @InvocableVariable(required=true)
                public Id bundleAssetId;   
            }   
        
    @InvocableMethod(label='Cross Assignment Clone Assets and Attribute values' description='Assignment - Creates Asset line Items and corresponding Attribute Values ')
    public static void ExecuteCAAssignmentProcess (NJ_AssignmentModificationRequest[] requests) {
        
            for(NJ_AssignmentModificationRequest input : requests) {
                createAssetLineItems(input.targetAccountId, input.bundleAssetId);
            }
    
    }

    public static void createAssetLineItems (String targetAccountId, String bundleAssetId) {

        List<Apttus_Config2__AssetLineItem__c> bundleALIList = new List<Apttus_Config2__AssetLineItem__c>();

        
        //system.debug('Into createAssetLineItems');
        //system.debug('bundleAssetId -->' + bundleAssetId);
        //Cloning the Bundle Level Asset Item
        string sQuery = getCreatableFieldsSOQL('Apttus_Config2__AssetLineItem__c','Id =: bundleAssetId');
        bundleALIList = Database.query(sQuery);

        //system.debug('bundleALIList -->' + bundleALIList);

        if(bundleALIList.size() > 0){

            List<Apttus_Config2__AssetLineItem__c> aliList = new List<Apttus_Config2__AssetLineItem__c>();
            aliList = [SELECT Id FROM Apttus_Config2__AssetLineItem__c WHERE APTS_Original_Agreement__c = :bundleALIList[0].Apttus_CMConfig__AgreementId__c AND Apttus_Config2__BillToAccountId__c = :targetAccountId];
            
            //system.debug('Before aliList');
            if(aliList == null || aliList.size() == 0){
                //.debug('Into bundleALIList IF');
                Apttus_Config2__AssetLineItem__c newBundleLevelAsset = bundleALIList[0].clone(false, true, false, false);
                newBundleLevelAsset.Apttus_Config2__BillToAccountId__c = targetAccountId;
                newBundleLevelAsset.Apttus_Config2__ShipToAccountId__c = targetAccountId;
                newBundleLevelAsset.Apttus_Config2__AccountId__c = targetAccountId;
                newBundleLevelAsset.Apttus_Config2__LocationId__c = null;
                newBundleLevelAsset.NJ_Is_CAAssignment_Pending__c = true;
                  

                try{

                    insert newBundleLevelAsset;
   
                } catch(Exception e){
                    System.debug('cError in reating bundle Asset Line Items: ' + e.getMessage());
                    //throw e; 
                }

                clonedALIList.add(newBundleLevelAsset);

                if(clonedALIList.size()> 0){
                    //system.debug('this.clonedALIList -->'+ clonedALIList);
                    //creates child ALI
                    createChildALI(targetAccountId, newBundleLevelAsset, bundleAssetId);
                    //creates APAV for each ALI
                    createAssetAttributeValues();

                }

            }
            else {
                system.debug('Bundle Asset Line Item already exists');
            }


        }

    
    }

    
    public static void createChildALI (String targetAccountId,Apttus_Config2__AssetLineItem__c newBundleAsset, String bundleAssetId) {
            //Clone Option Level Asset 
            //system.debug('bundleAssetId-->'+bundleAssetId);
            string sColQuery = getCreatableFieldsSOQL('Apttus_Config2__AssetLineItem__c','Apttus_Config2__BundleAssetId__c =:bundleAssetId');
            List<Apttus_Config2__AssetLineItem__c> assetLineItemList = Database.query(sColQuery);

            //system.debug('assetLineItemList -->'+ assetLineItemList);

            for(Apttus_Config2__AssetLineItem__c aLI : assetLineItemList) {
                //system.debug('ID Name -->'+ aLI.Id + '   ' +aLI.Name + '   ' +aLI.Apttus_Config2__BundleAssetId__c);

                Apttus_Config2__AssetLineItem__c colAssetItem =aLI.clone(false, true, false, false);
                colAssetItem.Apttus_Config2__BillToAccountId__c = targetAccountId;
                colAssetItem.Apttus_Config2__ShipToAccountId__c = targetAccountId;
                colAssetItem.Apttus_Config2__AccountId__c = targetAccountId;
                colAssetItem.Apttus_Config2__BundleAssetId__c = newBundleAsset.Id;
                colAssetItem.Apttus_Config2__LocationId__c = null;
                colAssetItem.NJ_Is_CAAssignment_Pending__c = true;
            
                if(colAssetItem.Apttus_Config2__ParentAssetId__c != null){
                    colAssetItem.Apttus_Config2__ParentAssetId__c = newBundleAsset.Id;    
                    system.debug(colAssetItem.Apttus_Config2__ChargeType__c);
                }
                
                clonedALIList.add(colAssetItem);
                
            }

        try{
            if(clonedALIList.size() > 0) {
                    upsert clonedALIList;
            }
        } catch(Exception e){
            System.debug('Error in creating child Asset Line Items: ' + e.getMessage());
            //throw e; 
        }

    }

    public static void createAssetAttributeValues (){

        Set<String> uniqueAttrValId = new Set<String>();
        for(Apttus_Config2__AssetLineItem__c colAssetItem1 : clonedALIList) {
                system.debug('AssetAttrID  -->'+ colAssetItem1.Apttus_Config2__AttributeValueId__c);
                uniqueAttrValId.add(colAssetItem1.Apttus_Config2__AttributeValueId__c);
            
        }
        //system.debug('Unique AssetAttrID  -->'+ uniqueAttrValId);

        Map<Id, Id> attrCloneMap = new Map<Id, Id>();
        for(Apttus_Config2__AssetAttributeValue__c atv : [SELECT Id FROM Apttus_Config2__AssetAttributeValue__c WHERE Id IN :uniqueAttrValId]) {
            attrCloneMap.put(atv.Id, cloneAssetAttributeValue(atv.Id));
            //system.debug('into Asst Clone loop');
        }

        //system.debug('Map AssetAttrID  -->'+ attrCloneMap);

        for(Apttus_Config2__AssetLineItem__c colAssetAttrValItem : clonedALIList) {
        
            string sAttributeValue = attrCloneMap.get(colAssetAttrValItem.Apttus_Config2__AttributeValueId__c);

            colAssetAttrValItem.Apttus_Config2__AttributeValueId__c = sAttributeValue;
           
        }


        try{
            if(clonedALIList.size()> 0 ) {
                update clonedALIList;
            }
        } catch(Exception e){
            System.debug('Error while cloning Asset Line Items with Atttibute Value error Message: ' + e.getMessage());
            System.debug('Error while cloning Asset Line Items with Atttibute Value stackTrace Message: ' + e.getStackTraceString());
        }
        
    }

    private static string cloneAssetAttributeValue(String assetAttrId){
    
        string sQuery = getCreatableFieldsSOQL('Apttus_Config2__AssetAttributeValue__c','Id =:assetAttrId LIMIT 1');
        //system.debug( 'Query-->'+ sQuery);
        Apttus_Config2__AssetAttributeValue__c assetAttrValue = Database.query(sQuery);
        Apttus_Config2__AssetAttributeValue__c clonedAssetAttrValue = assetAttrValue.clone(false, true, false, false);
                
        insert clonedAssetAttrValue;

        //system.debug( 'old AAVale-->' + assetAttrId +':::::::' + 'New AAVale-->' + clonedAssetAttrValue.Id);

        return clonedAssetAttrValue.Id;
    }
    //Returns a dynamic SOQL statement for the whole object, includes only creatable fields since we will be inserting a cloned result of this query
        private static string getCreatableFieldsSOQL(String objectName, String whereClause){
        String selects = '';
        if(whereClause == null || whereClause == ''){
            return null;
        }

        //get a map of field names and field tokens
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        List<String> selectFields = new List<String>();

        if(fMap!=null){
            for(Schema.SObjectField ft : fMap.values()){ //loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); //describe each field (fd)
                if (fd.isCreateable()){ //field is creatable
                    selectFields.add(fd.getName());
                }
            }
        }
        if(!selectFields.isEmpty()){
            for (string s: selectFields){
                selects += s + ',';
            }
            if(selects.endsWith(',')){
                selects = selects.substring(0,selects.lastIndexOf(','));
            } 
            //system.debug('Selects-->'+selects);
        }
        //system.debug('Query11-->'+'SELECT ' + selectFields+ ' FROM ' + objectName + ' WHERE ' + whereClause);
        return 'SELECT ' + selects+ ' FROM ' + objectName + ' WHERE ' + whereClause;
    }

    
}