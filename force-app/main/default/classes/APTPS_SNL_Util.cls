/************************************************************************************************************************
 @Name: APTPS_SNL_Util
 @Author: Conga PS Dev Team
 @CreateDate: 04 June 2021
 @Description: Util class for SNL business logic
 ************************************************************************************************************************
 @ModifiedBy:
 @ModifiedDate:
 @ChangeDescription:
 ************************************************************************************************************************/

public class APTPS_SNL_Util {
    /**
     @description: Check if provided Program Type is of SNL type or not
     @param: progType - Program Type
     @return: true in case of SNL products
     */
    public static boolean isSNLProgramType(String progType) {
        boolean isSNL = false;
        APTPS_SNL_Products__c snlProducts = APTPS_SNL_Products__c.getOrgDefaults();
        String prodNames = snlProducts.Products__c;
        if(progType != null && prodNames.contains(progType))
            isSNL = true;
        system.debug('Is SNL program type --> '+isSNL);
        return isSNL;
    }

    /**
     @description: Check if it's NJA/NJE Share product or not.
     @param: productCode - Product Code.
     @return: true in case of Share product
     */
    public static boolean isShareProduct(String productCode) {
        boolean result = false;
        if(productCode != null && (APTS_ConstantUtil.NJA_SHARE_CODE.equalsIgnoreCase(productCode) || APTS_ConstantUtil.NJE_SHARE_CODE.equalsIgnoreCase(productCode)))
            result = true;

        system.debug('isShareProduct --> '+result);
        return result;
    }

    /**
     @description: Compute the MMF price for Share
     @param: item - Line Item
     @param: listPrice - List Price
     @return: Computed MMF price
     */
    public static Decimal computeMMFPrice(Apttus_Config2__LineItem__c item, Decimal listPrice, Decimal definedPriceFactor) {
        Decimal computedMMFPrice = 0;
        try{
            Decimal shareHours = item.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c != null ? Decimal.valueOf(item.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c) : 0;
            Decimal mmfPriceFactor = shareHours/definedPriceFactor; //We are not adding validation to check for zero, instead execution should fail.
            Decimal mmfListPrice = listPrice != null ? listPrice : 0;
            system.debug('definedPriceFactor --> '+definedPriceFactor+'shareHours --> '+shareHours+' mmfPriceFactor --> '+mmfPriceFactor+' mmfListPrice --> '+mmfListPrice);
            computedMMFPrice = mmfPriceFactor*mmfListPrice;
        } catch(Exception e) {
            system.debug('Error while computing MMF Price. Error details --> ' + e + ' Line Number-->' + e.getLineNumber());
        }

        system.debug('computeMMFPrice, MMF computed Price is --> '+computedMMFPrice);
        return computedMMFPrice;
    }

    /**
    @description: Reusable method to set Agreement Status
    @param: ag - Agreement record
    @param: agStatus - Agreement Status and Chevron Status
    @param: fundingStatus - Funding Status
    @param: docStatus - Document Status
    @return: void
    */
    public static Apttus__APTS_Agreement__c setAgreementStatus(Apttus__APTS_Agreement__c ag, String agStatus, String fundingStatus, String docStatus) {
        ag.Agreement_Status__c = agStatus;
        ag.Chevron_Status__c = agStatus;
        ag.APTS_Path_Chevron_Status__c = agStatus;
        ag.Funding_Status__c = fundingStatus;
        ag.Document_Status__c = docStatus;
        return ag;
    }
    
    /**
    @description: Reusable method to populate Agreement Dates
    @param: ag - Agreement record
    @param: agLiList - agreementLineItemList
    @return: void
    */
    public static void populateDates(Apttus__APTS_Agreement__c ag,List<Apttus__AgreementLineItem__c> agLiList) {
        Date delayedStartDate, minimumCommitmentDate,effectiveDate,contractEndDate,closeDate;
        delayedStartDate=minimumCommitmentDate=effectiveDate=contractEndDate=closeDate= null;
        String delayedStartMonths = '';
        Integer termMonths = 0;
        String minimumCommitmentPeriod = '';
        if(APTS_ConstantUtil.SHARE.equalsIgnoreCase(ag.APTPS_Program_Type__c)){
            for(Apttus__AgreementLineItem__c agLi : agLiList) {
                if(agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Minimum_Commitment_Period__c != null) {
                    minimumCommitmentPeriod = agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Minimum_Commitment_Period__c;
                }
                if(agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Delayed_Start_Date__c != null) {
                    delayedStartDate = agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Delayed_Start_Date__c;
                }               
                if(agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c != null) {
                    delayedStartMonths = agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c;
                }
                if(agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Contract_Length__c != null) {
                    termMonths = Integer.ValueOf(agLi.Apttus_CMConfig__AttributeValueId__r.APTPS_Contract_Length__c);
                }
                                
            }
            effectiveDate = ag.Apttus__Contract_Start_Date__c;
            closeDate = ag.APTPS_Closing_Date__c;
            if(effectiveDate == null) {
                ag.Apttus__Contract_Start_Date__c = ag.APTPS_Closing_Date__c;
                effectiveDate = ag.APTPS_Closing_Date__c;
                
            }
            if(delayedStartDate != null) {
                ag.Delayed_Start_Date__c = delayedStartDate;
            }
            if(delayedStartMonths != '' && effectiveDate!=null) {
                ag.Delayed_Start_Date__c = effectiveDate.addMonths(Integer.valueOf(delayedStartMonths));
            }
            if(ag.First_Flight_Date__c!=null && delayedStartDate!=null && ag.First_Flight_Date__c < delayedStartDate) {
                delayedStartDate = ag.First_Flight_Date__c;
                ag.Delayed_Start_Date__c = delayedStartDate;
            }
            if(minimumCommitmentPeriod != '') {
                minimumCommitmentDate = effectiveDate.addMonths(Integer.valueOf(minimumCommitmentPeriod));
                if(delayedStartMonths!='') {
                    minimumCommitmentDate = minimumCommitmentDate.addMonths(Integer.valueOf(delayedStartMonths));       
                }
                if(delayedStartDate!=null) {
                    minimumCommitmentDate = delayedStartDate.addMonths(Integer.valueOf(minimumCommitmentPeriod));       
                }               
                ag.APTPS_Minimum_Commitment_Date__c = minimumCommitmentDate;
                
            }
            contractEndDate = effectiveDate.addMonths(termMonths);
            if(delayedStartMonths!='') {
                contractEndDate = contractEndDate.addMonths(Integer.valueOf(delayedStartMonths));
            }
            if(delayedStartDate!=null) {
                contractEndDate = delayedStartDate.addMonths(termMonths);       
            }
            ag.Apttus__Contract_End_Date__c = contractEndDate;
            
        }
    }
}