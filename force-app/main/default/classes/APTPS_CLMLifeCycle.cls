/************************************************************************************************************************
@Name: APTPS_CLMLifeCycle
@Author: Conga PS Dev Team
@CreateDate: 6 June 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CLMLifeCycle implements APTPS_CLMLifeCycleCallable {
    private static String dsMetadataName = 'APTPS_CLMLifeCycle_Registry__mdt';
    public static Map<String, String> prodCodeClassNameMap = new Map<String, String>();
    
    /** 
    @description: Find out the APEX class implementation for the given productCode
    @param: Product Code
    @return: APEX Class name
    */
    private String findCLMAction_Implementation(String productCode) {
        String className = null;
        if(productCode != null && !prodCodeClassNameMap.isEmpty() 
           && prodCodeClassNameMap.containsKey(productCode)) {
               className = prodCodeClassNameMap.get(productCode);
           }
        system.debug('APEX Class Details --> '+className);
        return className;
    }
    
    /** 
    @description: Execute product specific Deal Summary logic.
    @param: Product code and arguments
    @return: 
    */
    public Object call(String productCode, Map<String, Object> args) {
        system.debug('Product Code to update deal summary --> '+productCode);
        system.debug('Arguments to call method --> '+args);
        String retMsg = null;
        if(args == null || args.get('newAgrList') == null) 
            throw new ExtensionMalformedCallException('Agreement data is missing.');
        
        if(!String.isEmpty(productCode)) {
            if(prodCodeClassNameMap.isEmpty()) {
                setProdCodeClassNameMap();
            }
            
            String className = findCLMAction_Implementation(productCode);
            if(className != null) {
                APTPS_CLMLifeCycleInterface dsObj = (APTPS_CLMLifeCycleInterface)Type.forName(className).newInstance();
                dsObj.callCLMAction(args);
                retMsg = 'SUCCESS';
            }
        }
        return retMsg;
    }
    
    /** 
    @description: Custom Exception implementation
    @param:
    @return: 
    */
    public class ExtensionMalformedCallException extends Exception {
        public String errMsg = '';
        public void ExtensionMalformedCallException(String errMsg) {
            this.errMsg = errMsg;
        }
    }
    
    /** 
    @description: Read CLM Fields Registry metadata and prepare the Map
    @param:
    @return: 
    */
    private void setProdCodeClassNameMap() {
        List<APTPS_CLMLifeCycle_Registry__mdt> fieldsSummaryHandlers = [SELECT Product_Code__c, Class_Name__c FROM APTPS_CLMLifeCycle_Registry__mdt];
        for(APTPS_CLMLifeCycle_Registry__mdt handler : fieldsSummaryHandlers) {
            if(handler.Product_Code__c != null && handler.Class_Name__c != null) 
                prodCodeClassNameMap.put(handler.Product_Code__c, handler.Class_Name__c);
        }
    }
    
}