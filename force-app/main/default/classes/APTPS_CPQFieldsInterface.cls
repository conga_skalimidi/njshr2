/************************************************************************************************************************
@Name: APTPS_CPQFieldsInterface
@Author: Conga PS Dev Team
@CreateDate: 09 July 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public interface APTPS_CPQFieldsInterface {
    void populateCPQFields(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines);
}