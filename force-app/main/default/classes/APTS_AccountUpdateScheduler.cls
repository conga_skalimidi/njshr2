/*
* Class Name: APTS_AccountUpdateScheduler
* Description: 
* 1. Scheduler class to call Batch APEX.
* Business Logic - Currently, this Scheduler will run once in a day.
* Created By: DEV Team, Apttus

* Modification Log
* ------------------------------------------------------------------------------------------------------------------------------------------------------------
* Developer               Date           US/Defect        Description
* ------------------------------------------------------------------------------------------------------------------------------------------------------------
* Avinash Bamane		 13/12/19		  GCM-6804 			 Added
*/

public class APTS_AccountUpdateScheduler implements Schedulable {
    public void execute(SchedulableContext sc) {
        APTS_AccountUpdaterBatch accountUpdateBatch = new APTS_AccountUpdaterBatch(); 
        Database.executebatch(accountUpdateBatch);
    }
}