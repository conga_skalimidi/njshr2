/*
 * Class Name: APTPS_CertificateOfInsuranceRequestCtlr
 * Description: Used for certificate of insurance request email template
 * Created By: DEV Team, Conga(Apttus)
 *
 * Modification Log
 * ------------------------------------------------------------------------------------------------------------------------------------------------------------
 * Developer               Date           US/Defect                 Description
 * ------------------------------------------------------------------------------------------------------------------------------------------------------------
 * Sagar Solanki          19/01/21        GCM-9876                    Added
 *
*/
global with sharing class APTPS_CertificateOfInsuranceRequestCtlr
{
    public Id agreementLocationId {get;set;}
    //Fetch additional data from account location that needs to be used in email template
    global Apttus_Config2__AccountLocation__c aLocation{
    get{
        Apttus_Config2__AccountLocation__c accLocation;
        accLocation = [SELECT Name, Full_Address__c FROM Apttus_Config2__AccountLocation__c WHERE Id =: agreementLocationId limit 1];
        return accLocation;
    }
    set;
    }
}