/************************************************************************************************************************
@Name: APTPS_CLMChildAgreementsTest
@Author: Conga PS Dev Team
@CreateDate: 17 September 2021
@Description: Test class coverage for APTPS_CLMChildAgreements
************************************************************************************************************************/
@isTest
public class APTPS_CLMChildAgreementsTest {
    @isTest
    public static void testCLMChildMethod() {
        Test.startTest();
        //Create SNL Custom settings
        APTPS_SNL_Products__c snlProducts = new APTPS_SNL_Products__c();
        snlProducts.Products__c = 'Demo,Corporate Trial,Share';
        insert snlProducts;
        
       
        
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        insert accToInsert;
       
        Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', accToInsert.Id);
        insert testAccLocation;
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
        
        //Create Products
       
        List<Product2> prodList = new List<Product2>();
        //START: NJA Share product test setup
        Product2 NJA_SHARE = APTS_CPQTestUtility.createProduct('Share', 'NJUS_SHARE', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJA_SHARE);
       
        Product2 NJA_INTERIM_LEASE = APTS_CPQTestUtility.createProduct('Interim Lease', APTS_ConstantUtil.INTERIM_LEASE_CODE, 'Apttus', 'Option', true, true, true, true);
        prodList.add(NJA_INTERIM_LEASE);
       
        Product2 PHENOM_NJUS = APTS_CPQTestUtility.createProduct('Phenom 300', 'PHENOM_300_NJUS', 'Apttus', 'Option', true, true, true, true);
        PHENOM_NJUS.Family = 'Aircraft';
        prodList.add(PHENOM_NJUS);
        
       
        //END: NJA Share product test setup
        insert prodList;
        
        //Create Quote/Proposals
        List<Apttus_Proposal__Proposal__c> proposalList = new List<Apttus_Proposal__Proposal__c>();
                
        Apttus_Proposal__Proposal__c shareNJUSQuote = APTS_CPQTestUtility.createProposal('Share NJUS Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        shareNJUSQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        shareNJUSQuote.APTPS_Program_Type__c = 'Share';
        shareNJUSQuote.CurrencyIsoCode = 'USD';
        proposalList.add(shareNJUSQuote);
        
        
        insert proposalList;
        
        //Create Quote/Proposal Line Items
        List<Apttus_Proposal__Proposal_Line_Item__c> pliList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        
        Apttus_Proposal__Proposal_Line_Item__c shareNJUSInterim = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSInterim.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.OPTION;
        shareNJUSInterim.Apttus_QPConfig__OptionId__c = NJA_INTERIM_LEASE.Id;
        shareNJUSInterim.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        pliList.add(shareNJUSInterim);
        
      
        Apttus_Proposal__Proposal_Line_Item__c shareNJUSAc = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSAc.Apttus_QPConfig__ChargeType__c = APTS_ConstantUtil.PURCHASE_PRICE;
        shareNJUSAc.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        shareNJUSAc.Apttus_QPConfig__OptionId__c = PHENOM_NJUS.Id;
        shareNJUSAc.Apttus_QPConfig__NetPrice__c = 1000;
        pliList.add(shareNJUSAc);
        
      
        //END: Share PLI test setup
        insert pliList;
        Apttus__APTS_Agreement__c shareCLMLifeCycleAg = new Apttus__APTS_Agreement__c();
		shareCLMLifeCycleAg.Name = 'CLM LifeCycle Share Agreement';
		shareCLMLifeCycleAg.Agreement_Status__c = 'Draft';
		shareCLMLifeCycleAg.Apttus__Account__c = accToInsert.Id;
		shareCLMLifeCycleAg.Apttus_QPComply__RelatedProposalId__c = shareNJUSQuote.Id;
		shareCLMLifeCycleAg.APTPS_Program_Type__c = APTS_ConstantUtil.SHARE;
		shareCLMLifeCycleAg.Apttus__Status__c = 'Request';
		shareCLMLifeCycleAg.Apttus__Status_Category__c = 'Request';
		//shareCLMLifeCycleAg.CurrencyIsoCode = 'USD';
		shareCLMLifeCycleAg.Program__c = APTS_ConstantUtil.NETJETS_USD;
		shareCLMLifeCycleAg.APTPS_Contract_Purchase_Deposit__c = 100000;
		shareCLMLifeCycleAg.Apttus__Total_Contract_Value__c = 200000;
		shareCLMLifeCycleAg.APTPS_Closing_Date__c = Date.today();
		shareCLMLifeCycleAg.First_Flight_Date__c = Date.today();
		insert shareCLMLifeCycleAg;
		Map<String,Set<String>> productArgMap = new Map<String, Set<String>>();
		productArgMap.put(APTS_ConstantUtil.INTERIM_LEASE_CODE,new set<string>{shareCLMLifeCycleAg.id});
        APTPS_CLMChildAgreements clmChildAgreementsObj = new APTPS_CLMChildAgreements();
		clmChildAgreementsObj.call(productArgMap);
        Test.stopTest();
    }
    
     /* @isTest
    public static void testMethod() {
      Test.startTest();
        List<Apttus_Proposal__Proposal__c> quotesToUpdate = new List<Apttus_Proposal__Proposal__c>();
        Apttus_Proposal__Proposal__c demoNJUSQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                      WHERE APTPS_Program_Type__c = 'Demo' AND CurrencyIsoCode = 'USD' LIMIT 1];
        demoNJUSQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        quotesToUpdate.add(demoNJUSQuote);
        
        Apttus_Proposal__Proposal__c demoNJEQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                     WHERE APTPS_Program_Type__c = 'Demo' AND CurrencyIsoCode = 'EUR' LIMIT 1];
        demoNJEQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        quotesToUpdate.add(demoNJEQuote);
        
        Apttus_Proposal__Proposal__c corporateNJEQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                          WHERE APTPS_Program_Type__c = 'Corporate Trial' AND CurrencyIsoCode = 'EUR' LIMIT 1];
        corporateNJEQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        quotesToUpdate.add(corporateNJEQuote);
        
        //START: Deal Summary test coverage for Share
        Apttus_Proposal__Proposal__c shareNJAQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                      WHERE APTPS_Program_Type__c = 'Share' AND CurrencyIsoCode = 'USD' LIMIT 1];
        shareNJAQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        quotesToUpdate.add(shareNJAQuote);
        
        Apttus_Proposal__Proposal__c shareNJEQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                      WHERE APTPS_Program_Type__c = 'Share' AND CurrencyIsoCode = 'EUR' LIMIT 1];
        shareNJEQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        //quotesToUpdate.add(shareNJAQuote);
        
        //For NJE Share - common code block is getting executed for NJA & NJE.
        APTPS_DealSummary_Share.NJE_Summary njeDsObj = new APTPS_DealSummary_Share.NJE_Summary();
        Map<String,Object> njeMap = new Map<String,Object>{'qId' => shareNJEQuote.Id};
            Apttus_Proposal__Proposal__c njeObj = njeDsObj.updateSummary(njeMap, null);
        //End
        system.debug('Quote List --> '+quotesToUpdate);
        update quotesToUpdate;
        
        APTPS_DealSummary testObj = new APTPS_DealSummary();
        Map<Id, String> quoteProductMap = new Map<Id, String>();
        quoteProductMap.put(demoNJUSQuote.Id, 'NJUS_DEMO');
        try{
            testObj.call(quoteProductMap, null);
        }catch(APTPS_DealSummary.ExtensionMalformedCallException e) {
            system.debug('Test coverage for exception');
        }
        
        try{
            Map<Id, Map<String, Object>> quoteArgMap = new Map<Id, Map<String, Object>>();
            quoteArgMap.put(demoNJUSQuote.Id, null);
            testObj.call(quoteProductMap, quoteArgMap);
        }catch(APTPS_DealSummary.ExtensionMalformedCallException e) {
            system.debug('Test coverage for exception');
        }
        
        Apttus_Config2__ProductConfiguration__c configShareNJA = [SELECT Id FROM Apttus_Config2__ProductConfiguration__c 
                                                                  WHERE Apttus_QPConfig__Proposald__c =: shareNJAQuote.Id LIMIT 1];
        Apttus_Config2.CustomClass.ValidationResult result = Apttus_Config2.CPQWebService.validateCart(configShareNJA.Id);
        System.AssertEquals(false, result.isSuccess);
        
        Test.stopTest();
    }*/
}