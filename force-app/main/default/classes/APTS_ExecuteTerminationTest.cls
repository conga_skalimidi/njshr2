/***********************************************************************************************************************
@Name: APTS_ExecuteTerminationTest
@Author: Siva Kumar
@CreateDate: 26 October 2020
@Description: APEX class to to test classes APTS_ExecuteTerminationAPIBatch, TerminateAgreementAction
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
@isTest
public with sharing class APTS_ExecuteTerminationTest {
    @isTest
    static void agreementTerminationTest(){
       List<Account> accList = new List<Account>();
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();

        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
        accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
        insert accToInsert;

        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;

        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;

        Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal.APTS_Modification_Type__c = 'Expired Hours Card';
        insert testProposal;

        /*Product2 testProduct = APTS_CPQTestUtility.createProduct('Card', 'APTS', 'Apttus', 'Bundle', true, true, true, true);
        //populate custom fields value if required
        insert testProduct;

        List<Apttus_Proposal__Proposal_Line_Item__c> lineItemList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        Apttus_Proposal__Proposal_Line_Item__c quoteli = new Apttus_Proposal__Proposal_Line_Item__c();
        quoteli.APTS_Modification_Type__c = 'Term Extension';
        quoteli.Apttus_Proposal__Product__c = testProduct.Id;
        quoteli.Apttus_QPConfig__LineStatus__c = 'New';
        quoteli.Apttus_Proposal__Proposal__c = testProposal.Id;
        quoteli.Option_Product_Name__c = 'Enhancements';
        lineItemList.add(quoteli);

        Apttus_Proposal__Proposal_Line_Item__c quoteli1 = new Apttus_Proposal__Proposal_Line_Item__c();
        quoteli1.APTS_Modification_Type__c = 'Collective Service Area';
        quoteli1.Apttus_Proposal__Product__c = testProduct.Id;
        quoteli1.Apttus_QPConfig__LineStatus__c = 'New';
        quoteli1.Apttus_Proposal__Proposal__c = testProposal.Id;
        quoteli1.Option_Product_Name__c = 'Enhancements';
        lineItemList.add(quoteli1);

        insert lineItemList;*/

        ID accID = accToInsert.Id;
        List<Apttus__APTS_Agreement__c> aggList = new List<Apttus__APTS_Agreement__c>();
        Apttus__APTS_Agreement__c aggToInsert = new Apttus__APTS_Agreement__c();
        aggToInsert.Name = 'APTS Test Agreement 1111';
        aggToInsert.Program__c = 'NetJets U.S.';
        aggToInsert.Apttus__Account__c = accID;

        aggToInsert.RecordTypeId = recordTypeIdNJUS;
        aggToInsert.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
        aggList.add(aggToInsert);

        insert aggList;
        
        //Create New Agreement Line Item
        Apttus__AgreementLineItem__c li = new Apttus__AgreementLineItem__c();
        li.Apttus__AgreementId__c = aggList[0].Id;
        li.APTS_Modification_Type__c='Expired Hours Card';
        li.Apttus_CMConfig__ChargeType__c='Half Card Premium';
        li.Apttus__NetPrice__c = 1000;
        insert li;
        ID aliID = li.ID;
        
         //Create New Asset Line Item
        Apttus_Config2__AssetLineItem__c  assetLi = new Apttus_Config2__AssetLineItem__c ();
        assetLi.Apttus_CMConfig__AgreementId__c = aggList[0].Id;
        insert assetLi;
       

        //Start Test
        Test.startTest();
        TerminateAgreementAction  terminateAction  = new TerminateAgreementAction ();
        TerminateAgreementAction.TerminateAgreementRequest terminatereq = new TerminateAgreementAction.TerminateAgreementRequest();
        terminatereq.agreementId = aggList[0].Id;
        TerminateAgreementAction.terminateAgreements(new List<TerminateAgreementAction.TerminateAgreementRequest>{terminatereq});
        APTS_ExecuteTerminationAPIBatch batch = new APTS_ExecuteTerminationAPIBatch(aggList[0].Id,false);
        Database.executeBatch( batch,1);
        //Stop Test
        Test.stopTest();
    }

    
    
}