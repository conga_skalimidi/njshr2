/************************************************************************************************************************
@Name: APTPS_ENInterface
@Author: Conga PS Dev Team
@CreateDate: 21 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public interface APTPS_ENInterface {
    void sendNotification(Map<String, Object> args);
}