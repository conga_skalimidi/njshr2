/************************************************************************************************************************
@Name: APTPS_DealSummary_Corporate
@Author: Conga PS Dev Team
@CreateDate: 17 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_DealSummary_Corporate {
    /** 
    @description: Coprporate Trail implementation
    @param:
    @return: 
    */
    public class NJE_Summary implements APTPS_DealSummaryInterface {
        public Apttus_Proposal__Proposal__c updateSummary(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Corporate NJE deal summary');
            Id qId = (Id)args.get('qId');
            Apttus_Proposal__Proposal__c quoteToUpdate = new Apttus_Proposal__Proposal__c(Id = qId);
            //Get required Proposal Line Item
            Apttus_Proposal__Proposal_Line_Item__c dsPLI = null;
            if(quoteLines != null) {
                for(Apttus_Proposal__Proposal_Line_Item__c pli : quoteLines) {
                    if(APTS_ConstantUtil.LINE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c)) {
                        dsPLI = pli;
                        break;
                    }
                }
            }
            
            if(dsPLI != null) {
                String dsText = '';
                dsText += '<b>Transaction Type:</b> Corporate Trial Flight<br/><br/>';
                dsText += '<b>Aircraft Type:</b> '+dsPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Corporate_Trial_Aircraft__c+'<br/><br/>';
                if(dsPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Term_Months__c != null)
                	dsText += '<b>Term Months:</b> '+dsPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Term_Months__c+'<br/><br/>';
                if(dsPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Term_End_Date__c != null) {
                	Date termEndDate = dsPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Term_End_Date__c;
                    dsText += '<b>Term End Date:</b> '+termEndDate.format()+'<br/><br/>';
                }
                if(dsPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Maximum_Number_of_Trials__c != null)
                	dsText += '<b>Maximum Number of Trials:</b> '+dsPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Maximum_Number_of_Trials__c+'<br/><br/>';
                //Stamp Deposit Amount if it's greater than 0
                if(dsPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Override_Deposit__c != null && dsPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Override_Deposit__c > 0)
                	dsText += '<b>Override Deposit:</b> '+'€'+dsPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Override_Deposit__c;
                system.debug('Deal description --> '+dsText);
                quoteToUpdate.Deal_Description__c = dsText;
            }
            
            return quoteToUpdate;
        }
    }
}