/************************************************************************************************************************
@Name: APTPS_CLM_Automation_Interface
@Author: Conga PS Dev Team
@CreateDate: 20 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public interface APTPS_CLM_Automation_Interface {
    void createAgreementWithLines(Id proposalId);
    void copyAttachment(Id agreementId, Id attachmentId);
    void activateAgreement(Id agreenentId, Id docId);
}