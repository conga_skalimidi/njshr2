/*******************************************************************************************************************************************
@Name: APTS_AgreementDeletionScheduler
@Author: Avinash Bamane
@CreateDate: 05/03/2021
@Description: Scheduler to schedule APTS_AgreementDeletionBatch
*******************************************************************************************************************************************/

public class APTS_AgreementDeletionScheduler implements Schedulable {
    public void execute(SchedulableContext sc) {
        APTS_AgreementDeletionBatch agDelBatch = new APTS_AgreementDeletionBatch(); 
        Database.executebatch(agDelBatch);
    }
}