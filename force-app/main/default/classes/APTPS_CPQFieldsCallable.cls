/************************************************************************************************************************
@Name: APTPS_CPQFieldsCallable
@Author: Conga PS Dev Team
@CreateDate: 09 July 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public interface APTPS_CPQFieldsCallable {
    Object call(Map<Id, String> quoteProductMap, Map<Id, Map<String, Object>> quoteArgMap);
}