/************************************************************************************************************************
@Name: APTS_ProductAttributeCallbackTest
@Author: Avinash Bamane
@CreateDate: 27/10/2020
@Description: Test class for APTS_ProductAttributeCallback
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:

************************************************************************************************************************/

@IsTest
public class APTS_ProductAttributeCallbackTest {
	@testsetup
    static void setupData(){
        try{
            Account testAccount = APTS_CPQTestUtility.createAccount('test apttus account', 'Enterprise');
            insert testAccount;
            
            Contact testContact = APTS_CPQTestUtility.createContact('Apttus', 'Product',testAccount.Id);
            insert testContact;
            
            Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', testAccount.Id, 'Propose');
            insert testOpportunity;
            
            Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
            insert testPriceList;
            
            Product2 termProduct = APTS_CPQTestUtility.createProduct('Term', 'NJUS_TERM', 'Term', 'Option', true, true, true, true);
            insert termProduct;
            
            Product2 enhancementProduct = APTS_CPQTestUtility.createProduct('Enhancement', 'Enhancements', 'Enhancement', 'Option', true, true, true, true);
            insert enhancementProduct;
            
            PricebookEntry termPBE = APTS_CPQTestUtility.createPricebookEntry(Test.getStandardPricebookId(), termProduct.Id, 100, true);
            insert termPBE;
            
            Apttus_Config2__PriceListItem__c termPriceListItem = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, termProduct.Id, 100, 'Standard Price', 'One Time', 'Per Unit', 'Each', true);
            insert termPriceListItem;
            
            PricebookEntry enhancementPBE = APTS_CPQTestUtility.createPricebookEntry(Test.getStandardPricebookId(), enhancementProduct.Id, 100, true);
            insert enhancementPBE;
            
            Apttus_Config2__PriceListItem__c enhancementPriceListItem = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, enhancementProduct.Id, 100, 'Standard Price', 'One Time', 'Per Unit', 'Each', true);
            insert enhancementPriceListItem;
            
            List<Apttus_Proposal__Proposal__c> quoteToInsert = new List<Apttus_Proposal__Proposal__c>();
            Apttus_Proposal__Proposal__c tProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', testAccount.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
            tProposal.APTS_New_Sale_or_Modification__c = 'Modification';
            tProposal.APTS_Modification_Type__c = 'Direct Conversion';
            quoteToInsert.add(tProposal);
            
            Apttus_Proposal__Proposal__c tEHCProposal = APTS_CPQTestUtility.createProposal('test EHC Proposal', testAccount.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
            tEHCProposal.APTS_New_Sale_or_Modification__c = 'Modification';
            tEHCProposal.APTS_Modification_Type__c = 'Expired Hours Card';
            quoteToInsert.add(tEHCProposal);
            
            if(!quoteToInsert.isEmpty())
                insert quoteToInsert;
            
            Apttus_Proposal__Proposal__c testProposal = new Apttus_Proposal__Proposal__c();
            Apttus_Proposal__Proposal__c testEHCProposal = new Apttus_Proposal__Proposal__c();
            for(Apttus_Proposal__Proposal__c q : quoteToInsert) {
                if(q.APTS_Modification_Type__c == 'Direct Conversion')
                    testProposal = q;
                else if(q.APTS_Modification_Type__c == 'Expired Hours Card')
                    testEHCProposal = q;
            }
            
            String testConfigurationId = APTS_CPQTestUtility.createConfiguration(testProposal.Id);
            String testConfigEHC = APTS_CPQTestUtility.createConfiguration(testEHCProposal.Id);
            
            Apttus_Config2__ClassificationName__c testClassification = APTS_CPQTestUtility.createCategory('test Apttus Classification', 'test Hiearchy Label', 'Apttus', null, true);
            insert testClassification;
            
            Apttus_Config2__ClassificationHierarchy__c testClassificationHierarchy = APTS_CPQTestUtility.createClassificationHierarchy(testClassification.Id, 'Classification Hierarchy Label');
            insert testClassificationHierarchy;
            
            Apttus_Config2__ProductOptionComponent__c testProductOptionComponent = APTS_CPQTestUtility.createProductOptionComponent(1);
            insert testProductOptionComponent;
            
            Apttus_Config2__SummaryGroup__c testSummaryGroup = APTS_CPQTestUtility.createSummaryGroup(testConfigurationId, 1, 1);
            insert testSummaryGroup;
    
            Apttus__APTS_Agreement__c ag = new Apttus__APTS_Agreement__c();
            ag.Name = 'test agreement ';
            ag.Agreement_Status__c = 'Active';
            ag.Apttus__Account__c = testAccount.Id;
            ag.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
            ag.Active_Until_Date__c = Date.today();
            ag.Grace_Period_End_Date__c = Date.today();
            ag.Funding_Date__c = Date.today();
            insert ag;
            
            //Term Asset Line Item
            Apttus_Config2__AssetLineItem__c termAssetLi = APTS_CPQTestUtility.createAssetLI(termProduct.Id, 'Option', 'Activated', ag.Id);
            system.assert(termAssetLi!=null);
            insert termAssetLi;
            
            Apttus_Config2__AssetAttributeValue__c termAssetAttri = new Apttus_Config2__AssetAttributeValue__c();
            termAssetAttri.Apttus_Config2__AssetLineItemId__c = termAssetLi.Id;
            termAssetAttri.Delayed_Start_Amount_Months__c = String.valueOf(0);
            termAssetAttri.Initial_Term_Amount_Months__c = String.valueOf(12);
            termAssetAttri.Grace_Period_Amount_Months__c = String.valueOf(12);
            termAssetAttri.Override_Term__c = true;
            termAssetAttri.Overridden_Delayed_Start_Months__c = String.valueOf(6);
            termAssetAttri.Overridden_Grace_Period_Months__c = String.valueOf(6);
            termAssetAttri.Overridden_Initial_Term_Months__c = String.valueOf(6);
            insert termAssetAttri;
            
            termAssetLi.Apttus_Config2__AttributeValueId__c = termAssetAttri.Id;
            update termAssetLi;
            
            //Enhancement Asset Line Item
            Apttus_Config2__AssetLineItem__c enAssetLi = APTS_CPQTestUtility.createAssetLI(enhancementProduct.Id, 'Option', 'Activated', ag.Id);
            insert enAssetLi;
            
            Apttus_Config2__AssetAttributeValue__c enAssetAttri = new Apttus_Config2__AssetAttributeValue__c();
            enAssetAttri.Apttus_Config2__AssetLineItemId__c = enAssetLi.Id;
            enAssetAttri.Non_Standard__c = 'Test';
            enAssetAttri.Death_or_Disability_Recipient__c = testContact.Id;
            enAssetAttri.Death_or_Disability_Clause__c = true;
            enAssetAttri.APTS_Death_or_Disability_Recipient_2__c = testContact.Id;
            enAssetAttri.APTS_Death_or_Disability_Recipient__c = 'Apttus';
            enAssetAttri.APTS_Death_or_Disability_Recipient_txt_2__c = 'Apttus';
            insert enAssetAttri;
            
            enAssetLi.Apttus_Config2__AttributeValueId__c = enAssetAttri.Id;
            update enAssetLi;
            
            //Add Term Asset to the Cart
            List<Apttus_Config2__LineItem__c> liListToInsert = new List<Apttus_Config2__LineItem__c>();
            Apttus_Config2__LineItem__c termLi = APTS_CPQTestUtility.createLiForPAV(testConfigurationId, testProposal, termPriceListItem, termProduct);
            system.assert(termLi!=null);
            termLi.Apttus_Config2__Description__c = 'Term';
            termLi.Apttus_Config2__OptionId__c = termProduct.Id;
            termLi.Apttus_Config2__LineStatus__c = 'Existing';
            termLi.Apttus_Config2__AssetLineItemId__c = termAssetLi.Id;
            liListToInsert.add(termLi);
            
            //Add Enhancement Asset Line Item to the cart
            Apttus_Config2__LineItem__c enhanLi = APTS_CPQTestUtility.createLiForPAV(testConfigurationId, testProposal, enhancementPriceListItem, enhancementProduct);
            enhanLi.Apttus_Config2__Description__c = 'Enhancements';
            enhanLi.Apttus_Config2__OptionId__c = enhancementProduct.Id;
            enhanLi.Apttus_Config2__LineStatus__c = 'Existing';
            enhanLi.Apttus_Config2__AssetLineItemId__c = enAssetLi.Id;
            liListToInsert.add(enhanLi);
            
            //Add Term New Line Item to the Cart
            Apttus_Config2__LineItem__c termLiNew = APTS_CPQTestUtility.createLiForPAV(testConfigurationId, testProposal, termPriceListItem, termProduct);
            termLiNew.Apttus_Config2__Description__c = 'Term';
            termLiNew.Apttus_Config2__OptionId__c = termProduct.Id;
            termLiNew.Apttus_Config2__LineStatus__c = 'New';
            liListToInsert.add(termLiNew);
            
            //Add Enhancement New Line Item to the Cart
            Apttus_Config2__LineItem__c enLiNew = APTS_CPQTestUtility.createLiForPAV(testConfigurationId, testProposal, enhancementPriceListItem, enhancementProduct);
            enLiNew.Apttus_Config2__Description__c = 'Enhancements';
            enLiNew.Apttus_Config2__OptionId__c = enhancementProduct.Id;
            enLiNew.Apttus_Config2__LineStatus__c = 'New';
            insert enLiNew;
            /*It is working fine in Apttus Test. But this code is giving error in other environments for 
             * Preferred_Bank__c field on Product Attribute Value. Due to this error code, code coverage is impacted 
             * for APTS_ProductAttributeCallback. 
             * Hence, commenting out and it will not cover GCM-9207 related changes in PAV callback. 
            Apttus_Config2__ProductAttributeValue__c tPav = new Apttus_Config2__ProductAttributeValue__c();
            tPav.Apttus_Config2__LineItemId__c = enLiNew.Id;
            tPav.APTS_Selected_Product_Type__c = '100-Hour';
            insert tPav;
            
            enLiNew.Apttus_Config2__AttributeValueId__c = tPav.Id;
            update enLiNew;*/
            
            //Add Term Asset Line to EHC cart
            Apttus_Config2__LineItem__c termEHCLi = APTS_CPQTestUtility.createLiForPAV(testConfigEHC, testEHCProposal, termPriceListItem, termProduct);
            termEHCLi.Apttus_Config2__Description__c = 'Term';
            termEHCLi.Apttus_Config2__OptionId__c = termProduct.Id;
            termEHCLi.Apttus_Config2__LineStatus__c = 'Existing';
            termEHCLi.Apttus_Config2__AssetLineItemId__c = termAssetLi.Id;
            liListToInsert.add(termEHCLi);
            
            //Add Term New Line Item to EHC cart
            Apttus_Config2__LineItem__c termEHCLiNew = APTS_CPQTestUtility.createLiForPAV(testConfigEHC, testEHCProposal, termPriceListItem, termProduct);
            termEHCLiNew.Apttus_Config2__Description__c = 'Term';
            termEHCLiNew.Apttus_Config2__OptionId__c = termProduct.Id;
            termEHCLiNew.Apttus_Config2__LineStatus__c = 'New';
            liListToInsert.add(termEHCLiNew);
            if(!liListToInsert.isEmpty())
                insert liListToInsert;
        } catch (Exception e){
			System.debug(e.getStackTraceString());
		}
    }
    
    @isTest
    static void pavCallTest(){
        try{
            Test.startTest();
            
            Apttus_Config2__ConfigCustomClasses__c customSetting = new Apttus_Config2__ConfigCustomClasses__c();
            customSetting.Name = 'Custom Classes';
            customSetting.Apttus_Config2__ProductAttributeCallbackClass__c = 'APTS_ProductAttributeCallback';
            insert customSetting;
            
            Apttus_Proposal__Proposal__c testProposal = [SELECT Id FROM Apttus_Proposal__Proposal__c 
                                                         WHERE Apttus_Proposal__Proposal_Name__c = 'test Apttus Proposal'];
            
            Apttus_Config2__ProductConfiguration__c config1 = [SELECT ID FROM Apttus_Config2__ProductConfiguration__c 
                                                               WHERE Apttus_QPConfig__Proposald__c = :testProposal.Id];
            
            List<Apttus_Config2__LineItem__c> listLineItem = [SELECT Id, Apttus_Config2__LineStatus__c, Apttus_Config2__OptionId__r.ProductCode, 
                                                              Apttus_Config2__Description__c, Apttus_Config2__AttributeValueId__r.APTS_Selected_Product_Type__c 
                                                              FROM Apttus_Config2__LineItem__c 
                                                              WHERE Apttus_Config2__ConfigurationId__c = :config1.Id]; 
            
            Apttus_Config2__LineItem__c termLi = new Apttus_Config2__LineItem__c();
            Apttus_Config2__LineItem__c enhanLi = new Apttus_Config2__LineItem__c();
            for(Apttus_Config2__LineItem__c li : listLineItem) {
                if(li.Apttus_Config2__LineStatus__c == 'New' && li.Apttus_Config2__Description__c == 'Term')
                    termLi = li;
                
                if(li.Apttus_Config2__LineStatus__c == 'New' && li.Apttus_Config2__Description__c == 'Enhancements')
                    enhanLi = li;
            }
            
            Apttus_Proposal__Proposal__c testEHCProposal = [SELECT Id,CurrencyIsoCode FROM Apttus_Proposal__Proposal__c WHERE Apttus_Proposal__Proposal_Name__c = 'test EHC Proposal'];
            Apttus_Config2__ProductConfiguration__c configEHC = [SELECT ID FROM Apttus_Config2__ProductConfiguration__c 
                                                                 WHERE Apttus_QPConfig__Proposald__c = :testEHCProposal.Id];
            
            List<Apttus_Config2__LineItem__c> listLineItemEHC = [SELECT Id, Apttus_Config2__LineStatus__c, Apttus_Config2__OptionId__r.ProductCode, 
                                                                 Apttus_Config2__Description__c, Apttus_Config2__AttributeValueId__r.APTS_Selected_Product_Type__c 
                                                                 FROM Apttus_Config2__LineItem__c 
                                                                 WHERE Apttus_Config2__ConfigurationId__c = :configEHC.Id 
                                                                 AND Apttus_Config2__LineStatus__c = 'New' 
                                                                 AND Apttus_Config2__Description__c = 'Term'];
            
            Apttus_Config2.CallbackTester.testProductAttributeCallback
                                                  ( config1.Id
                                                  , termLi
                                                  , new APTS_ProductAttributeCallback());
            
            Apttus_Config2.CallbackTester.testProductAttributeCallback
                                                  ( config1.Id
                                                  , enhanLi
                                                  , new APTS_ProductAttributeCallback());
            
            Apttus_Config2.CallbackTester.testProductAttributeCallback
                                                  ( configEHC.Id
                                                  , listLineItemEHC[0]
                                                  , new APTS_ProductAttributeCallback());
            Test.stopTest();
        } catch (Exception e){
			System.debug(e.getStackTraceString());
		}
    }
    
}