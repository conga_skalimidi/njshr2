/**
 * Created by bbellanca on 1/29/2018.
 */
public class EnterCatalogFromOpportunityController {
	public Id opportunityID;

	public EnterCatalogFromOpportunityController(ApexPages.StandardController controller) {
		opportunityID = controller.getId();
	}

	public PageReference redirect() {
		Map<String, Object> parmMap = new Map<String, Object>();
		parmMap.put('opportunityID', opportunityID);
		Flow.Interview.Create_Proposal_From_Opportunity createProposalFlow = new Flow.Interview.Create_Proposal_From_Opportunity(parmMap);
		createProposalFlow.start();
		PageReference pageRef = new PageReference('/apex/Apttus_QPConfig__ProposalConfiguration?flow=NGDefault&id=' + createProposalFlow.proposalID);
		return pageRef;
	}

}