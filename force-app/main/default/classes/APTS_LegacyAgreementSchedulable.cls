/************************************************************************************************************************
@Name: APTS_LegacyAgreementSchedulable
@Author: Avinash Bamane
@CreateDate: 25/05/2020
@Description: Schedulable class used to finalize the cart. This logic is implemented as schedulable to avoid number of 
Queueable executions limits in single transaction.
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:

************************************************************************************************************************/

public class APTS_LegacyAgreementSchedulable implements Schedulable {
    private Id cartId;

    /** 
    @description: APTS_LegacyAgreementSchedulable Constructor
    @param: cartId - Configuration Id
    @return:
    */
    public APTS_LegacyAgreementSchedulable(Id cartId) {
        this.cartId = cartId;
    }
    
    /** 
    @description: Use out of the box APIs to finalize the cart.
    @param: SchedulableContext
    @return:
    */
    public void execute(SchedulableContext sc) {
        Apttus_CpqApi.CPQ.FinalizeCartRequestDO requestFinalize = new Apttus_CpqApi.CPQ.FinalizeCartRequestDO();
        requestFinalize.CartId = cartId;
        Apttus_CpqApi.CPQ.FinalizeCartResponseDO responseFinalize = Apttus_CpqApi.CPQWebService.finalizeCart(requestFinalize);
    }
}