/*
* Class Name: APTS_AccountUpdaterBatchTestSetUp
* Description: 
* Test class data factory
* Created By: DEV Team, Apttus

* Modification Log
* ------------------------------------------------------------------------------------------------------------------------------------------------------------
* Developer               Date           US/Defect        Description
* ------------------------------------------------------------------------------------------------------------------------------------------------------------
* Avinash Bamane		 18/12/19		  GCM-6804 			 Added
* Avinash Bamane		 29/07/20		  GCM-8415			 Updated
*/

@isTest
public class APTS_AccountUpdaterBatchTestSetUp {
    public static void setupBatchData (Integer numberOfAccounts, Integer numberOfAgreements) {
        List<Account> accList = new List<Account>();
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();
        for(integer i = 0; i < numberOfAccounts; i++) {
            Account accToInsert = new Account();
            accToInsert.Name = 'APTS Test Account Name '+i;
            //accToInsert.APTS_Has_Active_Agreement__c = true;
            accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
            accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
            accList.add(accToInsert);
        }
        insert accList;
        
        accList = [SELECT Id from Account];
        List<Apttus__APTS_Agreement__c> aggList = new List<Apttus__APTS_Agreement__c>();
        // NJUS Agreements
        for(integer i = 0; i < numberOfAgreements; i++) {
            Integer randomNumber = Integer.valueof((Math.random() * numberOfAccounts));
            Apttus__APTS_Agreement__c aggToInsert = new Apttus__APTS_Agreement__c();
            aggToInsert.Name = 'APTS Test Agreement '+i;
            aggToInsert.Program__c = 'NetJets U.S.';
            aggToInsert.Apttus__Account__c = accList.get(randomNumber).Id;
            aggToInsert.RecordTypeId = recordTypeIdNJUS;
            aggToInsert.Preferred_Bank__c = '';
            aggToInsert.Apttus__Contract_End_Date__c = Date.today()+365;
            if((randomNumber/2) == 0) {
                aggToInsert.Apttus__Status_Category__c = 'In Effect';
                aggToInsert.Apttus__Status__c = 'Activated';
            } else {
                aggToInsert.Apttus__Status_Category__c = 'Request';
                aggToInsert.Apttus__Status__c = 'Request';
            }
            aggList.add(aggToInsert);
        }
        
        // NJE Agreements
        for(integer i = 0; i < numberOfAgreements-5; i++) {
            Integer randomNumber = Integer.valueof((Math.random() * numberOfAccounts));
            Apttus__APTS_Agreement__c aggToInsert = new Apttus__APTS_Agreement__c();
            aggToInsert.Name = 'APTS Test Agreement '+i;
            aggToInsert.Program__c = 'NetJets Europe';
            aggToInsert.RecordTypeId = recordTypeIdNJE;
            aggToInsert.Preferred_Bank__c = '';
            aggToInsert.Apttus__Contract_End_Date__c = Date.today()+365;
            aggToInsert.Apttus__Account__c = accList.get(randomNumber).Id;
            if((randomNumber/2) == 0) {
                aggToInsert.Apttus__Status_Category__c = 'In Effect';
                aggToInsert.Apttus__Status__c = 'Activated';
            } else {
                aggToInsert.Apttus__Status_Category__c = 'Request';
                aggToInsert.Apttus__Status__c = 'Request';
            }
            aggList.add(aggToInsert);
        }
        insert aggList;
    }
}