/***********************************************************************************************************************
@Name: APTS_QuoteProposalBaseTriggerEHCTermTest
@Author: Conga DEV Team
@CreateDate: 25 March 2021
@Description: APEX class to test classes APTS_QuoteProposalBaseTrigger,APTS_QuoteProposalBaseTriggerHelper,APTS_QuoteProposalBaseTriggerHandler
Covers extended scenarios of EHC and termination
************************************************************************************************************************/
@isTest
public class APTS_QuoteProposalBaseTriggerEHCTermTest {
	Public static Product2 testProduct;
    Public static Product2 fromACProduct;
    Public static Product2 toACProduct;
    Public static Apttus_Proposal__Proposal__c NSProposal;
    Public static Apttus_Proposal__Proposal__c NCSCProposal;
    Public static Apttus_Proposal__Proposal__c AssignmentProposal;
    Public static Apttus_Proposal__Proposal__c EHCProposal;
    Public static Apttus_Proposal__Proposal__c TerminationProposal;
    Public static Apttus_Proposal__Proposal__c TEProposal;
    Public static list<Apttus_Config2__LineItem__c> NSlistLineItem;
    Public static List<Apttus_Proposal__Proposal__c> proposalList;
    Public static Apttus_Config2__AccountLocation__c testAccLocation;
    Public static list<Apttus_Config2__LineItem__c> NCSClistLineItem;
    Public static list<Apttus_Config2__LineItem__c> assignlistLineItem;    
    
    @testsetup
    static void setupData(){
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus_Proposal__Proposal__c.getRecordTypeInfosByName().get('Proposal NJUS').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus_Proposal__Proposal__c.getRecordTypeInfosByName().get('Proposal NJE').getRecordTypeId();
        
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
        accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
        insert accToInsert;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator' LIMIT 1];
        User newUser = [SELECT Id FROM User WHERE ProfileId =: p.Id and isActive = true LIMIT 1];
                
        List<AccountTeamMember> acctTeamMembersList = new List<AccountTeamMember>();
            AccountTeamMember acctTeamMember = new AccountTeamMember();
            acctTeamMember.UserId = newUser.Id;
            acctTeamMember.TeamMemberRole = 'Regional_Sales_Vice_President';
            acctTeamMember.AccountId  = accToInsert.Id;
            acctTeamMember.NetJets_Company__c = 'NJA';
            acctTeamMembersList.add(acctTeamMember);
            AccountTeamMember acctTeamMember1 = new AccountTeamMember();
            acctTeamMember1.UserId = newUser.Id;
            acctTeamMember1.TeamMemberRole = 'Sales_Executive';
            acctTeamMember1.AccountId  = accToInsert.Id;
            acctTeamMember.NetJets_Company__c = 'NJA';
            acctTeamMembersList.add(acctTeamMember1);
            AccountTeamMember acctTeamMember2 = new AccountTeamMember();
            acctTeamMember2.UserId = newUser.Id;
            acctTeamMember2.TeamMemberRole = 'Account_Executive';
            acctTeamMember2.AccountId  = accToInsert.Id;
            acctTeamMember.NetJets_Company__c = 'NJE';
            acctTeamMembersList.add(acctTeamMember2);
            insert acctTeamMembersList;
        
            testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', accToInsert.Id);
            insert testAccLocation;
            
            Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
            insert testOpportunity;
            
            Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
            insert testPriceList;
            
            testProduct = APTS_CPQTestUtility.createProduct('Card', 'APTS', 'Apttus', 'Bundle', true, true, true, true);
            insert testProduct;
            
            proposalList = new List<Apttus_Proposal__Proposal__c>();
            
            //New Sale
            NSProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
            NSProposal.APTS_New_Sale_or_Modification__c = APTS_Constants.MODIFICATION;
            NSProposal.RecordTypeId  = recordTypeIdNJUS;
			NSProposal.APTS_Modification_Type__c = 'Direct Conversion';
			NSProposal.APTS_Additional_Modification_Type__c = 'Term Extension';
            NSProposal.APTPS_Program_Type__c = 'Card';
            NSProposal.Requires_Re_Approval__c = true;
            proposalList.add(NSProposal);
        
        	//NCSC
            NCSCProposal = APTS_CPQTestUtility.createProposal('test Apttus ProposalNCSC', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
            //populate custom fields value if required
            NCSCProposal.APTS_New_Sale_or_Modification__c = APTS_Constants.MODIFICATION;
            NCSCProposal.APTS_Modification_Type__c = APTS_Constants.MODIFICATION_TYPE_NEWCARDSALE_CONVERSION;
            NCSCProposal.RecordTypeId  = recordTypeIdNJUS;
            NCSCProposal.APTPS_Program_Type__c = 'Card';
            NCSCProposal.Requires_Re_Approval__c = true;
            proposalList.add(NCSCProposal);
            
            //Assignment
            AssignmentProposal = APTS_CPQTestUtility.createProposal('Assignment Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
            //populate custom fields value if required
            AssignmentProposal.APTS_New_Sale_or_Modification__c = APTS_Constants.MODIFICATION;
            AssignmentProposal.APTS_Modification_Type__c = APTS_Constants.MODIFICATION_TYPE_ASSIGNMENT;
            AssignmentProposal.RecordTypeId  = recordTypeIdNJUS;
            AssignmentProposal.APTPS_Program_Type__c = 'Card';
            AssignmentProposal.Requires_Re_Approval__c = true;
            proposalList.add(AssignmentProposal);
            
            //EHC
            EHCProposal = APTS_CPQTestUtility.createProposal('EHC Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
            //populate custom fields value if required
            EHCProposal.APTS_New_Sale_or_Modification__c = APTS_Constants.MODIFICATION;
            EHCProposal.APTS_Modification_Type__c = APTS_Constants.EHC;
            EHCProposal.RecordTypeId  = recordTypeIdNJE;
            EHCProposal.APTPS_Program_Type__c = 'Card';
            EHCProposal.Requires_Re_Approval__c = true;
            proposalList.add(EHCProposal);
            
            //Termination
            TerminationProposal = APTS_CPQTestUtility.createProposal('Termination Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
            //populate custom fields value if required
            TerminationProposal.APTS_New_Sale_or_Modification__c = APTS_Constants.MODIFICATION;
            TerminationProposal.APTS_Modification_Type__c = APTS_Constants.MOD_TERMINATION;
            TerminationProposal.RecordTypeId  = recordTypeIdNJUS;
            TerminationProposal.APTS_Reason_for_Termination__c  = APTS_Constants.TERMINATION_REASON_DEATH;
            TerminationProposal.APTPS_Program_Type__c = 'Card';
            TerminationProposal.Requires_Re_Approval__c = true;
            proposalList.add(TerminationProposal);
            
            //TermExtension
            TEProposal = APTS_CPQTestUtility.createProposal('TermExtension Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
            //populate custom fields value if required
            TEProposal.APTS_New_Sale_or_Modification__c = APTS_Constants.MODIFICATION;
            TEProposal.APTS_Modification_Type__c = APTS_Constants.TERM_EXTENSION;
            TEProposal.RecordTypeId  = recordTypeIdNJUS;
            TEProposal.APTPS_Program_Type__c = 'Card';
            TEProposal.Requires_Re_Approval__c = true;
            proposalList.add(TEProposal);
            insert proposalList;
        
        	 fromACProduct = APTS_CPQTestUtility.createProduct('Phenom 300', 'PHENOM_300_NJUS', 'Aircraft', 'Option', true, true, true, true);
            //populate custom fields value if required
            fromACProduct.Cabin_Class__c = 'Light';
            //fromACProduct.Ferry_Waiver_Zone_Rank__c = 0;
            
            insert fromACProduct;
             toACProduct = APTS_CPQTestUtility.createProduct('Citation Excel', 'NJUS_CITATION_EXCEL', 'Aircraft', 'Option', true, true, true, true);
            //populate custom fields value if required
            toACProduct.Cabin_Class__c = 'Light';
            //toACProduct.Ferry_Waiver_Zone_Rank__c = 0;
            insert toACProduct;
             //Card PLI
			Apttus_Config2__PriceListItem__c cardPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testProduct.Id, 0.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);
			insert cardPLI;
            
        	
            String AssignmentConfigurationId = APTS_CPQTestUtility.createConfiguration(AssignmentProposal.Id);
            String EHCConfigurationId = APTS_CPQTestUtility.createConfiguration(EHCProposal.Id);
        	
            
        	List<Apttus_Config2__LineItem__c> cardLineItemToInsert = new List<Apttus_Config2__LineItem__c>();
            
        	Apttus_Config2__LineItem__c cardLineItem3 = APTS_CPQTestUtility.getLineItem(AssignmentConfigurationId, NSProposal, cardPLI, testProduct, testProduct.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
            cardLineItem3.Apttus_Config2__PricingStatus__c = 'Pending';
        	cardLineItemToInsert.add(cardLineItem3);   
        
        	insert cardLineItemToInsert;
            
        		
            assignlistLineItem = [SELECT Id FROM Apttus_Config2__LineItem__c WHERE Apttus_Config2__ConfigurationId__c = :AssignmentConfigurationId];
    }
    
   
    
    @isTest
        static void proposalBaseTriggerEHCTest(){
            setupData();
            //Create SNL Custom settings
            APTPS_SNL_Products__c snlProducts = new APTPS_SNL_Products__c();
            snlProducts.Products__c = 'Demo,Corporate Trial';
            insert snlProducts;
            //Populate Proposal Line item data for EHC
            List<Apttus_Proposal__Proposal_Line_Item__c> EHClineItemList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
            Apttus_Proposal__Proposal_Line_Item__c EHCquoteli = new Apttus_Proposal__Proposal_Line_Item__c();
            EHCquoteli.Apttus_Proposal__Product__c = testProduct.Id;
            EHCquoteli.Apttus_QPConfig__LineStatus__c = 'New';
            EHCquoteli.Apttus_Proposal__Proposal__c = EHCProposal.Id;
            EHCquoteli.Option_Product_Name__c = APTS_Constants.UPGRADE;
            EHCquoteli.Apttus_QPConfig__DerivedFromId__c = assignlistLineItem[0].id;
            EHCquoteli.Apttus_QPConfig__ChargeType__c = 'Purchase Price';
            insert EHCquoteli;          
                        
            Apttus_QPConfig__ProposalProductAttributeValue__c EHCpav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
            EHCpav.Apttus_QPConfig__LineItemId__c = EHCquoteli.id;
            EHCpav.Proposed_Extended_End_Date__c  = System.today();
            EHCpav.APTS_Bonus_Hours__c = 1;
            EHCpav.APTS_Credit_Amount__c = 100;
            EHCpav.Aircraft_Hours__c = 25;
            insert EHCpav;
            EHCquoteli.Apttus_QPConfig__AttributeValueId__c = EHCpav.id;
            update EHCquoteli;
            
            Test.startTest();
            APTS_QuoteProposalBaseTriggerHelper.printFlagStatus(proposalList);
            proposalList[3].Apttus_QPConfig__ConfigurationFinalizedDate__c = System.today();
            update proposalList;
        }
        @isTest
        static void proposalBaseTriggerTerminationTest(){
            setupData();
            //Create SNL Custom settings
            APTPS_SNL_Products__c snlProducts = new APTPS_SNL_Products__c();
            snlProducts.Products__c = 'Demo,Corporate Trial';
            insert snlProducts;
            //Populate Proposal Line item data for Termination
            List<Apttus_Proposal__Proposal_Line_Item__c> TerminationLIList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
            Apttus_Proposal__Proposal_Line_Item__c TerminationQli = new Apttus_Proposal__Proposal_Line_Item__c();
            TerminationQli.Apttus_Proposal__Product__c = testProduct.Id;
            TerminationQli.Apttus_QPConfig__LineStatus__c = 'New';
            TerminationQli.Apttus_Proposal__Proposal__c = TerminationProposal.Id;
            TerminationQli.Option_Product_Name__c = APTS_Constants.ENHANCEMENTS;
            TerminationQli.Apttus_QPConfig__DerivedFromId__c = assignlistLineItem[0].id;
            TerminationQli.Apttus_QPConfig__ChargeType__c = 'Purchase Price';
            insert TerminationQli;
            
            Apttus_QPConfig__ProposalProductAttributeValue__c Terminationpav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
            Terminationpav.Apttus_QPConfig__LineItemId__c = TerminationQli.id;
            Terminationpav.Proposed_Extended_End_Date__c  = System.today();
            Terminationpav.APTS_Bonus_Hours__c = 1;
            Terminationpav.APTS_Credit_Amount__c = 100;
            Terminationpav.Aircraft_Hours__c = 25;
            insert Terminationpav;
            TerminationQli.Apttus_QPConfig__AttributeValueId__c = Terminationpav.id;
            update TerminationQli;
            
            Test.startTest();
            APTS_QuoteProposalBaseTriggerHelper.printFlagStatus(proposalList);
            proposalList[4].Apttus_QPConfig__ConfigurationFinalizedDate__c = System.today();
            update proposalList;
            
            Test.stopTest();
        
        }
}