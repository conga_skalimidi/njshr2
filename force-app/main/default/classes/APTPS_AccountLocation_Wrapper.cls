/*************************************************************
@Name: APTPS_AccountLocation_Wrapper
@Author: Siva Kumar
@CreateDate: 21 October 2020
@Description : This class is used to prevent Duplicate values for Account Location
******************************************************************/
public class APTPS_AccountLocation_Wrapper {
    public String name;
    public String full_name;
    public String accountId;
    
    public APTPS_AccountLocation_Wrapper(string alName , string alFull_name, String alAccountId){
        name = alName;
        full_name = alFull_name;
        accountId = alAccountId;
    }
    
    public Boolean equals(Object obj) {
        if (obj instanceof APTPS_AccountLocation_Wrapper){
            APTPS_AccountLocation_Wrapper p = (APTPS_AccountLocation_Wrapper)obj;
            return ((name == p.name) && (full_name == p.full_name) && (accountId == p.accountId));
        }
        return false;
    }
    
    public Integer hashCode() {
        if(name!=null && full_name!=null && accountId!=null) {
            return (name.hashCode() * full_name.hashCode() * accountId.hashCode());
        } else {
            return 0;
        }
        
    }
}