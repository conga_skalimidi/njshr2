/************************************************************************************************************************
@Name: APTPS_AgreementExtFields
@Author: Conga PS Dev Team
@CreateDate: 16 July 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public with sharing class APTPS_AgreementExtFields {
     /** 
    @description:  populate AgreementExtension Fileds
    @param: Agreement List
    @return: 
    */
    public static void populateAgreementExtFields(List<Apttus__APTS_Agreement__c> agreementList) {
        List<APTPS_Agreement_Extension__c> agrExtListResult = new List<APTPS_Agreement_Extension__c>();
        Map<id, Id> agrToExtMapping = new Map<id, id>();
        Map<id, List<Apttus_Proposal__Proposal_Line_Item__c>> quoteToPropLinesMapping = new Map<id, List<Apttus_Proposal__Proposal_Line_Item__c>>();
        for (Apttus__APTS_Agreement__c agr : agreementList){
            agrToExtMapping.put(agr.Apttus_QPComply__RelatedProposalId__c, agr.APTPS_Agreement_Extension__c);
        }
        
        Map<Id, APTPS_Agreement_Extension__c> agrExtensionsMap = new Map<Id, APTPS_Agreement_Extension__c>([SELECT Id,APTPS_NetJets_Sales_Entity__c, APTPS_NetJets_Manager_Entity__c, APTPS_Contract_Borrowing_Hours__c, APTPS_Contract_Advance_Notice__c, APTPS_Contract_Compliance__c, APTPS_Contract_Insurance_Limit__c, APTPS_Contract_War_Risk_Limit__c, APTPS_Contract_Purchase_Deposit__c FROM APTPS_Agreement_Extension__c WHERE Id IN :agrToExtMapping.values()]);
        
        for (Apttus_Proposal__Proposal_Line_Item__c pli : [SELECT Id, Apttus_QPConfig__OptionId__r.Name, Apttus_QPConfig__OptionId__r.ProductCode, Apttus_QPConfig__LineType__c, Apttus_QPConfig__NetPrice__c, Product_Family__c, Apttus_QPConfig__IsPrimaryLine__c, Apttus_QPConfig__ChargeType__c, Apttus_Proposal__Proposal__c,Apttus_QPConfig__AttributeValueId__r.APTPS_Borrowing__c ,Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c, Apttus_QPConfig__OptionId__r.Cabin_Class__c 
                                                           FROM Apttus_Proposal__Proposal_Line_Item__c
                                                           WHERE Apttus_Proposal__Proposal__c IN :agrToExtMapping.keySet()]){
            if (quoteToPropLinesMapping.containsKey(pli.Apttus_Proposal__Proposal__c)){
                List<Apttus_Proposal__Proposal_Line_Item__c> existingLines = quoteToPropLinesMapping.get(pli.Apttus_Proposal__Proposal__c);
                existingLines.add(pli);
                quoteToPropLinesMapping.put(pli.Apttus_Proposal__Proposal__c, existingLines);
            } else{
                quoteToPropLinesMapping.put(pli.Apttus_Proposal__Proposal__c, new List<Apttus_Proposal__Proposal_Line_Item__c>{pli});
            }
        
        }
        System.debug('quoteToPropLinesMapping=>' + quoteToPropLinesMapping);
        try{

            for (id quoteId : quoteToPropLinesMapping.keySet()){
                APTPS_Agreement_Extension__c extension = agrExtensionsMap.get(agrToExtMapping.get(quoteId));
                System.debug('extension==>' + extension);
                System.debug('Line Items==>' + quoteToPropLinesMapping.get(quoteId));
                extension.APTPS_Contract_Borrowing_Hours__c = 0;
                extension.APTPS_NetJets_Sales_Entity__c = APTS_ConstantUtil.NJ_SALES_ENTITY ;
                extension.APTPS_NetJets_Manager_Entity__c = APTS_ConstantUtil.NJ_MANAGER_ENTITY;
                for (Apttus_Proposal__Proposal_Line_Item__c pli : quoteToPropLinesMapping.get(quoteId)){
                    if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.AIRCRAFT.equalsIgnoreCase(pli.Product_Family__c) 
                    && APTS_ConstantUtil.PURCHASE_PRICE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                        if(APTS_ConstantUtil.G450_AIRCRAFT.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Name)) {
                            extension.APTPS_NetJets_Sales_Entity__c = APTS_ConstantUtil.NJI_SALES_ENTITY ;
                            extension.APTPS_NetJets_Manager_Entity__c = APTS_ConstantUtil.NJI_MANAGER_ENTITY;
                        } 
                        if(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Borrowing__c) {
                            integer hours = Integer.valueOf(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c);
                            extension.APTPS_Contract_Borrowing_Hours__c = (hours)*(0.25);
                        }
                        if(APTS_ConstantUtil.LIGHT_CABIN.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Cabin_Class__c)) {
                            extension.APTPS_Contract_Compliance__c = 5000;
                            extension.APTPS_Contract_War_Risk_Limit__c = 100000000;
                            //extension.APTPS_Contract_Purchase_Deposit__c = 100000/50;
                        } else if(APTS_ConstantUtil.MID_CABIN.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Cabin_Class__c)) {
                            extension.APTPS_Contract_Compliance__c = 10000;
                            extension.APTPS_Contract_War_Risk_Limit__c = 150000000;
                            //extension.APTPS_Contract_Purchase_Deposit__c = 150000/50;
                        }  else if(APTS_ConstantUtil.SUPER_MID_CABIN.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Cabin_Class__c)) {
                            extension.APTPS_Contract_War_Risk_Limit__c = 150000000;
                            //extension.APTPS_Contract_Purchase_Deposit__c = 150000/50;
                        } else if(APTS_ConstantUtil.LARGE_CABIN.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Cabin_Class__c)) {
                            extension.APTPS_Contract_Compliance__c = 15000;
                            extension.APTPS_Contract_War_Risk_Limit__c = 250000000;
                            //extension.APTPS_Contract_Purchase_Deposit__c = 200000/50;
                        }
                        if(APTS_ConstantUtil.GLOBAL7500_AIRCRAFT.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Name)){
                            //extension.APTPS_Contract_Purchase_Deposit__c = 500000/50;
                        }
                        if(APTS_ConstantUtil.LIGHT_CABIN.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Cabin_Class__c) && Integer.valueOf(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) < 200) {
                            extension.APTPS_Contract_Advance_Notice__c = 8;
                        } else if(APTS_ConstantUtil.LIGHT_CABIN.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.Cabin_Class__c) && Integer.valueOf(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) > 200) {
                            extension.APTPS_Contract_Advance_Notice__c = 4;
                        } else if(pli.Apttus_QPConfig__OptionId__r.Cabin_Class__c != APTS_ConstantUtil.LIGHT_CABIN && Integer.valueOf(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) < 200) {
                            extension.APTPS_Contract_Advance_Notice__c = 10;
                        } else if(pli.Apttus_QPConfig__OptionId__r.Cabin_Class__c != APTS_ConstantUtil.LIGHT_CABIN && Integer.valueOf(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) > 200) {
                            extension.APTPS_Contract_Advance_Notice__c = 6;
                        }
                        
                    }
                }                
                agrExtListResult.add(extension);
            }
            if(!agrExtListResult.isEmpty()) {
                update agrExtListResult;
            }
            
        } catch (Exception e){
            System.debug('Exception in Class-APTPS_AgreementExtFields: Method- populateAgreementExtFields() at line' + e.getLineNumber()+' Message->' + e.getMessage());
        }
    }
}