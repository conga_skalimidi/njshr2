/************************************************************************************************************************
@Name: APTPS_CLMLifeCycleInterface
@Author: Conga PS Dev Team
@CreateDate: 4 June 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public interface APTPS_CLMLifeCycleInterface {
    void callCLMAction(Map<String, Object> args);
    
}