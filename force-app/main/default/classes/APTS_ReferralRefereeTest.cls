/*******************************************************************************************************************************************
@Name: APTS_ReferralRefereeTest
@Author: Avinash Bamane
@CreateDate: 28/10/2020
@Description: Test class for APTS_GenerateReferralRefereeAgreement 
********************************************************************************************************************************************
@ModifiedBy: Avinash Bamane
@ModifiedDate: 04/11/2020
@ChangeDescription: Removed SeeAllData test approach
********************************************************************************************************************************************/

@IsTest
public class APTS_ReferralRefereeTest {    
    @TestSetup
    public static void setupData() {
        try{
            
            APTS_Refereer_Referee_Terms__c termRR = new APTS_Refereer_Referee_Terms__c();
            termRR.APTS_Delayed_Start_Amount_Months__c = String.valueOf(0);
            termRR.APTS_Initial_Term_Amount_Months__c = String.valueOf(6);
            termRR.APTS_Grace_Period_Amount_Months__c = String.valueOf(12);
            insert termRR;
            
            Account testAccount = APTS_CPQTestUtility.createAccount('Test R&R', 'Other');
            insert testAccount;
            
            Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('New Sale Opportunity', 'New', testAccount.Id, 'Closed Won');
            insert testOpportunity;
            
            Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', testAccount.Id);
            insert testAccLocation;
            
            Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('USA', true);
            insert testPriceList;
            
            list<Product2> lstProducts = new list<Product2>();
            
            // Insert Card Bundle (US)
            Product2 testCardProduct = APTS_CPQTestUtility.createProduct('Card', 'NJUS_CARD', 'Netjets Product', 'Bundle', true, true, true, true);
            testCardProduct.Program__c = 'NetJets U.S.';
            
            // Insert Product Type Option (US)
            Product2 testECProduct = APTS_CPQTestUtility.createProduct('Elite Combo', 'ELITE_COMBO_NJUS', 'Product Type', 'Option', true, true, true, true);
            testECProduct.Program__c = 'NetJets U.S.';
            testECProduct.Cabin_Class__c = 'Mid-Size';
            
            // Insert Aircraft 1 Option (US)
            Product2 testAircraft1Product = APTS_CPQTestUtility.createProduct('Gulfstream 450', 'GULFSTREAM_450_NJUS', 'Aircraft', 'Option', true, true, true, true);
            testAircraft1Product.Program__c = 'NetJets U.S.';
            testAircraft1Product.Cabin_Class__c = 'Large';
            
            // Insert Aircraft 2 Option (US)
            Product2 testAircraft2Product = APTS_CPQTestUtility.createProduct('Citation Sovereign', 'CITATION_SOVEREIGN_NJUS', 'Aircraft', 'Option', true, true, true, true);
            testAircraft2Product.Program__c = 'NetJets U.S.';
            testAircraft2Product.Cabin_Class__c = 'Mid-Size';
            
            Product2 enhancementProduct = APTS_CPQTestUtility.createProduct('Enhancement', 'Enhancements', 'Enhancement', 'Option', true, true, true, true);
            enhancementProduct.Program__c = 'NetJets U.S.';
            
            Product2 termProduct = APTS_CPQTestUtility.createProduct('Term', 'NJUS_TERM', 'Term', 'Option', true, true, true, true);
            termProduct.Program__c = 'NetJets U.S.';
            
            lstProducts.add(testCardProduct);
            lstProducts.add(testECProduct);
            lstProducts.add(testAircraft1Product);
            lstProducts.add(testAircraft2Product);
            lstProducts.add(enhancementProduct);
            lstProducts.add(termProduct);
            insert lstProducts;
            
            Apttus_Config2__ClassificationName__c categorySo = APTS_CPQTestUtility.createCategory('NJUS Product Type', 'NJUS Product Type', 'Option Group', null, true);
            insert categorySo;
            
            Apttus_Config2__ClassificationHierarchy__c catHier = new Apttus_Config2__ClassificationHierarchy__c();
            catHier.Name = 'NJUS Product Type';
            catHier.Apttus_Config2__Label__c = 'Product Type';
            catHier.Apttus_Config2__HierarchyId__c = categorySo.Id;
            catHier.Apttus_Config2__MinOptions__c = 1;
            catHier.Apttus_Config2__MaxOptions__c = 1;
            catHier.Apttus_Config2__IncludeInTotalsView__c = TRUE;
            insert catHier;
            
            Apttus_Config2__ProductOptionGroup__c optGroup = new Apttus_Config2__ProductOptionGroup__c();
            optGroup.Apttus_Config2__RootOptionGroupId__c = catHier.Id;
            optGroup.Apttus_Config2__RootSequence__c = 1;
            optGroup.Apttus_Config2__Sequence__c = 1;
            optGroup.Apttus_Config2__OptionGroupId__c = catHier.Id;
            optGroup.Apttus_Config2__ProductId__c = testCardProduct.Id;
            optGroup.Apttus_Config2__MinOptions__c = 1;
            optGroup.Apttus_Config2__MaxOptions__c = 1;
            insert optGroup;
            
            Apttus_Config2__ProductOptionComponent__c optComp = new Apttus_Config2__ProductOptionComponent__c();
            optComp.Apttus_Config2__ComponentProductId__c = testECProduct.Id;
            optComp.Apttus_Config2__RelationshipType__c = 'Option';
            optComp.Apttus_Config2__Sequence__c = 1;
            optComp.Apttus_Config2__Modifiable__c = TRUE;
            optComp.Apttus_Config2__ParentProductId__c = testCardProduct.Id;
            optComp.Apttus_Config2__ProductOptionGroupId__c = optGroup.Id;
            insert optComp;
            
            list<Apttus_Config2__PriceListItem__c> lstPLI = new list<Apttus_Config2__PriceListItem__c>();
            
            //Card PLI
            Apttus_Config2__PriceListItem__c cardPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testCardProduct.Id, 0.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);
            
            //Elite Combo PLI's
            Apttus_Config2__PriceListItem__c ecPLI1 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testECProduct.Id, 0.0, 'Combo Card Premium FET', 'One Time', 'Flat Price', 'Each', true);
            Apttus_Config2__PriceListItem__c ecPLI2 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testECProduct.Id, 0.0, 'Product Type', 'One Time', 'Flat Price', 'Each', true);
            Apttus_Config2__PriceListItem__c ecPLI3 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testECProduct.Id, 0.0, 'Combo Card Premium', 'One Time', 'Per Unit', 'Each', true);
            
            //Gulfstream 450 PLI's
            Apttus_Config2__PriceListItem__c GSPLI1 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft1Product.Id, 0.0, 'Existing Asset', 'One Time', 'Flat Price', 'Each', true);
            Apttus_Config2__PriceListItem__c GSPLI2 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft1Product.Id, 0.0, 'Half Card Premium FET', 'One Time', 'Flat Price', 'Each', true);
            Apttus_Config2__PriceListItem__c GSPLI3 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft1Product.Id, 0.0, 'Half Card Premium', 'One Time', 'Per Unit', 'Each', true);
            Apttus_Config2__PriceListItem__c GSPLI4 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft1Product.Id, 0.0, 'Request Hours Value', 'One Time', 'Per Unit', 'Each', true);
            Apttus_Config2__PriceListItem__c GSPLI5 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft1Product.Id, 16796.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);
            Apttus_Config2__PriceListItem__c GSPLI6 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft1Product.Id, 0.0, 'Federal Excise Tax', 'One Time', 'Per Unit', 'Each', true);
            Apttus_Config2__PriceListItem__c GSPLI7 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft1Product.Id, 0.0, 'Expired Hours Value', 'One Time', 'Per Unit', 'Each', true);
            
            //Citation Sovereign PLI's
            Apttus_Config2__PriceListItem__c CSPLI1 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft2Product.Id, 0.0, 'Existing Asset', 'One Time', 'Flat Price', 'Each', true);
            Apttus_Config2__PriceListItem__c CSPLI2 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft2Product.Id, 0.0, 'Half Card Premium FET', 'One Time', 'Flat Price', 'Each', true);
            Apttus_Config2__PriceListItem__c CSPLI3 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft2Product.Id, 0.0, 'Half Card Premium', 'One Time', 'Per Unit', 'Each', true);
            Apttus_Config2__PriceListItem__c CSPLI4 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft2Product.Id, 0.0, 'Request Hours Value', 'One Time', 'Per Unit', 'Each', true);
            Apttus_Config2__PriceListItem__c CSPLI5 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft2Product.Id, 9796.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);
            Apttus_Config2__PriceListItem__c CSPLI6 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft2Product.Id, 0.0, 'Federal Excise Tax', 'One Time', 'Per Unit', 'Each', true);
            Apttus_Config2__PriceListItem__c CSPLI7 = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testAircraft2Product.Id, 0.0, 'Expired Hours Value', 'One Time', 'Per Unit', 'Each', true);
            
            Apttus_Config2__PriceListItem__c enhanPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, enhancementProduct.Id, 0.0, 'Enhancements', 'One Time', 'Per Unit', 'Each', true);
            Apttus_Config2__PriceListItem__c termPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, termProduct.Id, 0.0, 'Term', 'One Time', 'Per Unit', 'Each', true);
            
            lstPLI.add(cardPLI);
            lstPLI.add(ecPLI1);
            lstPLI.add(ecPLI2);
            lstPLI.add(ecPLI3);
            lstPLI.add(GSPLI1);
            lstPLI.add(GSPLI2);
            lstPLI.add(GSPLI3);
            lstPLI.add(GSPLI4);
            lstPLI.add(GSPLI5);
            lstPLI.add(GSPLI6);
            lstPLI.add(GSPLI7);
            lstPLI.add(CSPLI1);
            lstPLI.add(CSPLI2);
            lstPLI.add(CSPLI3);
            lstPLI.add(CSPLI4);
            lstPLI.add(CSPLI5);
            lstPLI.add(CSPLI6);
            lstPLI.add(CSPLI7);
            lstPLI.add(enhanPLI);
            lstPLI.add(termPLI);
            insert lstPLI;
            
            Apttus_Proposal__Proposal__c nsProposal = APTS_CPQTestUtility.createProposal('New Sale Test Quote', testAccount.Id, testOpportunity.Id, 'Proposal NJUS', testPriceList.Id);
            nsProposal.APTS_New_Sale_or_Modification__c = 'New Sale';
            insert nsProposal;
            
            Apttus__APTS_Agreement__c ag = APTS_CPQTestUtility.createAgreement('Test R/R agreement', 'Active', testAccount.Id, nsProposal.Id);
            system.assert(ag!=null);
            insert ag;
            
            List<Apttus__AgreementLineItem__c> agLineList = new List<Apttus__AgreementLineItem__c>();
            Apttus__AgreementLineItem__c prodTypeALI = APTS_CPQTestUtility.createAgreementLineItem(ag.Id, testECProduct, 'New', TRUE, testAccLocation.Id);
            system.assert(prodTypeALI!=null);
            agLineList.add(prodTypeALI);
            
            Apttus__AgreementLineItem__c ac1ALI = APTS_CPQTestUtility.createAgreementLineItem(ag.Id, testAircraft1Product, 'New', TRUE, testAccLocation.Id);
            system.assert(ac1ALI!=null);
            agLineList.add(ac1ALI);
            
            Apttus__AgreementLineItem__c ac2ALI = APTS_CPQTestUtility.createAgreementLineItem(ag.Id, testAircraft2Product, 'New', TRUE, testAccLocation.Id);
            system.assert(ac2ALI!=null);
            agLineList.add(ac2ALI);
            if(!agLineList.isEmpty())    
                insert agLineList;
            
            Apttus__APTS_Agreement__c agCurrent = APTS_CPQTestUtility.createAgreement('Current R/R agreement', 'Active', testAccount.Id, nsProposal.Id);
            system.assert(agCurrent!=null);
            insert agCurrent;
            
            Apttus_Config2__ProductConfiguration__c agConfig = new Apttus_Config2__ProductConfiguration__c();
            agConfig.Apttus_CMConfig__AgreementId__c = agCurrent.Id;
            agConfig.Apttus_Config2__AccountId__c = testAccount.Id;
            agConfig.Apttus_Config2__BusinessObjectRefId__c = agCurrent.Id;
            agConfig.Apttus_Config2__BusinessObjectType__c = 'Agreement';
            agConfig.Apttus_Config2__PriceListId__c = testPriceList.Id;
            insert agConfig;
            
            List<Apttus_Config2__LineItem__c> lineitemList = new List<Apttus_Config2__LineItem__c>();
            
            Apttus_Config2__LineItem__c cardLineItem = APTS_CPQTestUtility.getLineItem(agConfig.Id, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
            cardLineItem.Apttus_Config2__PricingStatus__c = 'Pending';
            lineitemList.add(cardLineItem);
            Apttus_Config2__LineItem__c ECLineItem = APTS_CPQTestUtility.getLineItem(agConfig.Id, nsProposal, ecPLI2, testCardProduct, testCardProduct.Id, testECProduct.Id, 'Option', 1, 2, 2, 2, 1, 1);
            ECLineItem.Apttus_Config2__PricingStatus__c = 'Pending';
            lineitemList.add(ECLineItem);
            Apttus_Config2__LineItem__c AC1LineItem = APTS_CPQTestUtility.getLineItem(agConfig.Id, nsProposal, GSPLI5, testCardProduct, testCardProduct.Id, testAircraft1Product.Id, 'Option', 1, 2, 3, 2, 1, 1);
            AC1LineItem.Apttus_Config2__PricingStatus__c = 'Pending';
            lineitemList.add(AC1LineItem);
            Apttus_Config2__LineItem__c termLineItem = APTS_CPQTestUtility.getLineItem(agConfig.Id, nsProposal, termPLI, testCardProduct, testCardProduct.Id, termProduct.Id, 'Option', 1, 2, 3, 2, 1, 1);
            termLineItem.Apttus_Config2__PricingStatus__c = 'Pending';
            lineitemList.add(termLineItem);
            
            insert lineitemList;
            
            List<Apttus_Config2__ProductAttributeValue__c> pavList = new List<Apttus_Config2__ProductAttributeValue__c>();
            Apttus_Config2__ProductAttributeValue__c cardLinePAV = APTS_CPQTestUtility.createAttributeValue(cardLineItem.Id);
            pavList.add(cardLinePAV);
            
            Apttus_Config2__ProductAttributeValue__c ecLinePAV = APTS_CPQTestUtility.createAttributeValue(ECLineItem.Id);
            ecLinePAV.APTS_Selected_Product_Type__c = 'Elite Combo';
            pavList.add(ecLinePAV);
            
            Apttus_Config2__ProductAttributeValue__c AC1LinePAV = APTS_CPQTestUtility.createAttributeValue(AC1LineItem.Id);
            AC1LinePAV.APTS_Selected_Product_Type__c = 'Elite Combo';
            AC1LinePAV.Aircraft_Remaining_Hours__c = 12.5;
            AC1LinePAV.Aircraft_Hours__c = 2;
            AC1LinePAV.APTS_Bonus_Hours__c = 2;
            AC1LinePAV.APTS_Current_Value__c = 10000.00;
            pavList.add(AC1LinePAV);
            
            Apttus_Config2__ProductAttributeValue__c termLinePAV = APTS_CPQTestUtility.createAttributeValue(termLineItem.Id);
            pavList.add(termLinePAV);
            
            if(!pavList.isEmpty())
                insert pavList;
        } catch (Exception e){
            System.debug(e.getStackTraceString());
        }
    }
    
    @IsTest
    public static void testRef() {
        try{
            Test.startTest();
            List<Apttus__APTS_Agreement__c> agList = [SELECT Id, Name FROM Apttus__APTS_Agreement__c];
            Apttus__APTS_Agreement__c agCurrent = new Apttus__APTS_Agreement__c();
            Apttus__APTS_Agreement__c ag = new Apttus__APTS_Agreement__c();
            for(Apttus__APTS_Agreement__c agr : agList) {
                if('Test R/R agreement'.equalsIgnoreCase(agr.Name))
                    ag = agr;
                if('Current R/R agreement'.equalsIgnoreCase(agr.Name))
                    agCurrent = agr;
            }
            Apttus_Config2__ProductConfiguration__c agConfig = [SELECT Id FROM Apttus_Config2__ProductConfiguration__c 
                                                                WHERE Apttus_CMConfig__AgreementId__c =: agCurrent.Id LIMIT 1];
            
            Product2 testCardProduct = [SELECT Id FROM Product2 WHERE ProductCode = 'NJUS_CARD' LIMIT 1];
            Apttus_Config2__AccountLocation__c testAccLocation = [SELECT Id FROM Apttus_Config2__AccountLocation__c 
                                                                  WHERE Name = 'Loc1' LIMIT 1];
            APTS_GenerateReferralRefereeAgreement.APTS_GenerateReferralRefereeAgreementRequest req = new APTS_GenerateReferralRefereeAgreement.APTS_GenerateReferralRefereeAgreementRequest();
            req.cartId = agConfig.Id;
            req.productProgram = 'NJUS Product Type';
            req.agreementId = ag.Id;
            req.bundleProductId = testCardProduct.Id;
            req.aircraftHours = 1.0;
            req.locationId = testAccLocation.Id;
            req.modificationType = '';
            req.termProductCode = 'NJUS_TERM';
            req.referralCardId = ag.Id;
            req.legalEntityName = 'Test';
            req.referralName = 'Referee Card';
            
            APTS_GenerateReferralRefereeAgreement.APTS_GenerateReferralRefereeAgreementRequest[] generateAgreementRequests =
                new APTS_GenerateReferralRefereeAgreement.APTS_GenerateReferralRefereeAgreementRequest[]{req};
                    APTS_GenerateReferralRefereeAgreement.generateAgreement(generateAgreementRequests);
            // Test with Non-Combo Product
            Product2 testProd = [SELECT Id, ProductCode FROM Product2 WHERE Name = 'Elite Combo' LIMIT 1];
            testProd.ProductCode = 'STANDARD_NJUS';
            update testProd;
            
            APTS_GenerateReferralRefereeAgreement.generateAgreement(generateAgreementRequests);
            Test.stopTest();
        } catch (Exception e){
            System.debug(e.getStackTraceString());
        }
    }
    
    @IsTest
    public static void testRefAddEnh() {
        try{
            Test.startTest();
            List<Apttus__APTS_Agreement__c> agList = [SELECT Id, Name FROM Apttus__APTS_Agreement__c];
            Apttus__APTS_Agreement__c agCurrent = new Apttus__APTS_Agreement__c();
            Apttus__APTS_Agreement__c ag = new Apttus__APTS_Agreement__c();
            for(Apttus__APTS_Agreement__c agr : agList) {
                if('Test R/R agreement'.equalsIgnoreCase(agr.Name))
                    ag = agr;
                if('Current R/R agreement'.equalsIgnoreCase(agr.Name))
                    agCurrent = agr;
            }
            Apttus_Config2__ProductConfiguration__c agConfig = [SELECT Id FROM Apttus_Config2__ProductConfiguration__c 
                                                                WHERE Apttus_CMConfig__AgreementId__c =: agCurrent.Id LIMIT 1];
            
            List<Apttus__AgreementLineItem__c> lineitemList = new List<Apttus__AgreementLineItem__c>();
            for(Apttus__AgreementLineItem__c li : [SELECT Id, Apttus_CMConfig__LineStatus__c 
                                                   FROM Apttus__AgreementLineItem__c 
                                                   WHERE Apttus__AgreementId__r.Id =: ag.Id]) {
                                                       li.Apttus_CMConfig__LineStatus__c = 'Existing';
                                                       lineitemList.add(li);
                                                       
                                                   }
            
            if(!lineitemList.isEmpty())
                update lineitemList;
            
            Product2 testCardProduct = [SELECT Id FROM Product2 WHERE ProductCode = 'NJUS_CARD' LIMIT 1];
            Apttus_Config2__AccountLocation__c testAccLocation = [SELECT Id FROM Apttus_Config2__AccountLocation__c 
                                                                  WHERE Name = 'Loc1' LIMIT 1];
            
            Product2 testProd = [SELECT Id, ProductCode FROM Product2 WHERE Name = 'Elite Combo' LIMIT 1];
            testProd.ProductCode = 'ELITE_COMBO_NJUS';
            update testProd;
            
            APTS_GenerateReferralRefereeAgreement.APTS_GenerateReferralRefereeAgreementRequest req = new APTS_GenerateReferralRefereeAgreement.APTS_GenerateReferralRefereeAgreementRequest();
            req.cartId = agConfig.Id;
            req.productProgram = 'NJUS Product Type';
            req.agreementId = ag.Id;
            req.bundleProductId = testCardProduct.Id;
            req.aircraftHours = 1.0;
            req.locationId = testAccLocation.Id;
            req.modificationType = 'Add Enhancements';
            req.termProductCode = 'NJUS_TERM';
            req.referralCardId = ag.Id;
            req.legalEntityName = 'Test';
            req.referralName = 'Referee Card';
            
            APTS_GenerateReferralRefereeAgreement.APTS_GenerateReferralRefereeAgreementRequest[] generateAgreementRequests =
                new APTS_GenerateReferralRefereeAgreement.APTS_GenerateReferralRefereeAgreementRequest[]{req};
                    APTS_GenerateReferralRefereeAgreement.generateAgreement(generateAgreementRequests);
            // Test with Non-Combo Product
            testProd.ProductCode = 'STANDARD_NJUS';
            update testProd;
            
            APTS_GenerateReferralRefereeAgreement.generateAgreement(generateAgreementRequests);
            Test.stopTest();
        } catch (Exception e){
            System.debug(e.getStackTraceString());
        }
    }
}