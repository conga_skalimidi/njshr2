/*************************************************************
@Name: APTS_CalculateTotalAmount
@Author: Anjali Kumari
@CreateDate: 6 May 2020
@Description : This Action class is called when agreement line item is created
******************************************************************
@Name: APTS_CalculateTotalAmount
@Author: Avinash Bamane
@CreateDate: 13 Oct 2020
@Description : GCM-9737
******************************************************************/
global class APTS_CalculateTotalAmount {
    
    @InvocableMethod(label='Get Combo and Half Card Premium amount' description='to calculate Combo and Half Card Premium amount from agreement line item' category='Agreement Line Item')
    public static void calculate(List<Id> agreementLineItemids) {
        Decimal comboPremiumAmount=0;
        Decimal halfCardPremiumAmount=0;
        Decimal Total_Purchase_Price=0;
        List<Apttus__AgreementLineItem__c>  aliList = new List<Apttus__AgreementLineItem__c>();
        aliList = [select id,Apttus__AgreementId__c,Apttus__AgreementId__r.APTS_Modification_Type__c from Apttus__AgreementLineItem__c where id IN :agreementLineItemids];
        String modificationType ='';
        modificationType = aliList[0].Apttus__AgreementId__r.APTS_Modification_Type__c;
        id agreementId = aliList!=null && aliList.size()>0 ? aliList[0].Apttus__AgreementId__c : null;
        if(agreementId !=null)
            aliList =  [select Apttus__AgreementId__c,Apttus_CMConfig__ChargeType__c,Apttus__NetPrice__c  from Apttus__AgreementLineItem__c where Apttus__AgreementId__c = :agreementId and Apttus_CMConfig__LineType__c ='Product/Service' and Apttus_CMConfig__LineStatus__c ='New'];
        
        try{
           // if(modificationType == APTS_ConstantUtil.DIRECT_CONVERSION || modificationType == APTS_ConstantUtil.EXPIRED_HOURS_CARD ){
                //halfCardPremiumAmount =0.0;
               // comboPremiumAmount = 0.0;                
           // }else{
                for(Apttus__AgreementLineItem__c ali : aliList){
                    
                    if(modificationType != APTS_ConstantUtil.DIRECT_CONVERSION && modificationType != APTS_ConstantUtil.EXPIRED_HOURS_CARD ){
                        if(ali.Apttus_CMConfig__ChargeType__c ==APTS_ConstantUtil.HALF_CARD_PREMIUM)
                            halfCardPremiumAmount = halfCardPremiumAmount + ali.Apttus__NetPrice__c;
                        
                        if(ali.Apttus_CMConfig__ChargeType__c ==APTS_ConstantUtil.COMBO_CARD_PREMIUM)
                            comboPremiumAmount = comboPremiumAmount + ali.Apttus__NetPrice__c; 
                    }
                    
                    //START: GCM-9737
                    if(APTS_ConstantUtil.NEW_CARD_SALE_CONVERSION.equalsIgnoreCase(modificationType) && 
                       (APTS_ConstantUtil.FEDERAL_EXCISE_TAX.equalsIgnoreCase(ali.Apttus_CMConfig__ChargeType__c) || 
                       APTS_ConstantUtil.PURCHASE_PRICE.equalsIgnoreCase(ali.Apttus_CMConfig__ChargeType__c))) {
                           Total_Purchase_Price += ali.Apttus__NetPrice__c;
                       }
                    //END: GCM-9737
                    
                    //GCM-9737: Added condition for New Card Sale Conversion
                    if(modificationType != 'Add Enhancements' && modificationType != 'Term Extension' && modificationType != 'Assignment' && modificationType != 'Termination' && 
                      modificationType != 'New Card Sale Conversion') {
                        Total_Purchase_Price = Total_Purchase_Price + ali.Apttus__NetPrice__c;
                    }
                }
          //  }
          	system.debug('modificationType --> '+modificationType+' Combo Premium Amount --> '+comboPremiumAmount+' Half Card Premium Amount --> '+halfCardPremiumAmount+
                        'Total Purchase Price ---> '+Total_Purchase_Price);
            if(agreementId != null){
                Apttus__APTS_Agreement__c agreement  = new Apttus__APTS_Agreement__c();//[select APTS_Combo_card_premium_amount__c,APTS_Half_Card_Premium_Amount__c from Apttus__APTS_Agreement__c where id = :agreementId ];
                agreement.Id = agreementId;
                agreement.APTS_Combo_card_premium_amount__c = comboPremiumAmount;
                agreement.APTS_Half_Card_Premium_Amount__c = halfCardPremiumAmount;
                agreement.APTS_Total_Purchase_Price__c = Total_Purchase_Price;
                
                update agreement;
            }
        }catch(Exception e){
            System.debug('Exception in APTS_CalculateTotalAmount class ==>'+e.getMessage()+ ' at line =>>>'+ e.getLineNumber());
        }
        
    }
}