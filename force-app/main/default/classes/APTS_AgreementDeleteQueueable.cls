/*******************************************************************************************************************************************
@Name: APTS_AgreementDeleteQueueable
@Author: Avinash Bamane
@CreateDate: 09/03/2021
@Description: Queueable to call Async method
*******************************************************************************************************************************************/

public class APTS_AgreementDeleteQueueable implements Queueable {
	Set<Id> propIdSet = new Set<Id>();
    String status = '';
    String comment = '';
    
    // Constructor: Accept Quote/Proposal List to update
    public APTS_AgreementDeleteQueueable(Set<Id> qIdSet, String qStatus, String qComment) {
        propIdSet = qIdSet;
        status = qStatus;
        comment = qComment;
    }
    
    public void execute(QueueableContext context) {
        // Call Async
        try {
            updateQuoteProposals(propIdSet, status, comment);
        } catch(DmlException e) {
            system.debug('Exception while updating Quote/Proposal list '+e.getStackTraceString());
        } 
    }
    
    @future
    public static void updateQuoteProposals(Set<Id> proposalIdSet, String proposalStatus, String proposalComment) {
        List<Apttus_Proposal__Proposal__c> propList = new List<Apttus_Proposal__Proposal__c>();
    	for(Apttus_Proposal__Proposal__c propObj : [SELECT Id, Proposal_Chevron_Status__c, Apttus_Proposal__Description__c 
                                                    FROM Apttus_Proposal__Proposal__c WHERE ID IN :proposalIdSet]) {
                                                        propObj.Proposal_Chevron_Status__c = proposalStatus;
														propObj.Apttus_Proposal__Description__c = !String.isEmpty(propObj.Apttus_Proposal__Description__c) 
                                                            ? propObj.Apttus_Proposal__Description__c + proposalComment : proposalComment;
                										propList.add(propObj);
        }
        
        system.debug('APTS_AgreementDeleteQueueable.updateQuoteProposals: Updating Quote/Proposals. Count --> '+propList.size());
        if(!propList.isEmpty())
            update propList;
    }
}