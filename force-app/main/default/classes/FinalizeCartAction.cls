public class FinalizeCartAction {

      @InvocableMethod(label='Finalize Cart' description='Finalizes a Cart')
        public static void finalizeCart (Id[] cartIds) {
            for(Id cartId : cartIds) {
            // create the finalize cart request         
       Apttus_CpqApi.CPQ.FinalizeCartRequestDO request = new Apttus_CpqApi.CPQ.FinalizeCartRequestDO();         
       // add request parameters         
       request.CartId = cartId;         
       // finalize the cart         
         Apttus_CpqApi.CPQ.FinalizeCartResponseDO response = Apttus_CpqApi.CPQWebService.finalizeCart(request);    
      }
        }
}