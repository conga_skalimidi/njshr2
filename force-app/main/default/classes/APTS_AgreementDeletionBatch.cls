/*******************************************************************************************************************************************
@Name: APTS_AgreementDeletionBatch
@Author: Avinash Bamane
@CreateDate: 05/03/2021
@Description: GCM-9961: Any agreement that is past 90 days after last modified date and has not been activated will need to be deleted.
*******************************************************************************************************************************************/

public with sharing class APTS_AgreementDeletionBatch implements Database.Batchable<sObject>, Database.Stateful {
    // Read batch related Custom settings.
    static APTPS_Agreement_Deletion_Batch__c batchDetails = APTPS_Agreement_Deletion_Batch__c.getOrgDefaults();
    static String numberOfDays = !String.isEmpty(batchDetails.Number_of_days__c) ? batchDetails.Number_of_days__c : 'LAST_N_DAYS:90';
    static String agStatus = !String.isEmpty(batchDetails.Agreement_Status__c) ? batchDetails.Agreement_Status__c : '\'Contract Generated\', \'Pending Funding\', \'Contract Pending\', \'Draft\'';
    static String proposalStatus = !String.isEmpty(batchDetails.Proposal_Status__c) ? batchDetails.Proposal_Status__c : 'Cancelled';
    static String proposalComment = !String.isEmpty(batchDetails.Proposal_Comment__c) ? batchDetails.Proposal_Comment__c : '';
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('SOQL query parameters are, numberOfDays --> '+numberOfDays+' agStatus --> '+agStatus+' proposalStatus --> '+proposalStatus+' proposalComment --> '+proposalComment);
        String agQuery = 'SELECT Id, Agreement_Status__c, APTS_Path_Chevron_Status__c, Apttus_QPComply__RelatedProposalId__c, Apttus_QPComply__RelatedProposalId__r.Proposal_Chevron_Status__c, APTS_Modification_Type__c, APTS_New_Sale_or_Modification__c FROM Apttus__APTS_Agreement__c WHERE LastModifiedDate < '+numberOfDays+' AND Agreement_Status__c IN ('+agStatus+')';
        system.debug('APTS_AgreementDeletionBatch SOQL query is --> '+agQuery);
        return Database.getQueryLocator(agQuery);
    }
    
    public void execute(Database.BatchableContext BC, List<Apttus__APTS_Agreement__c> agList) {
        try{
            system.debug('Total Number of agreements to process --> '+agList.size());
            Set<Id> proposalIdSet = new Set<Id>();
            Set<Id> ehcProp = new Set<Id>();
            Set<Id> otherModProp = new Set<Id>();

            for(Apttus__APTS_Agreement__c ag : agList) {
                proposalIdSet.add(ag.Apttus_QPComply__RelatedProposalId__c);
                //Sort out EHC and Other Modification Proposals
                if(ag.APTS_Modification_Type__c != null 
                   && APTS_Constants.EHC.equalsIgnoreCase(ag.APTS_Modification_Type__c))
                    ehcProp.add(ag.Apttus_QPComply__RelatedProposalId__c);
                else if(ag.APTS_New_Sale_or_Modification__c != null 
                        && APTS_Constants.MODIFICATION.equalsIgnoreCase(ag.APTS_New_Sale_or_Modification__c) 
                        && !String.isEmpty(ag.APTS_Modification_Type__c))
                    otherModProp.add(ag.Apttus_QPComply__RelatedProposalId__c);
            }
            
            system.debug('Number of Quote/Proposals to be updated --> '+proposalIdSet.size());
            system.debug('Number of EHC Proposals --> '+ehcProp.size()+' Data --> '+ehcProp);
            system.debug('Number of Proposals except EHC --> '+otherModProp.size()+' Data --> '+otherModProp);
            
            //Reuse Helper method to reset Parent Agreement Status from In Modification to Active or Inactive
            List<Apttus__APTS_Agreement__c> updateInModAgList = APTS_QuoteProposalBaseTriggerHelper.resetParentAgreementStatus(proposalIdSet, ehcProp, otherModProp);
            
            system.debug('Total number of In Modification agreements to update --> '+updateInModAgList.size());
            system.debug('Total number of agreements to delete --> '+agList.size());
            
            if(!proposalIdSet.isEmpty())
                System.enqueueJob(new APTS_AgreementDeleteQueueable(proposalIdSet, proposalStatus, proposalComment));
            
            if(!updateInModAgList.isEmpty())
                update updateInModAgList;
            
            if(!agList.isEmpty())
                delete agList;
        } catch(DMLException e){
            system.debug('===ERROR: APTS_AgreementDeletionBatch==='+e.getStackTraceString());
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        system.debug('===FINISH=== APTS_AgreementDeletionBatch Batch execution has been completed');
    }
}