/************************************************************************************************************************
@Name: APTPS_CLMChildAgreements
@Author: Conga PS Dev Team
@CreateDate: 16 September 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CLMChildAgreements implements APTPS_CLMChildAgreementsCallable {
    private static String dsMetadataName = 'APTPS_CLMChildAgreements__mdt';
    public static Map<String, String> prodCodeClassNameMap = new Map<String, String>();
    
    public APTPS_CLMChildAgreements() {
        setProdCodeClassNameMap();        
    }
    
    /** 
    @description: Find out the APEX class implementation for the given productCode
    @param: Product Code
    @return: APEX Class name
    */
    private String findChildAgreement_Implementation(String productCode) {
        String className = null;
        if(productCode != null && !prodCodeClassNameMap.isEmpty() 
           && prodCodeClassNameMap.containsKey(productCode)) {
               className = prodCodeClassNameMap.get(productCode);
           }
        system.debug('APEX Class Details --> '+className);
        return className;
    }
    
    /** 
    @description: Execute product specific Deal Summary logic.
    @param: Product code and arguments
    @return: 
    */
    public Object call(Map<String,set<string>> productArgMap) {
        system.debug('Arguments to call method --> '+productArgMap);
        String retMsg = null;
        for(String productCode : productArgMap.keySet()){
            if(!String.isEmpty(productCode)) {
                Set<String> args = productArgMap.get(productCode);
                if(args == null) 
                    throw new ExtensionMalformedCallException('Agreement data is missing.');
                String className = findChildAgreement_Implementation(productCode);
                if(className != null) {
                    APTPS_CLMChildAgreementsInterface dsObj = (APTPS_CLMChildAgreementsInterface)Type.forName(className).newInstance();
                    dsObj.updateChildAgreement(args);
                    retMsg = 'SUCCESS';
                }
            }
        }
        
        return retMsg;
    }
    
    /** 
    @description: Custom Exception implementation
    @param:
    @return: 
    */
    public class ExtensionMalformedCallException extends Exception {
        public String errMsg = '';
        public void ExtensionMalformedCallException(String errMsg) {
            this.errMsg = errMsg;
        }
    }
    
    /** 
    @description: Read CLM Fields Registry metadata and prepare the Map
    @param:
    @return: 
    */
    private void setProdCodeClassNameMap() {
        List<APTPS_CLMChildAgreements__mdt> fieldsSummaryHandlers = [SELECT Product_Code__c, Class_Name__c FROM APTPS_CLMChildAgreements__mdt];
        for(APTPS_CLMChildAgreements__mdt handler : fieldsSummaryHandlers) {
            if(handler.Product_Code__c != null && handler.Class_Name__c != null) 
                prodCodeClassNameMap.put(handler.Product_Code__c, handler.Class_Name__c);
        }
    }
    
}