/*************************************************************
 @Name: APTS_ExecuteTerminationAPIBatch
 @Author: Siva Kumar
 @CreateDate: 16 December 2019
 @Description : This batch class is used to execute Termination API to terminate agreement and parent agreement
 ******************************************************************/
global class APTS_ExecuteTerminationAPIBatch implements Database.Batchable<sObject>, Database.Stateful{
	global Id terminateAgreementId;
	//global Set<Id> agreementIds=new Set<Id>();
	global boolean isRelatedAgreementExists;
	global boolean setAgreementStatusToInactive;
	global List<Apttus__APTS_Agreement__c> relAgreementObj = new List<Apttus__APTS_Agreement__c>();
	global APTS_ExecuteTerminationAPIBatch(Id terminateAgreementId, Boolean setInactiveStatus){
		this.terminateAgreementId = terminateAgreementId;
		this.isRelatedAgreementExists = false;
		this.setAgreementStatusToInactive = setInactiveStatus;
	}

	global List<Apttus__APTS_Agreement__c> start(Database.BatchableContext BC){
		System.debug('@@ inside execute start');
		//relAgreementObj = [SELECT Id,Apttus__APTS_Contract_From__c,Apttus__APTS_Contract_To__c,APTS_Business_Object_Type__c FROM Apttus__APTS_Related_Agreement__c WHERE Apttus__APTS_Contract_From__c =:terminateAgreementId AND APTS_Business_Object_Type__c='Agreement Life Cycle Flow' Limit 1];
		Apttus__APTS_Agreement__c activeAgreementObj = [SELECT Id, Apttus__Status__c, Card_Number__c
		                                                FROM Apttus__APTS_Agreement__c
		                                                WHERE Id = :terminateAgreementId
		                                                LIMIT 1];
		relAgreementObj = [SELECT Id, Card_Number__c
		                   FROM Apttus__APTS_Agreement__c
		                   WHERE Id = :activeAgreementObj.Id OR (Card_Number__c != null AND Card_Number__c = :activeAgreementObj.Card_Number__c)];
		system.debug('relAgreementObj-->' + relAgreementObj);
		return this.relAgreementObj;
	}

	global void execute(Database.BatchableContext BC, List<Apttus__APTS_Agreement__c> agreementObj){


		System.debug('@@ inside execute');
		try{

			if (agreementObj.size() > 0){
				isRelatedAgreementExists = true;
				Integer jobs = [SELECT count()
				                FROM AsyncApexJob
				                where (ApexClass.name = 'OrderWorkflowQJob' OR ApexClass.name = 'CreateOrderQJob' OR ApexClass.name = 'APTS_UpdateAssetLIQueueble') AND (Status = 'Queued' or Status = 'Processing' or Status = 'Preparing')];
				if (jobs == 0){
					terminateAgreement(agreementObj[0].id);
					isRelatedAgreementExists = false;
					system.debug('agreementObj-->' + agreementObj);

				}

			}


		} catch (Exception e){
			System.debug('@@ exception ' + e.getMessage()+' line number ' + e.getLineNumber());
		}
	}

	public void terminateAgreement(Id agreementId){
		Boolean response = Apttus.AgreementWebService.terminateAgreement(agreementId);
		if (response){
			List<Apttus_Config2__AssetLineItem__c> updateALIList = new List<Apttus_Config2__AssetLineItem__c>();
			Apttus__APTS_Agreement__c updateAgreement = new Apttus__APTS_Agreement__c(id = agreementId, Apttus__Termination_Date__c = system.today(), Agreement_Status__c = 'Terminated', Chevron_Status__c = 'Terminated', APTS_Path_Chevron_Status__c = 'Terminated');
			//Edit 12-07: Updated updateAgreement to set Chevron status and Path Chevron status to Terminated
			system.debug('updateAgreement-->' + updateAgreement);
			update updateAgreement;
			Apttus__APTS_Agreement__c terminateAgreementObj = [SELECT Id, Apttus__Status__c, Card_Number__c
			                                                   FROM Apttus__APTS_Agreement__c
			                                                   WHERE Id = :agreementId
			                                                   LIMIT 1];
			for (Apttus_Config2__AssetLineItem__c updateALI : [SELECT Id, Apttus_CMConfig__AgreementId__c, Apttus_Config2__AssetStatus__c
			                                                   FROM Apttus_Config2__AssetLineItem__c
			                                                   WHERE Card_Number__c = :terminateAgreementObj.Card_Number__c]){
				updateALI.Apttus_Config2__AssetStatus__c = 'Cancelled';
				updateALI.APTS_Agreement_Status__c = 'Inactive';
				updateALI.APTS_NJ_Asset_Status__c = 'Terminated';
				updateALIList.add(updateALI);
			}
			system.debug('updateALIList-->' + updateALIList);
			if (!updateALIList.isEmpty()){
				update updateALIList;
			}
			//agreementIds.add(terminateAgreementId);
		}
	}

	global void finish(Database.BatchableContext BC){
		if (!Test.isRunningTest()){
            if(isRelatedAgreementExists) {
                APTS_ExecuteTerminationAPIBatch batch = new APTS_ExecuteTerminationAPIBatch(terminateAgreementId,setAgreementStatusToInactive );
                Database.executeBatch( batch, 1 );
           } 
        }
	}
}