/*************************************************************
@Name: APTPS_AccountLocation_Helper
@Author: Siva Kumar
@CreateDate: 21 October 2020
@Description : This class is used to prevent Duplicate values for Account Location
******************************************************************/
public class APTPS_AccountLocation_Helper {
  
    public static void AvoidDuplicate(List<Apttus_Config2__AccountLocation__c> accountLocationList,Map<id,Apttus_Config2__AccountLocation__c> oldMap){
        set<string> newNameSet = new set<string>();
        set<string> newFullNameSet = new set<string>();
        set<string> newAccountIdSet = new set<string>();
        set<APTPS_AccountLocation_Wrapper> dbWrapperSet = new set<APTPS_AccountLocation_Wrapper>();
        set<APTPS_AccountLocation_Wrapper> newWrapperSet = new set<APTPS_AccountLocation_Wrapper>();
        for(Apttus_Config2__AccountLocation__c newLE : accountLocationList){
            if((oldMap ==null && ((newLE.Name!=null || newLE.Full_Legal_Name__c!=null) && newLE.Apttus_Config2__AccountId__c!=null)) ||(oldMap !=null && (newLE.Name!=oldMap.get(newLE.Id).Name || newLE.Full_Legal_Name__c!=oldMap.get(newLE.Id).Full_Legal_Name__c || newLE.Apttus_Config2__AccountId__c!=oldMap.get(newLE.Id).Apttus_Config2__AccountId__c))) {
                APTPS_AccountLocation_Wrapper iKey = new APTPS_AccountLocation_Wrapper(newLE.Name, newLE.Full_Legal_Name__c, newLE.Apttus_Config2__AccountId__c);
            
                
                if(newWrapperSet.contains(iKey)){
                    newLE.addError('Duplicate in new list');
                }else{
                    newNameSet.add(newLE.Name);
                    newFullNameSet.add(newLE.Full_Legal_Name__c);
                    newAccountIdSet.add(newLE.Apttus_Config2__AccountId__c);
                    newWrapperSet.add(iKey);
                }
            }
        }

        for(Apttus_Config2__AccountLocation__c dbLE : [select id, Name, Full_Legal_Name__c, Apttus_Config2__AccountId__c from Apttus_Config2__AccountLocation__c where (Name IN:newNameSet OR Full_Legal_Name__c IN:newFullNameSet) AND Apttus_Config2__AccountId__c IN:newAccountIdSet]){
            dbWrapperSet.add(new APTPS_AccountLocation_Wrapper(dbLE.Name, dbLE.Full_Legal_Name__c, dbLE.Apttus_Config2__AccountId__c));
        }
        system.debug('dbWrapperSet-->'+dbWrapperSet);
        for(Apttus_Config2__AccountLocation__c newALE : accountLocationList){
            APTPS_AccountLocation_Wrapper iKey = new APTPS_AccountLocation_Wrapper(newALE.Name, newALE.Full_Legal_Name__c, newALE.Apttus_Config2__AccountId__c);
            if(dbWrapperSet.contains(iKey)){
                newALE.addError('Duplicate Account Location found');
            }

            
        }

    }   
   
    
}