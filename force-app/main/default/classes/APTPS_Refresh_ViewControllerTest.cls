/**
 * @description       : Test class for APTPS_Refresh_ViewController
 * @author            : Sagar Solanki
 * @group             :
 * @last modified on  : 11-02-2020
 * @last modified by  : Sagar Solanki
 * Modifications Log
 * Ver   Date         Author          Modification
 * 1.0   11-02-2020   Sagar Solanki   Initial Version
 **/
@isTest
public with sharing class APTPS_Refresh_ViewControllerTest{
	@isTest
	static void testUpdateRecord(){
		Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();

		Account accToInsert = new Account();
		accToInsert.Name = 'APTS Test Account';
		accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
		accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
		insert accToInsert;

		Apttus__APTS_Agreement__c aggToInsert = new Apttus__APTS_Agreement__c();
		aggToInsert.Name = 'APTS Test Agreement 1';
		aggToInsert.Program__c = 'NetJets U.S.';
		aggToInsert.Apttus__Account__c = accToInsert.Id;
		//aggToInsert.RecordTypeId = recordTypeIdNJUS;
		aggToInsert.Apttus__Status_Category__c = 'Request';
		aggToInsert.Apttus__Status__c = 'Request';
		aggToInsert.APTPS_Number_of_Conversion_Aircraft_Line__c = 5;
		aggToInsert.APTS_Modification_Type__c = 'Assignment';
		insert aggToInsert;

		Test.startTest();
		String result = APTPS_Refresh_ViewController.updateRecord(aggToInsert.Id, recordTypeIdNJUS);
		System.assertEquals('SUCCESS', result, 'Success!');

		Test.stopTest();
	}
}